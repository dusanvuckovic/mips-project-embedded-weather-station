_set_as_input:
;Thermometer.c,5 :: 		void set_as_input() {
SUB	SP, SP, #4
STR	LR, [SP, #0]
;Thermometer.c,6 :: 		GPIO_Config(thermo_port, thermo_pinmask,
MOVW	R0, #lo_addr(_thermo_pinmask+0)
MOVT	R0, #hi_addr(_thermo_pinmask+0)
LDRSH	R1, [R0, #0]
MOVW	R0, #lo_addr(_thermo_port+0)
MOVT	R0, #hi_addr(_thermo_port+0)
LDR	R0, [R0, #0]
;Thermometer.c,10 :: 		_GPIO_CFG_SPEED_50MHZ
MOVW	R2, #2146
;Thermometer.c,6 :: 		GPIO_Config(thermo_port, thermo_pinmask,
;Thermometer.c,10 :: 		_GPIO_CFG_SPEED_50MHZ
BL	_GPIO_Config+0
;Thermometer.c,12 :: 		}
L_end_set_as_input:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _set_as_input
_set_as_output:
;Thermometer.c,14 :: 		void set_as_output() {
SUB	SP, SP, #4
STR	LR, [SP, #0]
;Thermometer.c,15 :: 		GPIO_Config(thermo_port, thermo_pinmask,
MOVW	R0, #lo_addr(_thermo_pinmask+0)
MOVT	R0, #hi_addr(_thermo_pinmask+0)
LDRSH	R1, [R0, #0]
MOVW	R0, #lo_addr(_thermo_port+0)
MOVT	R0, #hi_addr(_thermo_port+0)
LDR	R0, [R0, #0]
;Thermometer.c,19 :: 		_GPIO_CFG_SPEED_50MHZ
MOVW	R2, #2148
;Thermometer.c,15 :: 		GPIO_Config(thermo_port, thermo_pinmask,
;Thermometer.c,19 :: 		_GPIO_CFG_SPEED_50MHZ
BL	_GPIO_Config+0
;Thermometer.c,21 :: 		}
L_end_set_as_output:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _set_as_output
_thermo_read_bit:
;Thermometer.c,23 :: 		unsigned char thermo_read_bit() {
SUB	SP, SP, #4
STR	LR, [SP, #0]
;Thermometer.c,24 :: 		unsigned char value = 7;
;Thermometer.c,26 :: 		THERMO_OUT = 0;
MOVS	R1, #0
SXTB	R1, R1
MOVW	R0, #lo_addr(ODR5_GPIOB_ODR_bit+0)
MOVT	R0, #hi_addr(ODR5_GPIOB_ODR_bit+0)
STR	R1, [R0, #0]
;Thermometer.c,27 :: 		set_as_output();
BL	_set_as_output+0
;Thermometer.c,28 :: 		Delay_us(5);
MOVW	R7, #231
MOVT	R7, #0
NOP
NOP
L_thermo_read_bit0:
SUBS	R7, R7, #1
BNE	L_thermo_read_bit0
NOP
NOP
NOP
NOP
;Thermometer.c,30 :: 		set_as_input();
BL	_set_as_input+0
;Thermometer.c,31 :: 		Delay_us(10);
MOVW	R7, #465
MOVT	R7, #0
NOP
NOP
L_thermo_read_bit2:
SUBS	R7, R7, #1
BNE	L_thermo_read_bit2
NOP
NOP
;Thermometer.c,32 :: 		value = THERMO_IN;
MOVW	R0, #lo_addr(IDR5_GPIOB_IDR_bit+0)
MOVT	R0, #hi_addr(IDR5_GPIOB_IDR_bit+0)
; value start address is: 0 (R0)
LDR	R0, [R0, #0]
;Thermometer.c,33 :: 		Delay_us(50);
MOVW	R7, #2331
MOVT	R7, #0
NOP
NOP
L_thermo_read_bit4:
SUBS	R7, R7, #1
BNE	L_thermo_read_bit4
NOP
NOP
NOP
NOP
;Thermometer.c,35 :: 		return value;
; value end address is: 0 (R0)
;Thermometer.c,36 :: 		}
L_end_thermo_read_bit:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _thermo_read_bit
_thermo_write_zero_bit:
;Thermometer.c,38 :: 		void thermo_write_zero_bit() {
SUB	SP, SP, #4
STR	LR, [SP, #0]
;Thermometer.c,39 :: 		THERMO_OUT = 0;
MOVS	R1, #0
SXTB	R1, R1
MOVW	R0, #lo_addr(ODR5_GPIOB_ODR_bit+0)
MOVT	R0, #hi_addr(ODR5_GPIOB_ODR_bit+0)
STR	R1, [R0, #0]
;Thermometer.c,40 :: 		set_as_output();
BL	_set_as_output+0
;Thermometer.c,41 :: 		Delay_us(65);
MOVW	R7, #3031
MOVT	R7, #0
NOP
NOP
L_thermo_write_zero_bit6:
SUBS	R7, R7, #1
BNE	L_thermo_write_zero_bit6
NOP
NOP
NOP
NOP
;Thermometer.c,43 :: 		set_as_input();
BL	_set_as_input+0
;Thermometer.c,44 :: 		Delay_us(5);
MOVW	R7, #231
MOVT	R7, #0
NOP
NOP
L_thermo_write_zero_bit8:
SUBS	R7, R7, #1
BNE	L_thermo_write_zero_bit8
NOP
NOP
NOP
NOP
;Thermometer.c,45 :: 		}
L_end_thermo_write_zero_bit:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _thermo_write_zero_bit
_thermo_write_one_bit:
;Thermometer.c,47 :: 		void thermo_write_one_bit() {
SUB	SP, SP, #4
STR	LR, [SP, #0]
;Thermometer.c,48 :: 		THERMO_OUT = 0;
MOVS	R1, #0
SXTB	R1, R1
MOVW	R0, #lo_addr(ODR5_GPIOB_ODR_bit+0)
MOVT	R0, #hi_addr(ODR5_GPIOB_ODR_bit+0)
STR	R1, [R0, #0]
;Thermometer.c,49 :: 		set_as_output();
BL	_set_as_output+0
;Thermometer.c,50 :: 		Delay_us(5);
MOVW	R7, #231
MOVT	R7, #0
NOP
NOP
L_thermo_write_one_bit10:
SUBS	R7, R7, #1
BNE	L_thermo_write_one_bit10
NOP
NOP
NOP
NOP
;Thermometer.c,52 :: 		set_as_input();
BL	_set_as_input+0
;Thermometer.c,53 :: 		Delay_us(55);
MOVW	R7, #2565
MOVT	R7, #0
NOP
NOP
L_thermo_write_one_bit12:
SUBS	R7, R7, #1
BNE	L_thermo_write_one_bit12
NOP
NOP
;Thermometer.c,54 :: 		}
L_end_thermo_write_one_bit:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _thermo_write_one_bit
_thermo_write_bit:
;Thermometer.c,56 :: 		void thermo_write_bit(unsigned char c) {
; c start address is: 0 (R0)
SUB	SP, SP, #4
STR	LR, [SP, #0]
; c end address is: 0 (R0)
; c start address is: 0 (R0)
;Thermometer.c,57 :: 		if (c)
CMP	R0, #0
IT	EQ
BEQ	L_thermo_write_bit14
; c end address is: 0 (R0)
;Thermometer.c,58 :: 		thermo_write_one_bit();
BL	_thermo_write_one_bit+0
IT	AL
BAL	L_thermo_write_bit15
L_thermo_write_bit14:
;Thermometer.c,60 :: 		thermo_write_zero_bit();
BL	_thermo_write_zero_bit+0
L_thermo_write_bit15:
;Thermometer.c,61 :: 		}
L_end_thermo_write_bit:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _thermo_write_bit
_thermo_write:
;Thermometer.c,63 :: 		void thermo_write(unsigned char write_data) {
; write_data start address is: 0 (R0)
SUB	SP, SP, #4
STR	LR, [SP, #0]
; write_data end address is: 0 (R0)
; write_data start address is: 0 (R0)
;Thermometer.c,65 :: 		for (i = 0; i < 8; i++) {
; i start address is: 44 (R11)
MOVW	R11, #0
SXTH	R11, R11
; write_data end address is: 0 (R0)
; i end address is: 44 (R11)
UXTB	R12, R0
L_thermo_write16:
; i start address is: 44 (R11)
; write_data start address is: 48 (R12)
CMP	R11, #8
IT	GE
BGE	L_thermo_write17
;Thermometer.c,66 :: 		thermo_write_bit(write_data & 1); //LSB
AND	R1, R12, #1
UXTB	R0, R1
BL	_thermo_write_bit+0
;Thermometer.c,67 :: 		write_data >>= 1;
LSR	R12, R12, #1
UXTB	R12, R12
;Thermometer.c,65 :: 		for (i = 0; i < 8; i++) {
ADD	R11, R11, #1
SXTH	R11, R11
;Thermometer.c,68 :: 		}
; write_data end address is: 48 (R12)
; i end address is: 44 (R11)
IT	AL
BAL	L_thermo_write16
L_thermo_write17:
;Thermometer.c,69 :: 		}
L_end_thermo_write:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _thermo_write
_thermo_read:
;Thermometer.c,71 :: 		unsigned char thermo_read() {
SUB	SP, SP, #4
STR	LR, [SP, #0]
;Thermometer.c,72 :: 		unsigned char result = 0, i;
; result start address is: 48 (R12)
MOVW	R12, #0
;Thermometer.c,73 :: 		for (i = 0; i < 8; i++)
; i start address is: 44 (R11)
MOVW	R11, #0
; result end address is: 48 (R12)
; i end address is: 44 (R11)
L_thermo_read19:
; i start address is: 44 (R11)
; result start address is: 48 (R12)
CMP	R11, #8
IT	CS
BCS	L_thermo_read20
;Thermometer.c,74 :: 		result |= (thermo_read_bit() << i); //LSB
BL	_thermo_read_bit+0
LSL	R0, R0, R11
UXTH	R0, R0
ORR	R0, R12, R0, LSL #0
UXTB	R12, R0
;Thermometer.c,73 :: 		for (i = 0; i < 8; i++)
ADD	R11, R11, #1
UXTB	R11, R11
;Thermometer.c,74 :: 		result |= (thermo_read_bit() << i); //LSB
; i end address is: 44 (R11)
IT	AL
BAL	L_thermo_read19
L_thermo_read20:
;Thermometer.c,75 :: 		return result;
UXTB	R0, R12
; result end address is: 48 (R12)
;Thermometer.c,76 :: 		}
L_end_thermo_read:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _thermo_read
_thermo_reset:
;Thermometer.c,78 :: 		unsigned short thermo_reset() {
SUB	SP, SP, #4
STR	LR, [SP, #0]
;Thermometer.c,79 :: 		unsigned short value = 0xFF;
;Thermometer.c,81 :: 		THERMO_OUT = 0;
MOVS	R1, #0
SXTB	R1, R1
MOVW	R0, #lo_addr(ODR5_GPIOB_ODR_bit+0)
MOVT	R0, #hi_addr(ODR5_GPIOB_ODR_bit+0)
STR	R1, [R0, #0]
;Thermometer.c,82 :: 		set_as_output();
BL	_set_as_output+0
;Thermometer.c,83 :: 		Delay_us(720);
MOVW	R7, #33598
MOVT	R7, #0
NOP
NOP
L_thermo_reset22:
SUBS	R7, R7, #1
BNE	L_thermo_reset22
NOP
NOP
NOP
;Thermometer.c,84 :: 		set_as_input();
BL	_set_as_input+0
;Thermometer.c,85 :: 		Delay_us(55);
MOVW	R7, #2565
MOVT	R7, #0
NOP
NOP
L_thermo_reset24:
SUBS	R7, R7, #1
BNE	L_thermo_reset24
NOP
NOP
;Thermometer.c,87 :: 		value = THERMO_IN;
MOVW	R0, #lo_addr(IDR5_GPIOB_IDR_bit+0)
MOVT	R0, #hi_addr(IDR5_GPIOB_IDR_bit+0)
; value start address is: 0 (R0)
LDR	R0, [R0, #0]
;Thermometer.c,88 :: 		Delay_us(400);
MOVW	R7, #18665
MOVT	R7, #0
NOP
NOP
L_thermo_reset26:
SUBS	R7, R7, #1
BNE	L_thermo_reset26
NOP
NOP
;Thermometer.c,90 :: 		return value;
; value end address is: 0 (R0)
;Thermometer.c,91 :: 		}
L_end_thermo_reset:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _thermo_reset
_thermo_get_family_code:
;Thermometer.c,93 :: 		unsigned short thermo_get_family_code() {
SUB	SP, SP, #4
STR	LR, [SP, #0]
;Thermometer.c,94 :: 		thermo_reset();
BL	_thermo_reset+0
;Thermometer.c,95 :: 		thermo_write(READ_ROM); //write 0x33
MOVS	R0, #51
BL	_thermo_write+0
;Thermometer.c,96 :: 		return thermo_read();  //read family code;
BL	_thermo_read+0
;Thermometer.c,97 :: 		}
L_end_thermo_get_family_code:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _thermo_get_family_code
_thermo_get_serial_number:
;Thermometer.c,99 :: 		unsigned long long thermo_get_serial_number() {
SUB	SP, SP, #16
STR	LR, [SP, #0]
;Thermometer.c,101 :: 		unsigned long long number = 0, temp = 0;
ADD	R11, SP, #8
ADD	R10, R11, #8
MOVW	R12, #lo_addr(?ICSthermo_get_serial_number_number_L0+0)
MOVT	R12, #hi_addr(?ICSthermo_get_serial_number_number_L0+0)
BL	___CC2DW+0
;Thermometer.c,102 :: 		thermo_get_family_code(); //first steps to serial
BL	_thermo_get_family_code+0
;Thermometer.c,103 :: 		for (i = 0; i < 6; i++) {
; i start address is: 16 (R4)
MOVS	R4, #0
SXTH	R4, R4
; i end address is: 16 (R4)
L_thermo_get_serial_number28:
; i start address is: 16 (R4)
CMP	R4, #6
IT	GE
BGE	L_thermo_get_serial_number29
;Thermometer.c,104 :: 		temp = thermo_read();
STRH	R4, [SP, #4]
BL	_thermo_read+0
LDRSH	R4, [SP, #4]
; temp start address is: 20 (R5)
UXTB	R5, R0
MOVS	R6, #0
;Thermometer.c,105 :: 		number |= (temp << (i*8));
LSLS	R0, R4, #3
SXTH	R0, R0
ASRS	R1, R0, #31
SUBS	R2, R0, #32
BPL	L__thermo_get_serial_number65
RSB	R2, R0, #32
LSR	R2, R5, R2
LSL	R3, R6, R0
ORRS	R3, R2
LSL	R2, R5, R0
B	L__thermo_get_serial_number66
L__thermo_get_serial_number65:
LSL	R3, R5, R2
MOVS	R2, #0
L__thermo_get_serial_number66:
; temp end address is: 20 (R5)
LDRD	R0, R1, [SP, #8]
ORRS	R0, R2
ORRS	R1, R3
STRD	R0, R1, [SP, #8]
;Thermometer.c,103 :: 		for (i = 0; i < 6; i++) {
ADDS	R0, R4, #1
; i end address is: 16 (R4)
; i start address is: 0 (R0)
;Thermometer.c,106 :: 		}
SXTH	R4, R0
; i end address is: 0 (R0)
IT	AL
BAL	L_thermo_get_serial_number28
L_thermo_get_serial_number29:
;Thermometer.c,107 :: 		return number;
LDRD	R0, R1, [SP, #8]
;Thermometer.c,108 :: 		}
L_end_thermo_get_serial_number:
LDR	LR, [SP, #0]
ADD	SP, SP, #16
BX	LR
; end of _thermo_get_serial_number
Thermometer_thermo_measure_temperature:
;Thermometer.c,110 :: 		static void thermo_measure_temperature() {
SUB	SP, SP, #4
STR	LR, [SP, #0]
;Thermometer.c,111 :: 		thermo_reset();
BL	_thermo_reset+0
;Thermometer.c,112 :: 		thermo_write(SKIP_ROM);
MOVS	R0, #204
BL	_thermo_write+0
;Thermometer.c,113 :: 		thermo_write(CONVERT_T);
MOVS	R0, #68
BL	_thermo_write+0
;Thermometer.c,114 :: 		Delay_ms(1000);
MOVW	R7, #5033
MOVT	R7, #712
NOP
NOP
L_Thermometer_thermo_measure_temperature31:
SUBS	R7, R7, #1
BNE	L_Thermometer_thermo_measure_temperature31
NOP
NOP
;Thermometer.c,115 :: 		}
L_end_thermo_measure_temperature:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of Thermometer_thermo_measure_temperature
_calculate_result:
;Thermometer.c,117 :: 		float calculate_result(unsigned char lower, unsigned char higher) {
; higher start address is: 4 (R1)
; lower start address is: 0 (R0)
SUB	SP, SP, #4
; higher end address is: 4 (R1)
; lower end address is: 0 (R0)
; lower start address is: 0 (R0)
; higher start address is: 4 (R1)
;Thermometer.c,118 :: 		float val = lower >> 1;
LSRS	R1, R0, #1
VCVT.F32	#0, S1, S1
; val start address is: 4 (R1)
;Thermometer.c,120 :: 		if (lower & 1) {
AND	R2, R0, #1
UXTB	R2, R2
; lower end address is: 0 (R0)
CMP	R2, #0
IT	EQ
BEQ	L__calculate_result49
;Thermometer.c,121 :: 		if (higher == 0)
CMP	R1, #0
IT	NE
BNE	L_calculate_result34
; higher end address is: 4 (R1)
;Thermometer.c,122 :: 		val += 0.5;
VMOV.F32	S0, #0.5
VADD.F32	S0, S1, S0
; val end address is: 4 (R1)
; val start address is: 0 (R0)
; val end address is: 0 (R0)
IT	AL
BAL	L_calculate_result35
L_calculate_result34:
;Thermometer.c,124 :: 		val -= 0.5;
; val start address is: 4 (R1)
VMOV.F32	S0, #0.5
VSUB.F32	S0, S1, S0
VMOV.F32	S1, S0
; val end address is: 4 (R1)
VMOV.F32	S0, S1
L_calculate_result35:
;Thermometer.c,125 :: 		}
; val start address is: 0 (R0)
; val end address is: 0 (R0)
IT	AL
BAL	L_calculate_result33
L__calculate_result49:
;Thermometer.c,120 :: 		if (lower & 1) {
VMOV.F32	S0, S1
;Thermometer.c,125 :: 		}
L_calculate_result33:
;Thermometer.c,127 :: 		return val;
; val start address is: 0 (R0)
; val end address is: 0 (R0)
;Thermometer.c,129 :: 		}
L_end_calculate_result:
ADD	SP, SP, #4
BX	LR
; end of _calculate_result
_thermo_read_temperature:
;Thermometer.c,131 :: 		float thermo_read_temperature() {
SUB	SP, SP, #12
STR	LR, [SP, #0]
;Thermometer.c,135 :: 		thermo_measure_temperature();
BL	Thermometer_thermo_measure_temperature+0
;Thermometer.c,137 :: 		thermo_reset();
BL	_thermo_reset+0
;Thermometer.c,138 :: 		thermo_write(SKIP_ROM);
MOVS	R0, #204
BL	_thermo_write+0
;Thermometer.c,139 :: 		thermo_write(READ_SCRATCHPAD);
MOVS	R0, #190
BL	_thermo_write+0
;Thermometer.c,140 :: 		lower = thermo_read();
BL	_thermo_read+0
STRB	R0, [SP, #4]
;Thermometer.c,141 :: 		higher = thermo_read();
BL	_thermo_read+0
; higher start address is: 44 (R11)
UXTB	R11, R0
;Thermometer.c,142 :: 		thermo_reset();
BL	_thermo_reset+0
;Thermometer.c,144 :: 		result = lower >> 1;
LDRB	R0, [SP, #4]
LSRS	R0, R0, #1
UXTB	R0, R0
VMOV	S0, R0
VCVT.F32	#0, S0, S0
VSTR	#1, S0, [SP, #8]
;Thermometer.c,145 :: 		if (lower & 1 && higher == 0x00)
LDRB	R0, [SP, #4]
AND	R0, R0, #1
UXTB	R0, R0
CMP	R0, #0
IT	EQ
BEQ	L__thermo_read_temperature52
CMP	R11, #0
IT	NE
BNE	L__thermo_read_temperature51
; higher end address is: 44 (R11)
L__thermo_read_temperature50:
;Thermometer.c,146 :: 		result += 0.5;
VLDR	#1, S1, [SP, #8]
VMOV.F32	S0, #0.5
VADD.F32	S0, S1, S0
VSTR	#1, S0, [SP, #8]
;Thermometer.c,145 :: 		if (lower & 1 && higher == 0x00)
L__thermo_read_temperature52:
L__thermo_read_temperature51:
;Thermometer.c,149 :: 		return result;
VLDR	#1, S0, [SP, #8]
;Thermometer.c,150 :: 		}
L_end_thermo_read_temperature:
LDR	LR, [SP, #0]
ADD	SP, SP, #12
BX	LR
; end of _thermo_read_temperature
_thermo_read_precise_temperature:
;Thermometer.c,152 :: 		float thermo_read_precise_temperature() {
SUB	SP, SP, #8
STR	LR, [SP, #0]
;Thermometer.c,155 :: 		thermo_measure_temperature();
BL	Thermometer_thermo_measure_temperature+0
;Thermometer.c,157 :: 		thermo_reset();
BL	_thermo_reset+0
;Thermometer.c,158 :: 		thermo_write(SKIP_ROM);
MOVS	R0, #204
BL	_thermo_write+0
;Thermometer.c,159 :: 		thermo_write(READ_SCRATCHPAD);
MOVS	R0, #190
BL	_thermo_write+0
;Thermometer.c,160 :: 		lower = thermo_read();
BL	_thermo_read+0
STRB	R0, [SP, #4]
;Thermometer.c,161 :: 		higher = thermo_read();
BL	_thermo_read+0
;Thermometer.c,162 :: 		thermo_read(); //th
BL	_thermo_read+0
;Thermometer.c,163 :: 		thermo_read(); //tl
BL	_thermo_read+0
;Thermometer.c,164 :: 		thermo_read(); //reserved
BL	_thermo_read+0
;Thermometer.c,165 :: 		thermo_read(); //reserved
BL	_thermo_read+0
;Thermometer.c,166 :: 		count_remain = thermo_read();
BL	_thermo_read+0
STRB	R0, [SP, #5]
;Thermometer.c,167 :: 		count_per_c = thermo_read();
BL	_thermo_read+0
;Thermometer.c,169 :: 		temp_read = ((short)lower >> 1);
LDRSB	R1, [SP, #4]
ASRS	R1, R1, #1
SXTB	R1, R1
;Thermometer.c,171 :: 		return temp_read - 0.25 + ((float)(count_per_c - count_remain) / count_per_c);
VMOV	S1, R1
VCVT.F32	#0, S1, S1
VMOV.F32	S0, #0.25
VSUB.F32	S2, S1, S0
LDRB	R1, [SP, #5]
SUB	R1, R0, R1
SXTH	R1, R1
VMOV	S1, R1
VCVT.F32	#1, S1, S1
VMOV	S0, R0
VCVT.F32	#0, S0, S0
VDIV.F32	S0, S1, S0
VADD.F32	S0, S2, S0
;Thermometer.c,172 :: 		}
L_end_thermo_read_precise_temperature:
LDR	LR, [SP, #0]
ADD	SP, SP, #8
BX	LR
; end of _thermo_read_precise_temperature
_calc_crc:
;Thermometer.c,174 :: 		unsigned char calc_crc(unsigned char buffer[]) {
; buffer start address is: 0 (R0)
SUB	SP, SP, #4
; buffer end address is: 0 (R0)
; buffer start address is: 0 (R0)
;Thermometer.c,176 :: 		unsigned char shift_reg=0, data_bit, shift_bit;
; shift_reg start address is: 24 (R6)
MOVS	R6, #0
;Thermometer.c,178 :: 		for (i=0; i<8; i++) { //for every byte
; i start address is: 16 (R4)
MOVS	R4, #0
SXTH	R4, R4
; buffer end address is: 0 (R0)
; shift_reg end address is: 24 (R6)
; i end address is: 16 (R4)
L_calc_crc39:
; i start address is: 16 (R4)
; shift_reg start address is: 24 (R6)
; buffer start address is: 0 (R0)
CMP	R4, #8
IT	GE
BGE	L_calc_crc40
;Thermometer.c,179 :: 		for (j=0; j<8; j++) { //for every bit
; j start address is: 20 (R5)
MOVS	R5, #0
SXTH	R5, R5
; buffer end address is: 0 (R0)
; shift_reg end address is: 24 (R6)
; j end address is: 20 (R5)
; i end address is: 16 (R4)
L_calc_crc42:
; j start address is: 20 (R5)
; buffer start address is: 0 (R0)
; shift_reg start address is: 24 (R6)
; i start address is: 16 (R4)
CMP	R5, #8
IT	GE
BGE	L_calc_crc43
;Thermometer.c,180 :: 		data_bit = (buffer[i]>>j)&0x01;
ADDS	R1, R0, R4
LDRB	R1, [R1, #0]
LSRS	R1, R5
UXTB	R1, R1
AND	R3, R1, #1
UXTB	R3, R3
;Thermometer.c,181 :: 		shift_bit = shift_reg & 0x01;
AND	R2, R6, #1
UXTB	R2, R2
;Thermometer.c,182 :: 		shift_reg >>= 1;
LSRS	R6, R6, #1
UXTB	R6, R6
;Thermometer.c,183 :: 		if (data_bit ^ shift_bit)
EOR	R1, R3, R2, LSL #0
UXTB	R1, R1
CMP	R1, #0
IT	EQ
BEQ	L__calc_crc53
;Thermometer.c,184 :: 		shift_reg = shift_reg ^ 0x8c;
EOR	R6, R6, #140
UXTB	R6, R6
; shift_reg end address is: 24 (R6)
IT	AL
BAL	L_calc_crc45
L__calc_crc53:
;Thermometer.c,183 :: 		if (data_bit ^ shift_bit)
;Thermometer.c,184 :: 		shift_reg = shift_reg ^ 0x8c;
L_calc_crc45:
;Thermometer.c,179 :: 		for (j=0; j<8; j++) { //for every bit
; shift_reg start address is: 24 (R6)
ADDS	R5, R5, #1
SXTH	R5, R5
;Thermometer.c,185 :: 		}
; j end address is: 20 (R5)
IT	AL
BAL	L_calc_crc42
L_calc_crc43:
;Thermometer.c,178 :: 		for (i=0; i<8; i++) { //for every byte
ADDS	R4, R4, #1
SXTH	R4, R4
;Thermometer.c,186 :: 		}
; buffer end address is: 0 (R0)
; i end address is: 16 (R4)
IT	AL
BAL	L_calc_crc39
L_calc_crc40:
;Thermometer.c,187 :: 		return shift_reg;
UXTB	R0, R6
; shift_reg end address is: 24 (R6)
;Thermometer.c,188 :: 		}
L_end_calc_crc:
ADD	SP, SP, #4
BX	LR
; end of _calc_crc
_thermo_check_crc:
;Thermometer.c,190 :: 		bool thermo_check_crc() {
SUB	SP, SP, #20
STR	LR, [SP, #0]
;Thermometer.c,193 :: 		thermo_reset();
BL	_thermo_reset+0
;Thermometer.c,194 :: 		thermo_write(SKIP_ROM);
MOVS	R0, #204
BL	_thermo_write+0
;Thermometer.c,195 :: 		thermo_write(READ_SCRATCHPAD);
MOVS	R0, #190
BL	_thermo_write+0
;Thermometer.c,196 :: 		for (i = 0; i < 8; i++)
; i start address is: 8 (R2)
MOVS	R2, #0
SXTH	R2, R2
; i end address is: 8 (R2)
L_thermo_check_crc46:
; i start address is: 8 (R2)
CMP	R2, #8
IT	GE
BGE	L_thermo_check_crc47
;Thermometer.c,197 :: 		buffer[i] = thermo_read();
ADD	R0, SP, #8
ADDS	R0, R0, R2
STR	R0, [SP, #16]
STRH	R2, [SP, #4]
BL	_thermo_read+0
LDRSH	R2, [SP, #4]
LDR	R1, [SP, #16]
STRB	R0, [R1, #0]
;Thermometer.c,196 :: 		for (i = 0; i < 8; i++)
ADDS	R2, R2, #1
SXTH	R2, R2
;Thermometer.c,197 :: 		buffer[i] = thermo_read();
; i end address is: 8 (R2)
IT	AL
BAL	L_thermo_check_crc46
L_thermo_check_crc47:
;Thermometer.c,198 :: 		crc = thermo_read();
BL	_thermo_read+0
; crc start address is: 28 (R7)
UXTB	R7, R0
;Thermometer.c,199 :: 		return (crc == calc_crc(buffer));
ADD	R0, SP, #8
BL	_calc_crc+0
CMP	R7, R0
MOVW	R0, #0
BNE	L__thermo_check_crc73
MOVS	R0, #1
L__thermo_check_crc73:
; crc end address is: 28 (R7)
UXTB	R0, R0
;Thermometer.c,200 :: 		}
L_end_thermo_check_crc:
LDR	LR, [SP, #0]
ADD	SP, SP, #20
BX	LR
; end of _thermo_check_crc
Thermometer____?ag:
SUB	SP, SP, #4
L_end_Thermometer___?ag:
ADD	SP, SP, #4
BX	LR
; end of Thermometer____?ag
