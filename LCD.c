#include "LCD.h"
#include "LCD_Constants.h"

static bool screen, blink, underline;
static char cursor_position;
static char* empty_string = "                ";

static char upper_nibble(Lcd_Instruction ins) {
       return (ins >> 4) & 0xf;
}
static char lower_nibble(Lcd_Instruction ins) {
       return (ins & 0xf);
}

static void nibble_to_data_pins(char nibble) {
       if (nibble & 0x8)
          LCD_D7 = 1;
       else
          LCD_D7 = 0;
       if (nibble & 0x4)
          LCD_D6 = 1;
       else
          LCD_D6 = 0;
       if (nibble & 0x2)
          LCD_D5 = 1;
       else
          LCD_D5 = 0;
       if (nibble & 0x1)
          LCD_D4 = 1;
       else
          LCD_D4 = 0;
}

static Lcd_Instruction CLEAR_DISPLAY() {
       cursor_position = 0x00;
       return 0x1;
}
static Lcd_Instruction RETURN_HOME() {
       cursor_position = 0x00;
       return 0x2;
}
static Lcd_Instruction ENTRY_MODE_SET(bool increment, bool shift) {
       Lcd_Instruction opcode = 0x4;
       if (increment)
          opcode |= 0x2;
       if (shift)
          opcode |= 0x1;
       return opcode;
}
static Lcd_Instruction DISPLAY_CONTROL(bool display_on, bool cursor_on, bool cursor_blink) {
       Lcd_Instruction opcode = 0x8;
       LCD_RS = 0;
       if (display_on)
          opcode |= 0x4;
       if (cursor_on)
          opcode |= 0x2;
       if (cursor_blink)
          opcode |= 0x1;
       return opcode;
}
static Lcd_Instruction CURSOR_OR_DISPLAY_SHIFT(bool shift_instead_of_move, bool to_the_right) {
       Lcd_Instruction opcode = 0x10;
       LCD_RS = 0;
       if (shift_instead_of_move)
          opcode |= 0x8;
       if (to_the_right)
          opcode |= 0x4;
       return opcode;
}
static Lcd_Instruction FUNCTION_SET() {
       //function set is static, as follows:
       //bit 5 is 1, for the opcode
       //bit 4 is 0, 4 bit interface (no pins)
       //bit 3 is 1, two lines on the screen
       //bit 2 is 0, two lines => 5x8 dots
       //bits 1 and 0 are X
       return 0x28;
}

static Lcd_Instruction SET_DDRAM_ADDRESS(unsigned char address) {
       Lcd_Instruction opcode = 0x80;
       opcode |= address;
       return opcode;
}

static void pulse() {
       LCD_EN = 1;
       Delay_ms(1);
       LCD_EN = 0;
}

static void send_nibble(nibble n) {
       Delay_ms(1);
       nibble_to_data_pins(n);
       pulse();
}

static void send_instruction(Lcd_Instruction ins) {
      nibble first_nibble = upper_nibble(ins),
             second_nibble = lower_nibble(ins);
      send_nibble(first_nibble);
      send_nibble(second_nibble);
}

void LCD_initialize() {

     GPIO_Clk_Enable(LCD_RS_PORT);
     GPIO_Clk_Enable(LCD_EN_PORT);
     GPIO_Clk_Enable(LCD_D7_PORT);
     GPIO_Clk_Enable(LCD_D6_PORT);
     GPIO_Clk_Enable(LCD_D5_PORT);
     GPIO_Clk_Enable(LCD_D4_PORT);

     GPIO_Digital_Output(LCD_RS_PORT, LCD_RS_PINMASK);
     GPIO_Digital_Output(LCD_EN_PORT, LCD_EN_PINMASK);
     GPIO_Digital_Output(LCD_D7_PORT, LCD_D7_PINMASK);
     GPIO_Digital_Output(LCD_D6_PORT, LCD_D6_PINMASK);
     GPIO_Digital_Output(LCD_D5_PORT, LCD_D5_PINMASK);
     GPIO_Digital_Output(LCD_D4_PORT, LCD_D4_PINMASK);

     LCD_RS = LCD_EN = LCD_D7 = LCD_D6 = LCD_D5 = LCD_D4 = 0;

     Delay_ms(50);
     send_nibble(0x3);
     Delay_ms(5);
     send_nibble(0x3);
     Delay_ms(100);
     send_nibble(0x3);
     send_nibble(0x2);

     send_instruction(FUNCTION_SET());
     send_instruction(DISPLAY_CONTROL(false, false, false));
     send_instruction(CLEAR_DISPLAY());
     send_instruction(ENTRY_MODE_SET(true, false));
     send_instruction(DISPLAY_CONTROL(true, true, true));

     send_instruction(CLEAR_DISPLAY());

     screen = blink = underline = true;
     cursor_position = 0x00;
}

void LCD_clear_screen() {
     send_instruction(CLEAR_DISPLAY());
}

void LCD_clear_screen_top() {
     LCD_write_line_position(1, 1, empty_string);
     LCD_set_cursor(2, 1);
}

void LCD_clear_screen_bottom() {
     LCD_write_line_position(2, 1, empty_string);
     LCD_set_cursor(2, 1);
}

void LCD_turn_screen_off() {
     if (!screen)
        return;
     send_instruction(DISPLAY_CONTROL(false, underline, blink));
     screen = false;
}

void LCD_turn_screen_on() {
     if (screen)
        return;
     send_instruction(DISPLAY_CONTROL(true, underline, blink));
     screen = true;
}

void LCD_turn_underline_off() {
     if (!underline)
        return;
     send_instruction(DISPLAY_CONTROL(screen, false, blink));
     underline = false;
}

void LCD_turn_underline_on() {
     if (underline)
        return;
     send_instruction(DISPLAY_CONTROL(screen, true, blink));
     underline = true;
}

void LCD_turn_blinking_off() {
     if (!blink)
        return;
     send_instruction(DISPLAY_CONTROL(screen, underline, false));
     blink = false;
}

void LCD_turn_blinking_on() {
     if (blink)
        return;
     send_instruction(DISPLAY_CONTROL(screen, underline, true));
     blink = true;
}

void LCD_command_device(bool screen_on, bool underline_on, bool blinking_on) {
     screen = screen_on;
     underline = underline_on;
     blink = blinking_on;
     send_instruction(DISPLAY_CONTROL(screen, underline, blink));
}

void LCD_shift_screen_right() {
     send_instruction(CURSOR_OR_DISPLAY_SHIFT(true, true));
}

void LCD_shift_screen_left() {
     send_instruction(CURSOR_OR_DISPLAY_SHIFT(true, false));
}

void LCD_shift_screen(bool right) {
     send_instruction(CURSOR_OR_DISPLAY_SHIFT(false, !right));
}

void LCD_set_cursor(unsigned char row, unsigned char column) {
     unsigned char address = column - 1;
     if (row != 1 && row != 2)
        return;
     if (!column || column > 16)
        return;
     if (row == 2)
        address |= 0x40;
     send_instruction(SET_DDRAM_ADDRESS(address));
     cursor_position = address;
}

void LCD_set_cursor_to_row(unsigned char line) {
     if ((line != 1) || (line != 2))
        return;
     LCD_set_cursor(line, 0);
}

void LCD_write_character_cursor(char character) {
     if (cursor_position == 0x10) {
        LCD_set_cursor(2, 1);
        cursor_position = 0x40;
     }
     else if (cursor_position == 0x50) {
          LCD_set_cursor(1, 1);
          cursor_position = 0x00;
     }
     LCD_RS = 1;
     send_instruction(character);
     ++cursor_position;
     LCD_RS = 0;
}

void LCD_write_line_cursor(const char* line) {
     while (*line != '\0') {
           LCD_write_character_cursor(*line);
           ++line;
     }
}

void LCD_write_character_position(unsigned char row, unsigned char column, char character) {
     LCD_set_cursor(row, column);
     LCD_write_character_cursor(character);
}

void LCD_write_line_position(unsigned char row, unsigned char column, const char* line) {
     LCD_set_cursor(row, column);
     LCD_write_line_cursor(line);
}

void LCD_write_unsigned_int_cursor(unsigned int num) {
     char txt[6];
     
     WordToStr(num, txt);
     Ltrim(txt);
     Rtrim(txt);
     LCD_write_line_cursor(txt);
     
}

void LCD_write_unsigned_int_position(int row, int column, unsigned int num) {
     LCD_set_cursor(row, column);
     LCD_write_unsigned_int_cursor(num);
}

void LCD_write_signed_int_cursor(int num) {
     char txt[7];

     IntToStr(num, txt);
     Ltrim(txt);
     Rtrim(txt);
     LCD_write_line_cursor(txt);
}

void LCD_write_signed_int_position(int row, int column, int num) {
     LCD_set_cursor(row, column);
     LCD_write_signed_int_cursor(num);
}

void LCD_write_short_cursor(short num) {
     char txt[5];

     ShortToStr(num, txt);
     Ltrim(txt);
     Rtrim(txt);
     LCD_write_line_cursor(txt);
}

void LCD_write_short_position(int row, int column, short num) {
     LCD_set_cursor(row, column);
     LCD_write_short_cursor(num);
}

void LCD_write_unsigned_long_long_cursor(unsigned long long num) {
     char txt[17];

     LongLongUnsignedToHex(num, txt);
     Ltrim(txt);
     Rtrim(txt);
     LCD_write_line_cursor(txt);
}

void LCD_write_unsigned_long_long_position(int row, int column, unsigned long long num) {
     LCD_set_cursor(row, column);
     LCD_write_unsigned_long_long_cursor(num);
}

void LCD_write_float_cursor(float f) {
     char txt[15];
     
     FloatToStr(f, txt);
     Ltrim(txt);
     Rtrim(txt);
     LCD_write_line_cursor(txt);
}

void LCD_write_float_position(int row, int column, float f) {
     LCD_set_cursor(row, column);
     LCD_write_float_cursor(f);
}