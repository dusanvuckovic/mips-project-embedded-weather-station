States_write_line:
;States.c,44 :: 		static void write_line() {
SUB	SP, SP, #4
STR	LR, [SP, #0]
;States.c,45 :: 		LCD_clear_screen_top();
BL	_LCD_clear_screen_top+0
;States.c,46 :: 		LCD_write_line_position(1, 1, STATE_LINES[current_state]);
MOVW	R0, #lo_addr(_current_state+0)
MOVT	R0, #hi_addr(_current_state+0)
LDRB	R0, [R0, #0]
LSLS	R1, R0, #2
MOVW	R0, #lo_addr(_STATE_LINES+0)
MOVT	R0, #hi_addr(_STATE_LINES+0)
ADDS	R0, R0, R1
LDR	R0, [R0, #0]
MOV	R2, R0
MOVS	R1, #1
MOVS	R0, #1
BL	_LCD_write_line_position+0
;States.c,47 :: 		}
L_end_write_line:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of States_write_line
States_state_sleep:
;States.c,49 :: 		static void state_sleep() {
SUB	SP, SP, #4
;States.c,50 :: 		}
L_end_state_sleep:
ADD	SP, SP, #4
BX	LR
; end of States_state_sleep
States_state_inputa:
;States.c,51 :: 		static void state_inputa() {
SUB	SP, SP, #4
STR	LR, [SP, #0]
;States.c,52 :: 		input_float(c);
MOVW	R0, #lo_addr(_c+0)
MOVT	R0, #hi_addr(_c+0)
LDRB	R0, [R0, #0]
BL	_input_float+0
;States.c,53 :: 		}
L_end_state_inputa:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of States_state_inputa
States_state_inputb:
;States.c,54 :: 		static void state_inputb() {
SUB	SP, SP, #4
STR	LR, [SP, #0]
;States.c,55 :: 		if (new_state) {
MOVW	R0, #lo_addr(_new_state+0)
MOVT	R0, #hi_addr(_new_state+0)
LDRB	R0, [R0, #0]
CMP	R0, #0
IT	EQ
BEQ	L_States_state_inputb0
;States.c,56 :: 		value1 = calculate_float();
BL	_calculate_float+0
MOVW	R0, #lo_addr(_value1+0)
MOVT	R0, #hi_addr(_value1+0)
VSTR	#1, S0, [R0, #0]
;States.c,57 :: 		LCD_clear_screen_bottom();
BL	_LCD_clear_screen_bottom+0
;States.c,58 :: 		LCD_write_character_cursor('+');
MOVS	R0, #43
BL	_LCD_write_character_cursor+0
;States.c,59 :: 		}
IT	AL
BAL	L_States_state_inputb1
L_States_state_inputb0:
;States.c,61 :: 		input_float(c);
MOVW	R0, #lo_addr(_c+0)
MOVT	R0, #hi_addr(_c+0)
LDRB	R0, [R0, #0]
BL	_input_float+0
L_States_state_inputb1:
;States.c,63 :: 		}
L_end_state_inputb:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of States_state_inputb
States_state_measure:
;States.c,64 :: 		static void state_measure() {
SUB	SP, SP, #4
STR	LR, [SP, #0]
;States.c,66 :: 		if (new_state) {
MOVW	R0, #lo_addr(_new_state+0)
MOVT	R0, #hi_addr(_new_state+0)
LDRB	R0, [R0, #0]
CMP	R0, #0
IT	EQ
BEQ	L_States_state_measure2
;States.c,67 :: 		value2 = calculate_float();
BL	_calculate_float+0
MOVW	R0, #lo_addr(_value2+0)
MOVT	R0, #hi_addr(_value2+0)
VSTR	#1, S0, [R0, #0]
;States.c,68 :: 		LCD_clear_screen_bottom();
BL	_LCD_clear_screen_bottom+0
;States.c,69 :: 		temp_value = thermo_read_precise_temperature();
BL	_thermo_read_precise_temperature+0
MOVW	R0, #lo_addr(_temp_value+0)
MOVT	R0, #hi_addr(_temp_value+0)
VSTR	#1, S0, [R0, #0]
;States.c,70 :: 		LCD_write_float_position(2, 1, temp_value);
MOVS	R1, #1
SXTH	R1, R1
MOVS	R0, #2
SXTH	R0, R0
BL	_LCD_write_float_position+0
;States.c,71 :: 		}
L_States_state_measure2:
;States.c,72 :: 		}
L_end_state_measure:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of States_state_measure
States_state_result:
;States.c,73 :: 		static void state_result() {
SUB	SP, SP, #12
STR	LR, [SP, #0]
;States.c,75 :: 		if (new_state) {
MOVW	R0, #lo_addr(_new_state+0)
MOVT	R0, #hi_addr(_new_state+0)
LDRB	R0, [R0, #0]
CMP	R0, #0
IT	EQ
BEQ	L_States_state_result3
;States.c,76 :: 		LCD_clear_screen_bottom();
BL	_LCD_clear_screen_bottom+0
;States.c,77 :: 		diff1 = (value1 > temp_value) ? value1 - temp_value : temp_value - value1;
MOVW	R0, #lo_addr(_temp_value+0)
MOVT	R0, #hi_addr(_temp_value+0)
VLDR	#1, S1, [R0, #0]
MOVW	R0, #lo_addr(_value1+0)
MOVT	R0, #hi_addr(_value1+0)
VLDR	#1, S0, [R0, #0]
VCMPE.F32	S0, S1
VMRS	#60, FPSCR
IT	LE
BLE	L_States_state_result4
MOVW	R0, #lo_addr(_temp_value+0)
MOVT	R0, #hi_addr(_temp_value+0)
VLDR	#1, S1, [R0, #0]
MOVW	R0, #lo_addr(_value1+0)
MOVT	R0, #hi_addr(_value1+0)
VLDR	#1, S0, [R0, #0]
VSUB.F32	S0, S0, S1
; ?FLOC__States_state_result?T21 start address is: 0 (R0)
; ?FLOC__States_state_result?T21 end address is: 0 (R0)
IT	AL
BAL	L_States_state_result5
L_States_state_result4:
MOVW	R0, #lo_addr(_value1+0)
MOVT	R0, #hi_addr(_value1+0)
VLDR	#1, S1, [R0, #0]
MOVW	R0, #lo_addr(_temp_value+0)
MOVT	R0, #hi_addr(_temp_value+0)
VLDR	#1, S0, [R0, #0]
VSUB.F32	S0, S0, S1
; ?FLOC__States_state_result?T21 start address is: 4 (R1)
VMOV.F32	S1, S0
; ?FLOC__States_state_result?T21 end address is: 4 (R1)
VMOV.F32	S0, S1
L_States_state_result5:
; ?FLOC__States_state_result?T21 start address is: 0 (R0)
; diff1 start address is: 8 (R2)
VMOV.F32	S2, S0
; ?FLOC__States_state_result?T21 end address is: 0 (R0)
;States.c,78 :: 		diff2 = (value2 > temp_value) ? value2 - temp_value : temp_value - value2;
MOVW	R0, #lo_addr(_temp_value+0)
MOVT	R0, #hi_addr(_temp_value+0)
VLDR	#1, S1, [R0, #0]
MOVW	R0, #lo_addr(_value2+0)
MOVT	R0, #hi_addr(_value2+0)
VLDR	#1, S0, [R0, #0]
VCMPE.F32	S0, S1
VMRS	#60, FPSCR
IT	LE
BLE	L_States_state_result6
MOVW	R0, #lo_addr(_temp_value+0)
MOVT	R0, #hi_addr(_temp_value+0)
VLDR	#1, S1, [R0, #0]
MOVW	R0, #lo_addr(_value2+0)
MOVT	R0, #hi_addr(_value2+0)
VLDR	#1, S0, [R0, #0]
VSUB.F32	S0, S0, S1
; ?FLOC__States_state_result?T25 start address is: 0 (R0)
; ?FLOC__States_state_result?T25 end address is: 0 (R0)
IT	AL
BAL	L_States_state_result7
L_States_state_result6:
MOVW	R0, #lo_addr(_value2+0)
MOVT	R0, #hi_addr(_value2+0)
VLDR	#1, S1, [R0, #0]
MOVW	R0, #lo_addr(_temp_value+0)
MOVT	R0, #hi_addr(_temp_value+0)
VLDR	#1, S0, [R0, #0]
VSUB.F32	S0, S0, S1
; ?FLOC__States_state_result?T25 start address is: 4 (R1)
VMOV.F32	S1, S0
; ?FLOC__States_state_result?T25 end address is: 4 (R1)
VMOV.F32	S0, S1
L_States_state_result7:
; ?FLOC__States_state_result?T25 start address is: 0 (R0)
; diff2 start address is: 4 (R1)
VMOV.F32	S1, S0
; ?FLOC__States_state_result?T25 end address is: 0 (R0)
;States.c,79 :: 		LCD_write_float_cursor(value1);
MOVW	R0, #lo_addr(_value1+0)
MOVT	R0, #hi_addr(_value1+0)
VLDR	#1, S0, [R0, #0]
VSTR	#1, S1, [SP, #4]
VSTR	#1, S2, [SP, #8]
BL	_LCD_write_float_cursor+0
VLDR	#1, S2, [SP, #8]
VLDR	#1, S1, [SP, #4]
;States.c,80 :: 		if (diff1 < diff2) {
VCMPE.F32	S2, S1
VMRS	#60, FPSCR
IT	GE
BGE	L_States_state_result8
;States.c,81 :: 		LCD_write_line_position(2, 1, "A | ");
MOVW	R0, #lo_addr(?lstr_6_States+0)
MOVT	R0, #hi_addr(?lstr_6_States+0)
VSTR	#1, S1, [SP, #4]
VSTR	#1, S2, [SP, #8]
MOV	R2, R0
MOVS	R1, #1
MOVS	R0, #2
BL	_LCD_write_line_position+0
VLDR	#1, S2, [SP, #8]
VLDR	#1, S1, [SP, #4]
;States.c,82 :: 		LCD_write_float_cursor(diff2-diff1);
VSUB.F32	S0, S1, S2
; diff1 end address is: 8 (R2)
; diff2 end address is: 4 (R1)
BL	_LCD_write_float_cursor+0
;States.c,83 :: 		}
IT	AL
BAL	L_States_state_result9
L_States_state_result8:
;States.c,84 :: 		else if (diff1 > diff2) {
; diff2 start address is: 4 (R1)
; diff1 start address is: 8 (R2)
VCMPE.F32	S2, S1
VMRS	#60, FPSCR
IT	LE
BLE	L_States_state_result10
;States.c,85 :: 		LCD_write_line_position(2, 1, "B | ");
MOVW	R0, #lo_addr(?lstr_7_States+0)
MOVT	R0, #hi_addr(?lstr_7_States+0)
VSTR	#1, S1, [SP, #4]
VSTR	#1, S2, [SP, #8]
MOV	R2, R0
MOVS	R1, #1
MOVS	R0, #2
BL	_LCD_write_line_position+0
VLDR	#1, S2, [SP, #8]
VLDR	#1, S1, [SP, #4]
;States.c,86 :: 		LCD_write_float_cursor(diff1-diff2);
VSUB.F32	S0, S2, S1
; diff1 end address is: 8 (R2)
; diff2 end address is: 4 (R1)
BL	_LCD_write_float_cursor+0
;States.c,87 :: 		}
IT	AL
BAL	L_States_state_result11
L_States_state_result10:
;States.c,89 :: 		LCD_write_line_position(2, 1, "Nereseno!");
MOVW	R0, #lo_addr(?lstr_8_States+0)
MOVT	R0, #hi_addr(?lstr_8_States+0)
MOV	R2, R0
MOVS	R1, #1
MOVS	R0, #2
BL	_LCD_write_line_position+0
L_States_state_result11:
L_States_state_result9:
;States.c,90 :: 		}
L_States_state_result3:
;States.c,91 :: 		}
L_end_state_result:
LDR	LR, [SP, #0]
ADD	SP, SP, #12
BX	LR
; end of States_state_result
States_next_state:
;States.c,93 :: 		static state_function next_state(char c) {
; c start address is: 0 (R0)
SUB	SP, SP, #8
STR	LR, [SP, #0]
; c end address is: 0 (R0)
; c start address is: 0 (R0)
;States.c,96 :: 		if (c != 'D' && c != 'A')
CMP	R0, #68
IT	EQ
BEQ	L_States_next_state21
CMP	R0, #65
IT	EQ
BEQ	L_States_next_state20
; c end address is: 0 (R0)
L_States_next_state19:
;States.c,97 :: 		return current_state_ptr;
MOVW	R1, #lo_addr(_current_state_ptr+0)
MOVT	R1, #hi_addr(_current_state_ptr+0)
LDR	R0, [R1, #0]
IT	AL
BAL	L_end_next_state
;States.c,96 :: 		if (c != 'D' && c != 'A')
L_States_next_state21:
; c start address is: 0 (R0)
L_States_next_state20:
;States.c,99 :: 		if (c == 'A')
CMP	R0, #65
IT	NE
BNE	L_States_next_state15
; c end address is: 0 (R0)
;States.c,100 :: 		interrupt_no = 1;
; interrupt_no start address is: 0 (R0)
MOVS	R0, #1
SXTH	R0, R0
; interrupt_no end address is: 0 (R0)
IT	AL
BAL	L_States_next_state16
L_States_next_state15:
;States.c,102 :: 		interrupt_no = 0;
; interrupt_no start address is: 0 (R0)
MOVS	R0, #0
SXTH	R0, R0
; interrupt_no end address is: 0 (R0)
L_States_next_state16:
;States.c,103 :: 		current_state = STATE_ENUMS[current_state][interrupt_no];
; interrupt_no start address is: 0 (R0)
MOVW	R3, #lo_addr(_current_state+0)
MOVT	R3, #hi_addr(_current_state+0)
STR	R3, [SP, #4]
LDRB	R1, [R3, #0]
LSLS	R2, R1, #1
MOVW	R1, #lo_addr(_STATE_ENUMS+0)
MOVT	R1, #hi_addr(_STATE_ENUMS+0)
ADDS	R1, R1, R2
ADDS	R1, R1, R0
; interrupt_no end address is: 0 (R0)
LDRB	R1, [R1, #0]
STRB	R1, [R3, #0]
;States.c,104 :: 		write_line();
BL	States_write_line+0
;States.c,105 :: 		LCD_clear_screen_bottom();
BL	_LCD_clear_screen_bottom+0
;States.c,106 :: 		new_state = true;
MOVS	R2, #1
MOVW	R1, #lo_addr(_new_state+0)
MOVT	R1, #hi_addr(_new_state+0)
STRB	R2, [R1, #0]
;States.c,107 :: 		return STATE_PTRS[current_state];
LDR	R1, [SP, #4]
LDRB	R1, [R1, #0]
LSLS	R2, R1, #2
MOVW	R1, #lo_addr(_STATE_PTRS+0)
MOVT	R1, #hi_addr(_STATE_PTRS+0)
ADDS	R1, R1, R2
LDR	R1, [R1, #0]
MOV	R0, R1
;States.c,108 :: 		}
L_end_next_state:
LDR	LR, [SP, #0]
ADD	SP, SP, #8
BX	LR
; end of States_next_state
_states_initialize:
;States.c,110 :: 		void states_initialize() {
SUB	SP, SP, #4
STR	LR, [SP, #0]
;States.c,111 :: 		current_state = SLEEP_ST;
MOVS	R1, #0
MOVW	R0, #lo_addr(_current_state+0)
MOVT	R0, #hi_addr(_current_state+0)
STRB	R1, [R0, #0]
;States.c,112 :: 		current_state_ptr = &state_sleep;
MOVW	R1, #lo_addr(States_state_sleep+0)
MOVT	R1, #hi_addr(States_state_sleep+0)
MOVW	R0, #lo_addr(_current_state_ptr+0)
MOVT	R0, #hi_addr(_current_state_ptr+0)
STR	R1, [R0, #0]
;States.c,113 :: 		new_state = true;
MOVS	R1, #1
MOVW	R0, #lo_addr(_new_state+0)
MOVT	R0, #hi_addr(_new_state+0)
STRB	R1, [R0, #0]
;States.c,114 :: 		write_line();
BL	States_write_line+0
;States.c,115 :: 		LCD_clear_screen_bottom();
BL	_LCD_clear_screen_bottom+0
;States.c,116 :: 		}
L_end_states_initialize:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _states_initialize
States_get_character:
;States.c,118 :: 		static char get_character() {
SUB	SP, SP, #4
STR	LR, [SP, #0]
;States.c,119 :: 		char c = '$';
;States.c,120 :: 		c = keypad_get_pressed_button_interrupt();
BL	_keypad_get_pressed_button_interrupt+0
; c start address is: 0 (R0)
;States.c,121 :: 		Delay_ms(20);
MOVW	R7, #15827
MOVT	R7, #14
NOP
NOP
L_States_get_character17:
SUBS	R7, R7, #1
BNE	L_States_get_character17
NOP
NOP
NOP
NOP
;States.c,122 :: 		return c;
; c end address is: 0 (R0)
;States.c,123 :: 		}
L_end_get_character:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of States_get_character
_interrupt_handler:
;States.c,125 :: 		void interrupt_handler() {
SUB	SP, SP, #4
STR	LR, [SP, #0]
;States.c,126 :: 		DisableInterrupts();
BL	_DisableInterrupts+0
;States.c,127 :: 		c = get_character();
BL	States_get_character+0
MOVW	R1, #lo_addr(_c+0)
MOVT	R1, #hi_addr(_c+0)
STRB	R0, [R1, #0]
;States.c,128 :: 		current_state_ptr = next_state(c);
BL	States_next_state+0
MOVW	R4, #lo_addr(_current_state_ptr+0)
MOVT	R4, #hi_addr(_current_state_ptr+0)
STR	R0, [R4, #0]
;States.c,129 :: 		(*current_state_ptr)();
LDR	R4, [R4, #0]
BLX	R4
;States.c,130 :: 		new_state = false;
MOVS	R1, #0
MOVW	R0, #lo_addr(_new_state+0)
MOVT	R0, #hi_addr(_new_state+0)
STRB	R1, [R0, #0]
;States.c,132 :: 		EnableInterrupts();
BL	_EnableInterrupts+0
;States.c,133 :: 		}
L_end_interrupt_handler:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _interrupt_handler
States____?ag:
SUB	SP, SP, #4
L_end_States___?ag:
ADD	SP, SP, #4
BX	LR
; end of States____?ag
