#include <MIPS.h>

//microprocessor code
void interrupt_initialize() {
     SYSCFGEN_bit = 1;                                 //enable int
     
     
     //PC0, PC1, PC2, PC3
     SYSCFG_EXTICR1 |= 0x2222;
     EXTI_RTSR |= 0xf;
     EXTI_IMR |= 0xf;
     NVIC_IntEnable(IVT_INT_EXTI0);
     NVIC_IntEnable(IVT_INT_EXTI1);
     NVIC_IntEnable(IVT_INT_EXTI2);
     NVIC_IntEnable(IVT_INT_EXTI3);

}

void cpu_sleep() {
     asm {
         wfi;
     }
}

void interrupt_EXTI0() iv IVT_INT_EXTI0 ics ICS_AUTO {

     interrupt_handler();
     EXTI_PR.B0 = 1;
     return;
}

void interrupt_EXTI1() iv IVT_INT_EXTI1 ics ICS_AUTO {

     interrupt_handler();
     EXTI_PR.B1 = 1;
     return;
}

void interrupt_EXTI2() iv IVT_INT_EXTI2 ics ICS_AUTO {

     interrupt_handler();
     EXTI_PR.B2 = 1;
     return;
}

void interrupt_EXTI3() iv IVT_INT_EXTI3 ics ICS_AUTO {

     interrupt_handler();
     EXTI_PR.B3 = 1;
     return;
}

//microprocessor code


// main function
void main() {
     
     LCD_initialize();
     keypad_initialize(true);
     interrupt_initialize();
     states_initialize();
     
     while (true) {
          cpu_sleep();
     }
     
}