_interrupt_initialize:
;MIPS.c,4 :: 		void interrupt_initialize() {
SUB	SP, SP, #4
STR	LR, [SP, #0]
;MIPS.c,5 :: 		SYSCFGEN_bit = 1;                                 //enable int
MOVS	R1, #1
SXTB	R1, R1
MOVW	R0, #lo_addr(SYSCFGEN_bit+0)
MOVT	R0, #hi_addr(SYSCFGEN_bit+0)
STR	R1, [R0, #0]
;MIPS.c,9 :: 		SYSCFG_EXTICR1 |= 0x2222;
MOVW	R0, #lo_addr(SYSCFG_EXTICR1+0)
MOVT	R0, #hi_addr(SYSCFG_EXTICR1+0)
LDR	R1, [R0, #0]
MOVW	R0, #8738
ORRS	R1, R0
MOVW	R0, #lo_addr(SYSCFG_EXTICR1+0)
MOVT	R0, #hi_addr(SYSCFG_EXTICR1+0)
STR	R1, [R0, #0]
;MIPS.c,10 :: 		EXTI_RTSR |= 0xf;
MOVW	R0, #lo_addr(EXTI_RTSR+0)
MOVT	R0, #hi_addr(EXTI_RTSR+0)
LDR	R0, [R0, #0]
ORR	R1, R0, #15
MOVW	R0, #lo_addr(EXTI_RTSR+0)
MOVT	R0, #hi_addr(EXTI_RTSR+0)
STR	R1, [R0, #0]
;MIPS.c,11 :: 		EXTI_IMR |= 0xf;
MOVW	R0, #lo_addr(EXTI_IMR+0)
MOVT	R0, #hi_addr(EXTI_IMR+0)
LDR	R0, [R0, #0]
ORR	R1, R0, #15
MOVW	R0, #lo_addr(EXTI_IMR+0)
MOVT	R0, #hi_addr(EXTI_IMR+0)
STR	R1, [R0, #0]
;MIPS.c,12 :: 		NVIC_IntEnable(IVT_INT_EXTI0);
MOVW	R0, #22
BL	_NVIC_IntEnable+0
;MIPS.c,13 :: 		NVIC_IntEnable(IVT_INT_EXTI1);
MOVW	R0, #23
BL	_NVIC_IntEnable+0
;MIPS.c,14 :: 		NVIC_IntEnable(IVT_INT_EXTI2);
MOVW	R0, #24
BL	_NVIC_IntEnable+0
;MIPS.c,15 :: 		NVIC_IntEnable(IVT_INT_EXTI3);
MOVW	R0, #25
BL	_NVIC_IntEnable+0
;MIPS.c,17 :: 		}
L_end_interrupt_initialize:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _interrupt_initialize
_cpu_sleep:
;MIPS.c,19 :: 		void cpu_sleep() {
SUB	SP, SP, #4
;MIPS.c,21 :: 		wfi;
WFI
;MIPS.c,23 :: 		}
L_end_cpu_sleep:
ADD	SP, SP, #4
BX	LR
; end of _cpu_sleep
_interrupt_EXTI0:
;MIPS.c,25 :: 		void interrupt_EXTI0() iv IVT_INT_EXTI0 ics ICS_AUTO {
SUB	SP, SP, #4
STR	LR, [SP, #0]
;MIPS.c,27 :: 		interrupt_handler();
BL	_interrupt_handler+0
;MIPS.c,28 :: 		EXTI_PR.B0 = 1;
MOVS	R1, #1
SXTB	R1, R1
MOVW	R0, #lo_addr(EXTI_PR+0)
MOVT	R0, #hi_addr(EXTI_PR+0)
STR	R1, [R0, #0]
;MIPS.c,29 :: 		return;
;MIPS.c,30 :: 		}
L_end_interrupt_EXTI0:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _interrupt_EXTI0
_interrupt_EXTI1:
;MIPS.c,32 :: 		void interrupt_EXTI1() iv IVT_INT_EXTI1 ics ICS_AUTO {
SUB	SP, SP, #4
STR	LR, [SP, #0]
;MIPS.c,34 :: 		interrupt_handler();
BL	_interrupt_handler+0
;MIPS.c,35 :: 		EXTI_PR.B1 = 1;
MOVS	R1, #1
SXTB	R1, R1
MOVW	R0, #lo_addr(EXTI_PR+0)
MOVT	R0, #hi_addr(EXTI_PR+0)
STR	R1, [R0, #0]
;MIPS.c,36 :: 		return;
;MIPS.c,37 :: 		}
L_end_interrupt_EXTI1:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _interrupt_EXTI1
_interrupt_EXTI2:
;MIPS.c,39 :: 		void interrupt_EXTI2() iv IVT_INT_EXTI2 ics ICS_AUTO {
SUB	SP, SP, #4
STR	LR, [SP, #0]
;MIPS.c,41 :: 		interrupt_handler();
BL	_interrupt_handler+0
;MIPS.c,42 :: 		EXTI_PR.B2 = 1;
MOVS	R1, #1
SXTB	R1, R1
MOVW	R0, #lo_addr(EXTI_PR+0)
MOVT	R0, #hi_addr(EXTI_PR+0)
STR	R1, [R0, #0]
;MIPS.c,43 :: 		return;
;MIPS.c,44 :: 		}
L_end_interrupt_EXTI2:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _interrupt_EXTI2
_interrupt_EXTI3:
;MIPS.c,46 :: 		void interrupt_EXTI3() iv IVT_INT_EXTI3 ics ICS_AUTO {
SUB	SP, SP, #4
STR	LR, [SP, #0]
;MIPS.c,48 :: 		interrupt_handler();
BL	_interrupt_handler+0
;MIPS.c,49 :: 		EXTI_PR.B3 = 1;
MOVS	R1, #1
SXTB	R1, R1
MOVW	R0, #lo_addr(EXTI_PR+0)
MOVT	R0, #hi_addr(EXTI_PR+0)
STR	R1, [R0, #0]
;MIPS.c,50 :: 		return;
;MIPS.c,51 :: 		}
L_end_interrupt_EXTI3:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _interrupt_EXTI3
_main:
;MIPS.c,57 :: 		void main() {
SUB	SP, SP, #4
;MIPS.c,59 :: 		LCD_initialize();
BL	_LCD_initialize+0
;MIPS.c,60 :: 		keypad_initialize(true);
MOVS	R0, #1
BL	_keypad_initialize+0
;MIPS.c,61 :: 		interrupt_initialize();
BL	_interrupt_initialize+0
;MIPS.c,62 :: 		states_initialize();
BL	_states_initialize+0
;MIPS.c,64 :: 		while (true) {
L_main0:
;MIPS.c,65 :: 		cpu_sleep();
BL	_cpu_sleep+0
;MIPS.c,66 :: 		}
IT	AL
BAL	L_main0
;MIPS.c,68 :: 		}
L_end_main:
L__main_end_loop:
B	L__main_end_loop
; end of _main
