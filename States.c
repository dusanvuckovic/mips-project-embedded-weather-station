#include "States.h"

extern void LCD_write_line_position(unsigned char, unsigned char, const char*);
extern void LCD_write_line_cursor(const char*);
extern void LCD_clear_screen();
extern void LCD_clear_screen_bottom();
extern void LCD_write_float_cursor(float);

extern float thermo_read_precise_temperature();

typedef enum State {SLEEP_ST, FIRST_ST, SECOND_ST, MEASURE_ST, RESULT_ST};
const char * STATE_LINES[5] = {
      "D dalje, A nazad",
      "Ubaci temp. A:",
      "Ubaci temp. B:",
      "Temperatura:",
      "Pobedio | za"
};

char c;
float value1, value2, temp_value;
bool new_state = true;

state_function STATE_PTRS[5] = {
                &state_sleep,
                &state_inputa,
                &state_inputb,
                &state_measure,
                &state_result
};
enum State STATE_ENUMS[5][2] = {
               {FIRST_ST,   SLEEP_ST},
               {SECOND_ST,  SLEEP_ST},
               {MEASURE_ST, FIRST_ST},
               {RESULT_ST,  SLEEP_ST},
               {FIRST_ST,   SLEEP_ST}
};

state_function current_state_ptr = &state_sleep;
enum State     current_state = SLEEP_ST, 
               last_state    = SLEEP_ST;


static void write_line() {
         LCD_clear_screen_top();
         LCD_write_line_position(1, 1, STATE_LINES[current_state]);
}

static void state_sleep() {
}
static void state_inputa() {
       input_float(c);
}
static void state_inputb() {
       if (new_state) {
          value1 = calculate_float();
          LCD_clear_screen_bottom();
          LCD_write_character_cursor('+');
       }
       else 
            input_float(c);

}
static void state_measure() {

       if (new_state) {
          value2 = calculate_float();
          LCD_clear_screen_bottom();
          temp_value = thermo_read_precise_temperature();
          LCD_write_float_position(2, 1, temp_value);
       }
}
static void state_result() {
     float diff1, diff2;
     if (new_state) {
          LCD_clear_screen_bottom();
          diff1 = (value1 > temp_value) ? value1 - temp_value : temp_value - value1;
          diff2 = (value2 > temp_value) ? value2 - temp_value : temp_value - value2;
          LCD_write_float_cursor(value1);
          if (diff1 < diff2) {
             LCD_write_line_position(2, 1, "A | ");
             LCD_write_float_cursor(diff2-diff1);
          }
          else if (diff1 > diff2) {
               LCD_write_line_position(2, 1, "B | ");
               LCD_write_float_cursor(diff1-diff2);
          }
          else
              LCD_write_line_position(2, 1, "Nereseno!");
     }
}

static state_function next_state(char c) {
       int interrupt_no;

       if (c != 'D' && c != 'A')
          return current_state_ptr;

       if (c == 'A')
         interrupt_no = 1;
       else
          interrupt_no = 0;
       current_state = STATE_ENUMS[current_state][interrupt_no];
       write_line();
       LCD_clear_screen_bottom();
       new_state = true;
       return STATE_PTRS[current_state];
}

void states_initialize() {
     current_state = SLEEP_ST;
     current_state_ptr = &state_sleep;
     new_state = true;
     write_line();
     LCD_clear_screen_bottom();
}

static char get_character() {
     char c = '$';
     c = keypad_get_pressed_button_interrupt();
     Delay_ms(20);
     return c;
}

void interrupt_handler() {
     DisableInterrupts();
     c = get_character();
     current_state_ptr = next_state(c);
     (*current_state_ptr)();
     new_state = false;

     EnableInterrupts();
}