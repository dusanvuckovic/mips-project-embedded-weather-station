#ifndef _BUTTONCHECK_H_
#define _BUTTONCHECK_H_

static const BUTTON_TIME = 2;
static const NUM_OF_STATES = 5;

typedef struct {
       bool oldstate;
       unsigned char* current_state;
       unsigned int* port;
       unsigned int pin;
} ButtonCheck;

void buttoncheck_new(ButtonCheck* bc, unsigned int *port, unsigned int pin, unsigned char* state) {
     bc->oldstate = false;
     bc->current_state = state;
     bc->port = port;
     bc->pin = pin;
}

/*inline void buttoncheck_check(ButtonCheck *bc) {
     if (Button(bc->port, bc->pin, BUTTON_TIME, 0)) {
        bc->oldstate = true;
     }
     if (bc->oldstate && Button(bc->port, bc->pin, BUTTON_TIME, 1)) {
        bc->oldstate = false;
        bc->current_state = (bc->current_state + 1) % NUM_OF_STATES;
     }
}
*/


#endif