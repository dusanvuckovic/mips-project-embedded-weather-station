#include "Thermometer.h"
#include "Thermometer_Constants.h"
#include <stdbool.h>

void set_as_input() {
       GPIO_Config(thermo_port, thermo_pinmask,
            _GPIO_CFG_MODE_INPUT   |
            _GPIO_CFG_OTYPE_OD     |
            _GPIO_CFG_PULL_NO      |
            _GPIO_CFG_SPEED_50MHZ
       );
}

void set_as_output() {
     GPIO_Config(thermo_port, thermo_pinmask,
          _GPIO_CFG_MODE_OUTPUT |
          _GPIO_CFG_OTYPE_OD    |
          _GPIO_CFG_PULL_NO     |
          _GPIO_CFG_SPEED_50MHZ
     );
}

unsigned char thermo_read_bit() {
     unsigned char value = 7;

     THERMO_OUT = 0;
     set_as_output();
     Delay_us(5);
     
     set_as_input();
     Delay_us(10);
     value = THERMO_IN;
     Delay_us(50);
     
     return value;
}

void thermo_write_zero_bit() {
     THERMO_OUT = 0;
     set_as_output();
     Delay_us(65);
     
     set_as_input();
     Delay_us(5);
}

void thermo_write_one_bit() {
     THERMO_OUT = 0;
     set_as_output();
     Delay_us(5);

     set_as_input();
     Delay_us(55);
}

void thermo_write_bit(unsigned char c) {
     if (c)
        thermo_write_one_bit();
     else
        thermo_write_zero_bit();
}

void thermo_write(unsigned char write_data) {
     int i;
     for (i = 0; i < 8; i++) {
         thermo_write_bit(write_data & 1); //LSB
         write_data >>= 1;
     }
}

unsigned char thermo_read() {
     unsigned char result = 0, i;
     for (i = 0; i < 8; i++)
         result |= (thermo_read_bit() << i); //LSB
     return result;
}

unsigned short thermo_reset() {
         unsigned short value = 0xFF;

         THERMO_OUT = 0;
         set_as_output();
         Delay_us(720);
         set_as_input();
         Delay_us(55);

         value = THERMO_IN;
         Delay_us(400);

         return value;
}

unsigned short thermo_get_family_code() {
         thermo_reset();
         thermo_write(READ_ROM); //write 0x33
         return thermo_read();  //read family code;
}

unsigned long long thermo_get_serial_number() {
         int i;
         unsigned long long number = 0, temp = 0;
         thermo_get_family_code(); //first steps to serial
         for (i = 0; i < 6; i++) {
             temp = thermo_read();
             number |= (temp << (i*8));
         }
         return number;
}

static void thermo_measure_temperature() {
     thermo_reset();
     thermo_write(SKIP_ROM);
     thermo_write(CONVERT_T);
     Delay_ms(1000);
}

float calculate_result(unsigned char lower, unsigned char higher) {
      float val = lower >> 1;
      
      if (lower & 1) {
            if (higher == 0)
               val += 0.5;
            else
               val -= 0.5;
      }
      
      return val;

}

float thermo_read_temperature() {
         unsigned char lower, higher;
         float result;
         
         thermo_measure_temperature();
         
         thermo_reset();
         thermo_write(SKIP_ROM);
         thermo_write(READ_SCRATCHPAD);
         lower = thermo_read();
         higher = thermo_read();
         thermo_reset();
         
         result = lower >> 1;
         if (lower & 1 && higher == 0x00)
            result += 0.5;
         /*if (lower & 1 && higher == 0xFF)
            result -= 0.5;*/
         return result;
}

float thermo_read_precise_temperature() {
      unsigned char lower, higher, temp_read, count_remain, count_per_c;
      
      thermo_measure_temperature();
      
      thermo_reset();
      thermo_write(SKIP_ROM);
      thermo_write(READ_SCRATCHPAD);
      lower = thermo_read();
      higher = thermo_read();
      thermo_read(); //th
      thermo_read(); //tl
      thermo_read(); //reserved
      thermo_read(); //reserved
      count_remain = thermo_read();
      count_per_c = thermo_read();
      
      temp_read = ((short)lower >> 1);
      
      return temp_read - 0.25 + ((float)(count_per_c - count_remain) / count_per_c);
}

unsigned char calc_crc(unsigned char buffer[]) {
   int i, j;
   unsigned char shift_reg=0, data_bit, shift_bit;

   for (i=0; i<8; i++) { //for every byte
      for (j=0; j<8; j++) { //for every bit
         data_bit = (buffer[i]>>j)&0x01;
         shift_bit = shift_reg & 0x01;
         shift_reg >>= 1;
         if (data_bit ^ shift_bit)
            shift_reg = shift_reg ^ 0x8c;
      }
   }
   return shift_reg;
}

bool thermo_check_crc() {
     int i;
     unsigned char buffer[8], crc;
     thermo_reset();
     thermo_write(SKIP_ROM);
     thermo_write(READ_SCRATCHPAD);
     for (i = 0; i < 8; i++)
         buffer[i] = thermo_read();
     crc = thermo_read();
     return (crc == calc_crc(buffer));
}