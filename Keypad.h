#ifndef _KEYPAD_H_
#define _KEYPAD_H_

#include <stdbool.h>

extern unsigned long* KEYPAD_C1_PORT;
extern unsigned long* KEYPAD_C2_PORT;
extern unsigned long* KEYPAD_C3_PORT;
extern unsigned long* KEYPAD_C4_PORT;
extern unsigned long* KEYPAD_R1_PORT;
extern unsigned long* KEYPAD_R2_PORT;
extern unsigned long* KEYPAD_R3_PORT;
extern unsigned long* KEYPAD_R4_PORT;
//declarations of keyboard ports (rows/columns 1-4)

//declaration of keyboard pinmasks (rows/columns 1-4)
extern signed int KEYPAD_C1_PINMASK,
                  KEYPAD_C2_PINMASK,
                  KEYPAD_C3_PINMASK,
                  KEYPAD_C4_PINMASK,
                  KEYPAD_R1_PINMASK,
                  KEYPAD_R2_PINMASK,
                  KEYPAD_R3_PINMASK,
                  KEYPAD_R4_PINMASK;
//declaration of keyboard pinmasks (rows/columns 1-4)

//declaration of keyboard bits (rows/columns 1-4)
extern sbit KEYPAD_C1;
extern sbit KEYPAD_C2;
extern sbit KEYPAD_C3;
extern sbit KEYPAD_C4;
extern sbit KEYPAD_R1;
extern sbit KEYPAD_R2;
extern sbit KEYPAD_R3;
extern sbit KEYPAD_R4;
//declaration of keyboard bits (rows/columns 1-4)

static void set_column(unsigned int column_no);
static void clear_column(unsigned int column_no);
static char check_rows();
static char set_column_and_check(unsigned int column_no);
static void lower_columns();
static void raise_columns();
static char get_pressed_button_singular();

void keypad_initialize(bool interrupt_mode);

char keypad_get_pressed_button();
char keypad_get_pressed_button_interrupt();
char keypad_block_until_pressed_any();
void keypad_block_until_pressed_character(char to_be_pressed);

#endif