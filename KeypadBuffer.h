#ifndef KEYPADBUFFER_H_
#define KEYPADBUFFER_H_

#include <stdbool.h>

static void add_to_string(unsigned char ch);
static void remove_from_string();
static float m_pow(int exponent);
static void clear_state();

float calculate_float();
float input_float_blocking();
void input_float(char c);

#endif