#line 1 "C:/Users/Dusan/Desktop/clicker 2 for STM32 Project/KeypadBuffer.c"
#line 1 "c:/users/dusan/desktop/clicker 2 for stm32 project/keypadbuffer.h"
#line 1 "c:/mikroelektronika/mikroc pro for arm/include/stdbool.h"



 typedef char _Bool;
#line 6 "c:/users/dusan/desktop/clicker 2 for stm32 project/keypadbuffer.h"
static void add_to_string(unsigned char ch);
static void remove_from_string();
static float m_pow(int exponent);
static void clear_state();

float calculate_float();
float input_float_blocking();
void input_float(char c);
#line 1 "c:/mikroelektronika/mikroc pro for arm/include/stdbool.h"
#line 5 "C:/Users/Dusan/Desktop/clicker 2 for STM32 Project/KeypadBuffer.c"
extern void LCD_set_cursor_to_row(unsigned char);
extern void LCD_clear_screen_bottom();
extern void LCD_write_character_position(unsigned char, unsigned char, unsigned char);
extern void LCD_write_line_cursor(const char*);

extern unsigned char keypad_block_until_pressed_any();


static unsigned char PERIOD = '.';
static unsigned char input_string[16] = "\0";
static unsigned int position = 0, to_the_left = 0, to_the_right = 0;
static  _Bool  decimal_in =  0 , is_negative =  0 ;



static void add_to_string(unsigned char ch) {
 if (position == 15)
 return;
 if (ch == PERIOD) {
 if (decimal_in)
 return;
 else
 decimal_in =  1 ;
 }
 else if (!decimal_in && to_the_left == 3)
 return;
 else if (decimal_in && to_the_right == 4)
 return;
 else if (decimal_in)
 ++to_the_right;
 else
 ++to_the_left;

 input_string[position++] = ch;
 input_string[position] = '\0';
}

static void remove_from_string() {
 if (!position)
 return;
 if (input_string[--position] == PERIOD)
 decimal_in =  0 ;
 else if (decimal_in)
 --to_the_right;
 else
 --to_the_left;

 input_string[position] = '\0';
}

static float m_pow(int exponent) {
 if (!exponent)
 return 1;
 return pow(10, exponent);
}

static void clear_state() {
 input_string[0] = '\0';
 position = to_the_left = to_the_right = 0;
 decimal_in = is_negative =  0 ;
}

float calculate_float() {
 double result = 0;
 int i = 0, j = 0;

 for (i = 0; i < to_the_left; i++) {
 result += (input_string[i] - '0') * m_pow(to_the_left - 1 - i);
 }
 if (decimal_in)
 for (j = i+1; j < i+1+to_the_right; j++)
 result += (input_string[j] - '0') * m_pow(-(j-to_the_left));
 if (is_negative)
 result = -result;
 clear_state();
 return result;
}

float input_float_blocking() {
 unsigned char c;
 float ret_val;
  _Bool  input_done =  0 , change_made =  0 ;
 LCD_set_cursor_to_row(2);

 while (!input_done) {
 c = keypad_block_until_pressed_any();
 switch (c) {
 case '$': case 'A': case 'D':
 break;
 case '#':
 input_done =  1 ; break;
 case '*':
 add_to_string(PERIOD); change_made =  1 ; break;
 case 'B':
 remove_from_string(); change_made =  1 ; break;
 case 'C':
 is_negative = !is_negative; change_made =  1 ; break;
 default:
 add_to_string(c); change_made =  1 ; break;
 }
 if (change_made) {
 LCD_clear_screen_bottom();
 if (is_negative)
 LCD_write_character_position(2, 1, '-');
 else
 LCD_write_character_position(2, 1, '+');
 LCD_write_line_cursor(input_string);
 change_made =  0 ;
 }
 }
 ret_val = calculate_float();
 return ret_val;
}

void input_float(char c) {
 switch (c) {
 case '*':
 add_to_string(PERIOD); break;
 case 'B':
 remove_from_string(); break;
 case 'C':
 is_negative = !is_negative; break;
 case '#': case 'A': case 'D': return;
 default:
 add_to_string(c); break;
 }
 LCD_clear_screen_bottom();
 if (is_negative)
 LCD_write_character_position(2, 1, '-');
 else
 LCD_write_character_position(2, 1, '+');
 LCD_write_line_cursor(input_string);
}
