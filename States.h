#ifndef STATES_H_
#define STATES_H_

#include "LCD.h"

typedef void (*state_function)(void);

static void state_sleep(void);
static void state_inputa(void);
static void state_inputb(void);
static void state_measure(void);
static void state_result(void);

static void write_line();
static state_function next_state(char c);
static char get_character();

void states_initialize();
void interrupt_handler(char c);

extern char keypad_get_pressed_button_interrupt();
extern void input_float(unsigned char);
extern float calculate_float();
extern bool float_OK();

#endif