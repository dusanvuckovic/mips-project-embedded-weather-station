#ifndef LCD_CONSTANTS_H_
#define LCD_CONSTANTS_H_

//LCD ports
unsigned long *LCD_RS_PORT = &GPIOE_BASE,
              *LCD_EN_PORT = &GPIOE_BASE,
              *LCD_D4_PORT = &GPIOE_BASE,
              *LCD_D5_PORT = &GPIOE_BASE,
              *LCD_D6_PORT = &GPIOE_BASE,
              *LCD_D7_PORT = &GPIOB_BASE;
//LCD ports

//LCD pinmasks
signed int    LCD_RS_PINMASK = _GPIO_PINMASK_1,
              LCD_EN_PINMASK = _GPIO_PINMASK_2,
              LCD_D4_PINMASK = _GPIO_PINMASK_3,
              LCD_D5_PINMASK = _GPIO_PINMASK_4,
              LCD_D6_PINMASK = _GPIO_PINMASK_6,
              LCD_D7_PINMASK = _GPIO_PINMASK_6;
//LCD pinmasks

// LCD bit positions
sbit LCD_RS at GPIOE_ODR.B1;
sbit LCD_EN at GPIOE_ODR.B2;
sbit LCD_D4 at GPIOE_ODR.B3;
sbit LCD_D5 at GPIOE_ODR.B4;
sbit LCD_D6 at GPIOE_ODR.B6;
sbit LCD_D7 at GPIOB_ODR.B6;
//LCD bit positions

#endif