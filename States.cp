#line 1 "C:/Users/Dusan/Desktop/clicker 2 for STM32 Project/States.c"
#line 1 "c:/users/dusan/desktop/clicker 2 for stm32 project/states.h"
#line 1 "c:/users/dusan/desktop/clicker 2 for stm32 project/lcd.h"
#line 1 "c:/mikroelektronika/mikroc pro for arm/include/stdbool.h"



 typedef char _Bool;
#line 6 "c:/users/dusan/desktop/clicker 2 for stm32 project/lcd.h"
typedef unsigned long* port;
typedef signed int pinmask;


extern port LCD_RS_PORT;
extern port LCD_EN_PORT;
extern port LCD_D4_PORT;
extern port LCD_D5_PORT;
extern port LCD_D6_PORT;
extern port LCD_D7_PORT;




extern pinmask LCD_RS_PINMASK;
extern pinmask LCD_EN_PINMASK;
extern pinmask LCD_D4_PINMASK;
extern pinmask LCD_D5_PINMASK;
extern pinmask LCD_D6_PINMASK;
extern pinmask LCD_D7_PINMASK;



extern sbit LCD_RS;
extern sbit LCD_EN;
extern sbit LCD_D4;
extern sbit LCD_D5;
extern sbit LCD_D6;
extern sbit LCD_D7;


typedef char Lcd_Instruction;
typedef char nibble;
static const Lcd_Instruction ERROR_INSTRUCTION = 0xff;


static char upper_nibble(Lcd_Instruction ins);
static char lower_nibble(Lcd_Instruction ins);
static void nibble_to_data_pins(char nibble);

static Lcd_Instruction CLEAR_DISPLAY();
static Lcd_Instruction RETURN_HOME();
static Lcd_Instruction ENTRY_MODE_SET( _Bool  increment,  _Bool  shift);
static Lcd_Instruction DISPLAY_CONTROL( _Bool  display_on,  _Bool  cursor_on,  _Bool  cursor_blink);
static Lcd_Instruction CURSOR_OR_DISPLAY_SHIFT( _Bool  shift_instead_of_move,  _Bool  to_the_right);
static Lcd_Instruction FUNCTION_SET();
static Lcd_Instruction SET_DDRAM_ADDRESS(unsigned char address);

static void pulse();
static void send_nibble(nibble n);
static void send_instruction(Lcd_Instruction ins);



void LCD_initialize();
void LCD_clear_screen();
void LCD_clear_screen_top();
void LCD_clear_screen_bottom();
void LCD_turn_screen_off();
void LCD_turn_screen_on();
void LCD_turn_underline_off();
void LCD_turn_underline_on();
void LCD_turn_blinking_off();
void LCD_turn_blinking_on();
void LCD_command_device( _Bool  screen_on,  _Bool  underline_on,  _Bool  blinking_on);

void LCD_shift_screen_right();
void LCD_shift_screen_left();
void LCD_shift_screen( _Bool  right);
void LCD_set_cursor(unsigned char row, unsigned char column);
void LCD_set_cursor_to_row(unsigned char line);

void LCD_write_character_cursor(char character);
void LCD_write_line_cursor(const char* line);
void LCD_write_character_position(unsigned char row, unsigned char column, char character);
void LCD_write_line_position(unsigned char row, unsigned char column, const char* line);
void LCD_write_unsigned_int_cursor(unsigned int num);
void LCD_write_unsigned_int_position(int row, int column, unsigned int num);
void LCD_write_signed_int_cursor(int num);
void LCD_write_signed_int_position(int row, int column, int num);
void LCD_write_float_cursor(float f);
void LCD_write_float_position(int row, int column, float f);
#line 6 "c:/users/dusan/desktop/clicker 2 for stm32 project/states.h"
typedef void (*state_function)(void);

static void state_sleep(void);
static void state_inputa(void);
static void state_inputb(void);
static void state_measure(void);
static void state_result(void);

static void write_line();
static state_function next_state(char c);
static char get_character();

void states_initialize();
void interrupt_handler(char c);

extern char keypad_get_pressed_button_interrupt();
extern void input_float(unsigned char);
extern float calculate_float();
extern  _Bool  float_OK();
#line 3 "C:/Users/Dusan/Desktop/clicker 2 for STM32 Project/States.c"
extern void LCD_write_line_position(unsigned char, unsigned char, const char*);
extern void LCD_write_line_cursor(const char*);
extern void LCD_clear_screen();
extern void LCD_clear_screen_bottom();
extern void LCD_write_float_cursor(float);

extern float thermo_read_precise_temperature();

typedef enum State {SLEEP_ST, FIRST_ST, SECOND_ST, MEASURE_ST, RESULT_ST};
const char * STATE_LINES[5] = {
 "D dalje, A nazad",
 "Ubaci temp. A:",
 "Ubaci temp. B:",
 "Temperatura:",
 "Pobedio | za"
};

char c;
float value1, value2, temp_value;
 _Bool  new_state =  1 ;

state_function STATE_PTRS[5] = {
 &state_sleep,
 &state_inputa,
 &state_inputb,
 &state_measure,
 &state_result
};
enum State STATE_ENUMS[5][2] = {
 {FIRST_ST, SLEEP_ST},
 {SECOND_ST, SLEEP_ST},
 {MEASURE_ST, FIRST_ST},
 {RESULT_ST, SLEEP_ST},
 {FIRST_ST, SLEEP_ST}
};

state_function current_state_ptr = &state_sleep;
enum State current_state = SLEEP_ST,
 last_state = SLEEP_ST;


static void write_line() {
 LCD_clear_screen_top();
 LCD_write_line_position(1, 1, STATE_LINES[current_state]);
}

static void state_sleep() {
}
static void state_inputa() {
 input_float(c);
}
static void state_inputb() {
 if (new_state) {
 value1 = calculate_float();
 LCD_clear_screen_bottom();
 LCD_write_character_cursor('+');
 }
 else
 input_float(c);

}
static void state_measure() {

 if (new_state) {
 value2 = calculate_float();
 LCD_clear_screen_bottom();
 temp_value = thermo_read_precise_temperature();
 LCD_write_float_position(2, 1, temp_value);
 }
}
static void state_result() {
 float diff1, diff2;
 if (new_state) {
 LCD_clear_screen_bottom();
 diff1 = (value1 > temp_value) ? value1 - temp_value : temp_value - value1;
 diff2 = (value2 > temp_value) ? value2 - temp_value : temp_value - value2;
 LCD_write_float_cursor(value1);
 if (diff1 < diff2) {
 LCD_write_line_position(2, 1, "A | ");
 LCD_write_float_cursor(diff2-diff1);
 }
 else if (diff1 > diff2) {
 LCD_write_line_position(2, 1, "B | ");
 LCD_write_float_cursor(diff1-diff2);
 }
 else
 LCD_write_line_position(2, 1, "Nereseno!");
 }
}

static state_function next_state(char c) {
 int interrupt_no;

 if (c != 'D' && c != 'A')
 return current_state_ptr;

 if (c == 'A')
 interrupt_no = 1;
 else
 interrupt_no = 0;
 current_state = STATE_ENUMS[current_state][interrupt_no];
 write_line();
 LCD_clear_screen_bottom();
 new_state =  1 ;
 return STATE_PTRS[current_state];
}

void states_initialize() {
 current_state = SLEEP_ST;
 current_state_ptr = &state_sleep;
 new_state =  1 ;
 write_line();
 LCD_clear_screen_bottom();
}

static char get_character() {
 char c = '$';
 c = keypad_get_pressed_button_interrupt();
 Delay_ms(20);
 return c;
}

void interrupt_handler() {
 DisableInterrupts();
 c = get_character();
 current_state_ptr = next_state(c);
 (*current_state_ptr)();
 new_state =  0 ;

 EnableInterrupts();
}
