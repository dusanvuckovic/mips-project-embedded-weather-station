#line 1 "C:/Users/Dusan/Desktop/clicker 2 for STM32 Project/Thermometer.c"
#line 1 "c:/users/dusan/desktop/clicker 2 for stm32 project/thermometer.h"
#line 1 "c:/mikroelektronika/mikroc pro for arm/include/stdbool.h"



 typedef char _Bool;
#line 1 "c:/users/dusan/desktop/clicker 2 for stm32 project/thermometer.h"
#line 12 "c:/users/dusan/desktop/clicker 2 for stm32 project/thermometer.h"
extern sbit THERMO_OUT;
extern sbit THERMO_IN;

extern unsigned long *thermo_port;
extern signed int thermo_pinmask;


static void set_as_input();
static void set_as_output();
static unsigned char thermo_read_bit();

static void thermo_write_zero_bit();
static void thermo_write_one_bit();
static void thermo_write_bit(unsigned char c);

static void thermo_write(unsigned char write_data);
static unsigned char thermo_read();
static unsigned short thermo_reset();

static float calculate_result(unsigned char lower, unsigned char higher);
static void thermo_measure_temperature();
static unsigned char calc_crc(unsigned char buffer[]);



unsigned short thermo_get_family_code();
unsigned long long thermo_get_serial_number();
float thermo_read_temperature();
float thermo_read_precise_temperature();
 _Bool  thermo_check_crc();
#line 1 "c:/users/dusan/desktop/clicker 2 for stm32 project/thermometer_constants.h"



unsigned long *thermo_port = &GPIOB_BASE;
signed int thermo_pinmask = _GPIO_PINMASK_5;

sbit THERMO_OUT at ODR5_GPIOB_ODR_bit;
sbit THERMO_IN at IDR5_GPIOB_IDR_bit;
#line 1 "c:/mikroelektronika/mikroc pro for arm/include/stdbool.h"
#line 5 "C:/Users/Dusan/Desktop/clicker 2 for STM32 Project/Thermometer.c"
void set_as_input() {
 GPIO_Config(thermo_port, thermo_pinmask,
 _GPIO_CFG_MODE_INPUT |
 _GPIO_CFG_OTYPE_OD |
 _GPIO_CFG_PULL_NO |
 _GPIO_CFG_SPEED_50MHZ
 );
}

void set_as_output() {
 GPIO_Config(thermo_port, thermo_pinmask,
 _GPIO_CFG_MODE_OUTPUT |
 _GPIO_CFG_OTYPE_OD |
 _GPIO_CFG_PULL_NO |
 _GPIO_CFG_SPEED_50MHZ
 );
}

unsigned char thermo_read_bit() {
 unsigned char value = 7;

 THERMO_OUT = 0;
 set_as_output();
 Delay_us(5);

 set_as_input();
 Delay_us(10);
 value = THERMO_IN;
 Delay_us(50);

 return value;
}

void thermo_write_zero_bit() {
 THERMO_OUT = 0;
 set_as_output();
 Delay_us(65);

 set_as_input();
 Delay_us(5);
}

void thermo_write_one_bit() {
 THERMO_OUT = 0;
 set_as_output();
 Delay_us(5);

 set_as_input();
 Delay_us(55);
}

void thermo_write_bit(unsigned char c) {
 if (c)
 thermo_write_one_bit();
 else
 thermo_write_zero_bit();
}

void thermo_write(unsigned char write_data) {
 int i;
 for (i = 0; i < 8; i++) {
 thermo_write_bit(write_data & 1);
 write_data >>= 1;
 }
}

unsigned char thermo_read() {
 unsigned char result = 0, i;
 for (i = 0; i < 8; i++)
 result |= (thermo_read_bit() << i);
 return result;
}

unsigned short thermo_reset() {
 unsigned short value = 0xFF;

 THERMO_OUT = 0;
 set_as_output();
 Delay_us(720);
 set_as_input();
 Delay_us(55);

 value = THERMO_IN;
 Delay_us(400);

 return value;
}

unsigned short thermo_get_family_code() {
 thermo_reset();
 thermo_write( (0x33) );
 return thermo_read();
}

unsigned long long thermo_get_serial_number() {
 int i;
 unsigned long long number = 0, temp = 0;
 thermo_get_family_code();
 for (i = 0; i < 6; i++) {
 temp = thermo_read();
 number |= (temp << (i*8));
 }
 return number;
}

static void thermo_measure_temperature() {
 thermo_reset();
 thermo_write( (0xCC) );
 thermo_write( (0x44) );
 Delay_ms(1000);
}

float calculate_result(unsigned char lower, unsigned char higher) {
 float val = lower >> 1;

 if (lower & 1) {
 if (higher == 0)
 val += 0.5;
 else
 val -= 0.5;
 }

 return val;

}

float thermo_read_temperature() {
 unsigned char lower, higher;
 float result;

 thermo_measure_temperature();

 thermo_reset();
 thermo_write( (0xCC) );
 thermo_write( (0xBE) );
 lower = thermo_read();
 higher = thermo_read();
 thermo_reset();

 result = lower >> 1;
 if (lower & 1 && higher == 0x00)
 result += 0.5;
#line 149 "C:/Users/Dusan/Desktop/clicker 2 for STM32 Project/Thermometer.c"
 return result;
}

float thermo_read_precise_temperature() {
 unsigned char lower, higher, temp_read, count_remain, count_per_c;

 thermo_measure_temperature();

 thermo_reset();
 thermo_write( (0xCC) );
 thermo_write( (0xBE) );
 lower = thermo_read();
 higher = thermo_read();
 thermo_read();
 thermo_read();
 thermo_read();
 thermo_read();
 count_remain = thermo_read();
 count_per_c = thermo_read();

 temp_read = ((short)lower >> 1);

 return temp_read - 0.25 + ((float)(count_per_c - count_remain) / count_per_c);
}

unsigned char calc_crc(unsigned char buffer[]) {
 int i, j;
 unsigned char shift_reg=0, data_bit, shift_bit;

 for (i=0; i<8; i++) {
 for (j=0; j<8; j++) {
 data_bit = (buffer[i]>>j)&0x01;
 shift_bit = shift_reg & 0x01;
 shift_reg >>= 1;
 if (data_bit ^ shift_bit)
 shift_reg = shift_reg ^ 0x8c;
 }
 }
 return shift_reg;
}

 _Bool  thermo_check_crc() {
 int i;
 unsigned char buffer[8], crc;
 thermo_reset();
 thermo_write( (0xCC) );
 thermo_write( (0xBE) );
 for (i = 0; i < 8; i++)
 buffer[i] = thermo_read();
 crc = thermo_read();
 return (crc == calc_crc(buffer));
}
