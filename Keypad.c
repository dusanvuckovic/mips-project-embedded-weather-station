#include "Keypad.h"
#include "Keypad_Constants.h"

//declarations of keyboard ports (rows/columns 1-4)

static const char NO_DETECTION = '$';
static const unsigned int DEBOUNCE_INIT[4] = {11000, 7000, 4500, 3250};
static unsigned int debounce_check = DEBOUNCE_INIT[0];
static char current_row = 0, current_column = 0;
static char last_pressed = NO_DETECTION;

const char KEYPAD[4][4] = {
      {'1', '2', '3', 'A'},
      {'4', '5', '6', 'B'},
      {'7', '8', '9', 'C'},
      {'*', '0', '#', 'D'}
};

void keypad_initialize(bool interrupt_mode) {

     //enable clocks on ports
     GPIO_Clk_Enable(KEYPAD_R1_PORT);
     GPIO_Clk_Enable(KEYPAD_R2_PORT);
     GPIO_Clk_Enable(KEYPAD_R3_PORT);
     GPIO_Clk_Enable(KEYPAD_R4_PORT);
     GPIO_Clk_Enable(KEYPAD_C1_PORT);
     GPIO_Clk_Enable(KEYPAD_C2_PORT);
     GPIO_Clk_Enable(KEYPAD_C3_PORT);
     GPIO_Clk_Enable(KEYPAD_C4_PORT);
     
     GPIO_Digital_Input(KEYPAD_R1_PORT, KEYPAD_R1_PINMASK);
     GPIO_Digital_Input(KEYPAD_R2_PORT, KEYPAD_R2_PINMASK);
     GPIO_Digital_Input(KEYPAD_R3_PORT, KEYPAD_R3_PINMASK);
     GPIO_Digital_Input(KEYPAD_R4_PORT, KEYPAD_R4_PINMASK);

     //set rows as inputs, complete with pulldown
     GPIO_Config(KEYPAD_R1_PORT, KEYPAD_R1_PINMASK, _GPIO_CFG_PULL_DOWN);
     GPIO_Config(KEYPAD_R2_PORT, KEYPAD_R2_PINMASK, _GPIO_CFG_PULL_DOWN);
     GPIO_Config(KEYPAD_R3_PORT, KEYPAD_R3_PINMASK, _GPIO_CFG_PULL_DOWN);
     GPIO_Config(KEYPAD_R4_PORT, KEYPAD_R4_PINMASK, _GPIO_CFG_PULL_DOWN);

     //set columns as outputs
     GPIO_Digital_Output(KEYPAD_C1_PORT, KEYPAD_C1_PINMASK);
     GPIO_Digital_Output(KEYPAD_C2_PORT, KEYPAD_C2_PINMASK);
     GPIO_Digital_Output(KEYPAD_C3_PORT, KEYPAD_C3_PINMASK);
     GPIO_Digital_Output(KEYPAD_C4_PORT, KEYPAD_C4_PINMASK);
     
     if (interrupt_mode)
         raise_columns();
     else
         lower_columns();
}

static inline void reset_debounce() {
       debounce_check = DEBOUNCE_INIT[current_column];
}

static void set_column(unsigned int column_no) {
       if (!column_no || column_no > 4)
          return;
       switch (column_no) {
              case 1: KEYPAD_C1 = 1; break;
              case 2: KEYPAD_C2 = 1; break;
              case 3: KEYPAD_C3 = 1; break;
              case 4: KEYPAD_C4 = 1; break;
              default: break;
       }
       current_column = column_no - 1;
}
static void clear_column(unsigned int column_no) {
       if (!column_no || column_no > 4)
          return;
       switch (column_no) {
              case 1: KEYPAD_C1 = 0; break;
              case 2: KEYPAD_C2 = 0; break;
              case 3: KEYPAD_C3 = 0; break;
              case 4: KEYPAD_C4 = 0; break;
              default: break;
       }
       //current_column = 4;
}

static char check_rows() {
     if (KEYPAD_R1 == 1)
        return KEYPAD[0][current_column];
     if (KEYPAD_R2 == 1)
        return KEYPAD[1][current_column];
     if (KEYPAD_R3 == 1)
          return KEYPAD[2][current_column];
     if (KEYPAD_R4 == 1)
          return KEYPAD[3][current_column];
     return NO_DETECTION;
}

static char set_column_and_check(unsigned int column_no) {
       char pressed = NO_DETECTION;
       
       set_column(column_no);
       pressed = check_rows();
       clear_column(column_no);

       return pressed;
}

static char get_pressed_button_singular() {
     char pressed = NO_DETECTION;
     
     pressed = set_column_and_check(1);

     if (pressed != NO_DETECTION)
        return pressed;

     pressed = set_column_and_check(2);
     
     if (pressed != NO_DETECTION)
        return pressed;

     pressed = set_column_and_check(3);
     
     if (pressed != NO_DETECTION)
        return pressed;

     pressed = set_column_and_check(4);

     return pressed;
}

static void lower_columns() {
     KEYPAD_C1 = KEYPAD_C2 = KEYPAD_C3 = KEYPAD_C4 = 0;
}
static void raise_columns() {
     KEYPAD_C1 = KEYPAD_C2 = KEYPAD_C3 = KEYPAD_C4 = 1;
}

char keypad_get_pressed_button() {
     char new_pressed = get_pressed_button_singular();
     if (new_pressed != NO_DETECTION) {
        if (new_pressed == last_pressed) {
           --debounce_check;
           if (!debounce_check) {
              reset_debounce();
              return new_pressed;
           }
           else
               return NO_DETECTION;
        }
        else {
             reset_debounce();
             last_pressed = new_pressed;
             return NO_DETECTION;
        }
     }
     else {
         reset_debounce();
         return NO_DETECTION;
     }

     return new_pressed;
}

char keypad_get_pressed_button_interrupt() {
     char pressed = '$';

     lower_columns();
     while (pressed == '$')
           pressed = keypad_get_pressed_button();
     raise_columns();
     return pressed;
}

char keypad_block_until_pressed_any() {
     char pressed = NO_DETECTION;
     while (pressed == NO_DETECTION)
           pressed = keypad_get_pressed_button();
     return pressed;
}

void keypad_block_until_pressed_character(char to_be_pressed) {
     char pressed = NO_DETECTION;
     while (pressed != to_be_pressed)
           pressed = keypad_get_pressed_button();
}