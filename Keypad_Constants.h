#ifndef KEYPAD_CONSTANTS_H_
#define KEYPAD_CONSTANTS_H_

//declarations of keyboard ports (rows/columns 1-4)
unsigned long *KEYPAD_R1_PORT = &GPIOC_BASE;
unsigned long *KEYPAD_R2_PORT = &GPIOC_BASE;
unsigned long *KEYPAD_R3_PORT = &GPIOC_BASE;
unsigned long *KEYPAD_R4_PORT = &GPIOC_BASE;
unsigned long *KEYPAD_C1_PORT = &GPIOB_BASE;
unsigned long *KEYPAD_C2_PORT = &GPIOA_BASE;
unsigned long *KEYPAD_C3_PORT = &GPIOC_BASE;
unsigned long *KEYPAD_C4_PORT = &GPIOD_BASE;
//declarations of keyboard ports (rows/columns 1-4)

//declaration of keyboard pinmasks (rows/columns 1-4)
signed int    KEYPAD_R1_PINMASK = _GPIO_PINMASK_0,
              KEYPAD_R2_PINMASK = _GPIO_PINMASK_1,
              KEYPAD_R3_PINMASK = _GPIO_PINMASK_2,
              KEYPAD_R4_PINMASK = _GPIO_PINMASK_3,
              KEYPAD_C1_PINMASK = _GPIO_PINMASK_1,
              KEYPAD_C2_PINMASK = _GPIO_PINMASK_4,
              KEYPAD_C3_PINMASK = _GPIO_PINMASK_4,
              KEYPAD_C4_PINMASK = _GPIO_PINMASK_3;
//declaration of keyboard pinmasks (rows/columns 1-4)

//declaration of keyboard bits (rows/columns 1-4)
sbit KEYPAD_R1 at GPIOC_IDR.B0;
sbit KEYPAD_R2 at GPIOC_IDR.B1;
sbit KEYPAD_R3 at GPIOC_IDR.B2;
sbit KEYPAD_R4 at GPIOC_IDR.B3;
sbit KEYPAD_C1 at GPIOB_ODR.B1;
sbit KEYPAD_C2 at GPIOA_ODR.B4;
sbit KEYPAD_C3 at GPIOC_ODR.B4;
sbit KEYPAD_C4 at GPIOD_ODR.B3;
//declaration of keyboard bits (rows/columns 1-4)

#endif