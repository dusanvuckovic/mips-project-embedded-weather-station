LCD_upper_nibble:
;LCD.c,8 :: 		static char upper_nibble(Lcd_Instruction ins) {
; ins start address is: 0 (R0)
SUB	SP, SP, #4
; ins end address is: 0 (R0)
; ins start address is: 0 (R0)
;LCD.c,9 :: 		return (ins >> 4) & 0xf;
LSRS	R1, R0, #4
UXTB	R1, R1
; ins end address is: 0 (R0)
AND	R1, R1, #15
UXTB	R0, R1
;LCD.c,10 :: 		}
L_end_upper_nibble:
ADD	SP, SP, #4
BX	LR
; end of LCD_upper_nibble
LCD_lower_nibble:
;LCD.c,11 :: 		static char lower_nibble(Lcd_Instruction ins) {
; ins start address is: 0 (R0)
SUB	SP, SP, #4
; ins end address is: 0 (R0)
; ins start address is: 0 (R0)
;LCD.c,12 :: 		return (ins & 0xf);
AND	R1, R0, #15
; ins end address is: 0 (R0)
UXTB	R0, R1
;LCD.c,13 :: 		}
L_end_lower_nibble:
ADD	SP, SP, #4
BX	LR
; end of LCD_lower_nibble
LCD_nibble_to_data_pins:
;LCD.c,15 :: 		static void nibble_to_data_pins(char nibble) {
; nibble start address is: 0 (R0)
SUB	SP, SP, #4
; nibble end address is: 0 (R0)
; nibble start address is: 0 (R0)
;LCD.c,16 :: 		if (nibble & 0x8)
AND	R1, R0, #8
UXTB	R1, R1
CMP	R1, #0
IT	EQ
BEQ	L_LCD_nibble_to_data_pins0
;LCD.c,17 :: 		LCD_D7 = 1;
MOVS	R2, #1
SXTB	R2, R2
MOVW	R1, #lo_addr(GPIOB_ODR+0)
MOVT	R1, #hi_addr(GPIOB_ODR+0)
STR	R2, [R1, #0]
IT	AL
BAL	L_LCD_nibble_to_data_pins1
L_LCD_nibble_to_data_pins0:
;LCD.c,19 :: 		LCD_D7 = 0;
MOVS	R2, #0
SXTB	R2, R2
MOVW	R1, #lo_addr(GPIOB_ODR+0)
MOVT	R1, #hi_addr(GPIOB_ODR+0)
STR	R2, [R1, #0]
L_LCD_nibble_to_data_pins1:
;LCD.c,20 :: 		if (nibble & 0x4)
AND	R1, R0, #4
UXTB	R1, R1
CMP	R1, #0
IT	EQ
BEQ	L_LCD_nibble_to_data_pins2
;LCD.c,21 :: 		LCD_D6 = 1;
MOVS	R2, #1
SXTB	R2, R2
MOVW	R1, #lo_addr(GPIOE_ODR+0)
MOVT	R1, #hi_addr(GPIOE_ODR+0)
STR	R2, [R1, #0]
IT	AL
BAL	L_LCD_nibble_to_data_pins3
L_LCD_nibble_to_data_pins2:
;LCD.c,23 :: 		LCD_D6 = 0;
MOVS	R2, #0
SXTB	R2, R2
MOVW	R1, #lo_addr(GPIOE_ODR+0)
MOVT	R1, #hi_addr(GPIOE_ODR+0)
STR	R2, [R1, #0]
L_LCD_nibble_to_data_pins3:
;LCD.c,24 :: 		if (nibble & 0x2)
AND	R1, R0, #2
UXTB	R1, R1
CMP	R1, #0
IT	EQ
BEQ	L_LCD_nibble_to_data_pins4
;LCD.c,25 :: 		LCD_D5 = 1;
MOVS	R2, #1
SXTB	R2, R2
MOVW	R1, #lo_addr(GPIOE_ODR+0)
MOVT	R1, #hi_addr(GPIOE_ODR+0)
STR	R2, [R1, #0]
IT	AL
BAL	L_LCD_nibble_to_data_pins5
L_LCD_nibble_to_data_pins4:
;LCD.c,27 :: 		LCD_D5 = 0;
MOVS	R2, #0
SXTB	R2, R2
MOVW	R1, #lo_addr(GPIOE_ODR+0)
MOVT	R1, #hi_addr(GPIOE_ODR+0)
STR	R2, [R1, #0]
L_LCD_nibble_to_data_pins5:
;LCD.c,28 :: 		if (nibble & 0x1)
AND	R1, R0, #1
UXTB	R1, R1
; nibble end address is: 0 (R0)
CMP	R1, #0
IT	EQ
BEQ	L_LCD_nibble_to_data_pins6
;LCD.c,29 :: 		LCD_D4 = 1;
MOVS	R2, #1
SXTB	R2, R2
MOVW	R1, #lo_addr(GPIOE_ODR+0)
MOVT	R1, #hi_addr(GPIOE_ODR+0)
STR	R2, [R1, #0]
IT	AL
BAL	L_LCD_nibble_to_data_pins7
L_LCD_nibble_to_data_pins6:
;LCD.c,31 :: 		LCD_D4 = 0;
MOVS	R2, #0
SXTB	R2, R2
MOVW	R1, #lo_addr(GPIOE_ODR+0)
MOVT	R1, #hi_addr(GPIOE_ODR+0)
STR	R2, [R1, #0]
L_LCD_nibble_to_data_pins7:
;LCD.c,32 :: 		}
L_end_nibble_to_data_pins:
ADD	SP, SP, #4
BX	LR
; end of LCD_nibble_to_data_pins
LCD_CLEAR_DISPLAY:
;LCD.c,34 :: 		static Lcd_Instruction CLEAR_DISPLAY() {
SUB	SP, SP, #4
;LCD.c,35 :: 		cursor_position = 0x00;
MOVS	R1, #0
MOVW	R0, #lo_addr(LCD_cursor_position+0)
MOVT	R0, #hi_addr(LCD_cursor_position+0)
STRB	R1, [R0, #0]
;LCD.c,36 :: 		return 0x1;
MOVS	R0, #1
;LCD.c,37 :: 		}
L_end_CLEAR_DISPLAY:
ADD	SP, SP, #4
BX	LR
; end of LCD_CLEAR_DISPLAY
LCD_RETURN_HOME:
;LCD.c,38 :: 		static Lcd_Instruction RETURN_HOME() {
SUB	SP, SP, #4
;LCD.c,39 :: 		cursor_position = 0x00;
MOVS	R1, #0
MOVW	R0, #lo_addr(LCD_cursor_position+0)
MOVT	R0, #hi_addr(LCD_cursor_position+0)
STRB	R1, [R0, #0]
;LCD.c,40 :: 		return 0x2;
MOVS	R0, #2
;LCD.c,41 :: 		}
L_end_RETURN_HOME:
ADD	SP, SP, #4
BX	LR
; end of LCD_RETURN_HOME
LCD_ENTRY_MODE_SET:
;LCD.c,42 :: 		static Lcd_Instruction ENTRY_MODE_SET(bool increment, bool shift) {
; shift start address is: 4 (R1)
; increment start address is: 0 (R0)
SUB	SP, SP, #4
UXTB	R2, R0
; shift end address is: 4 (R1)
; increment end address is: 0 (R0)
; increment start address is: 8 (R2)
; shift start address is: 4 (R1)
;LCD.c,43 :: 		Lcd_Instruction opcode = 0x4;
; opcode start address is: 0 (R0)
MOVS	R0, #4
;LCD.c,44 :: 		if (increment)
CMP	R2, #0
IT	EQ
BEQ	L_LCD_ENTRY_MODE_SET46
; increment end address is: 8 (R2)
;LCD.c,45 :: 		opcode |= 0x2;
ORR	R0, R0, #2
UXTB	R0, R0
; opcode end address is: 0 (R0)
IT	AL
BAL	L_LCD_ENTRY_MODE_SET8
L_LCD_ENTRY_MODE_SET46:
;LCD.c,44 :: 		if (increment)
;LCD.c,45 :: 		opcode |= 0x2;
L_LCD_ENTRY_MODE_SET8:
;LCD.c,46 :: 		if (shift)
; opcode start address is: 0 (R0)
CMP	R1, #0
IT	EQ
BEQ	L_LCD_ENTRY_MODE_SET47
; shift end address is: 4 (R1)
;LCD.c,47 :: 		opcode |= 0x1;
ORR	R0, R0, #1
UXTB	R0, R0
; opcode end address is: 0 (R0)
IT	AL
BAL	L_LCD_ENTRY_MODE_SET9
L_LCD_ENTRY_MODE_SET47:
;LCD.c,46 :: 		if (shift)
;LCD.c,47 :: 		opcode |= 0x1;
L_LCD_ENTRY_MODE_SET9:
;LCD.c,48 :: 		return opcode;
; opcode start address is: 0 (R0)
; opcode end address is: 0 (R0)
;LCD.c,49 :: 		}
L_end_ENTRY_MODE_SET:
ADD	SP, SP, #4
BX	LR
; end of LCD_ENTRY_MODE_SET
LCD_DISPLAY_CONTROL:
;LCD.c,50 :: 		static Lcd_Instruction DISPLAY_CONTROL(bool display_on, bool cursor_on, bool cursor_blink) {
; cursor_blink start address is: 8 (R2)
; cursor_on start address is: 4 (R1)
; display_on start address is: 0 (R0)
SUB	SP, SP, #4
; cursor_blink end address is: 8 (R2)
; cursor_on end address is: 4 (R1)
; display_on end address is: 0 (R0)
; display_on start address is: 0 (R0)
; cursor_on start address is: 4 (R1)
; cursor_blink start address is: 8 (R2)
;LCD.c,51 :: 		Lcd_Instruction opcode = 0x8;
; opcode start address is: 20 (R5)
MOVS	R5, #8
;LCD.c,52 :: 		LCD_RS = 0;
MOVS	R4, #0
SXTB	R4, R4
MOVW	R3, #lo_addr(GPIOE_ODR+0)
MOVT	R3, #hi_addr(GPIOE_ODR+0)
STR	R4, [R3, #0]
;LCD.c,53 :: 		if (display_on)
CMP	R0, #0
IT	EQ
BEQ	L_LCD_DISPLAY_CONTROL48
; display_on end address is: 0 (R0)
;LCD.c,54 :: 		opcode |= 0x4;
ORR	R0, R5, #4
UXTB	R0, R0
; opcode end address is: 20 (R5)
; opcode start address is: 0 (R0)
; opcode end address is: 0 (R0)
IT	AL
BAL	L_LCD_DISPLAY_CONTROL10
L_LCD_DISPLAY_CONTROL48:
;LCD.c,53 :: 		if (display_on)
UXTB	R0, R5
;LCD.c,54 :: 		opcode |= 0x4;
L_LCD_DISPLAY_CONTROL10:
;LCD.c,55 :: 		if (cursor_on)
; opcode start address is: 0 (R0)
CMP	R1, #0
IT	EQ
BEQ	L_LCD_DISPLAY_CONTROL49
; cursor_on end address is: 4 (R1)
;LCD.c,56 :: 		opcode |= 0x2;
ORR	R0, R0, #2
UXTB	R0, R0
; opcode end address is: 0 (R0)
IT	AL
BAL	L_LCD_DISPLAY_CONTROL11
L_LCD_DISPLAY_CONTROL49:
;LCD.c,55 :: 		if (cursor_on)
;LCD.c,56 :: 		opcode |= 0x2;
L_LCD_DISPLAY_CONTROL11:
;LCD.c,57 :: 		if (cursor_blink)
; opcode start address is: 0 (R0)
CMP	R2, #0
IT	EQ
BEQ	L_LCD_DISPLAY_CONTROL50
; cursor_blink end address is: 8 (R2)
;LCD.c,58 :: 		opcode |= 0x1;
ORR	R0, R0, #1
UXTB	R0, R0
; opcode end address is: 0 (R0)
IT	AL
BAL	L_LCD_DISPLAY_CONTROL12
L_LCD_DISPLAY_CONTROL50:
;LCD.c,57 :: 		if (cursor_blink)
;LCD.c,58 :: 		opcode |= 0x1;
L_LCD_DISPLAY_CONTROL12:
;LCD.c,59 :: 		return opcode;
; opcode start address is: 0 (R0)
; opcode end address is: 0 (R0)
;LCD.c,60 :: 		}
L_end_DISPLAY_CONTROL:
ADD	SP, SP, #4
BX	LR
; end of LCD_DISPLAY_CONTROL
LCD_CURSOR_OR_DISPLAY_SHIFT:
;LCD.c,61 :: 		static Lcd_Instruction CURSOR_OR_DISPLAY_SHIFT(bool shift_instead_of_move, bool to_the_right) {
; to_the_right start address is: 4 (R1)
; shift_instead_of_move start address is: 0 (R0)
SUB	SP, SP, #4
; to_the_right end address is: 4 (R1)
; shift_instead_of_move end address is: 0 (R0)
; shift_instead_of_move start address is: 0 (R0)
; to_the_right start address is: 4 (R1)
;LCD.c,62 :: 		Lcd_Instruction opcode = 0x10;
; opcode start address is: 16 (R4)
MOVS	R4, #16
;LCD.c,63 :: 		LCD_RS = 0;
MOVS	R3, #0
SXTB	R3, R3
MOVW	R2, #lo_addr(GPIOE_ODR+0)
MOVT	R2, #hi_addr(GPIOE_ODR+0)
STR	R3, [R2, #0]
;LCD.c,64 :: 		if (shift_instead_of_move)
CMP	R0, #0
IT	EQ
BEQ	L_LCD_CURSOR_OR_DISPLAY_SHIFT51
; shift_instead_of_move end address is: 0 (R0)
;LCD.c,65 :: 		opcode |= 0x8;
ORR	R0, R4, #8
UXTB	R0, R0
; opcode end address is: 16 (R4)
; opcode start address is: 0 (R0)
; opcode end address is: 0 (R0)
IT	AL
BAL	L_LCD_CURSOR_OR_DISPLAY_SHIFT13
L_LCD_CURSOR_OR_DISPLAY_SHIFT51:
;LCD.c,64 :: 		if (shift_instead_of_move)
UXTB	R0, R4
;LCD.c,65 :: 		opcode |= 0x8;
L_LCD_CURSOR_OR_DISPLAY_SHIFT13:
;LCD.c,66 :: 		if (to_the_right)
; opcode start address is: 0 (R0)
CMP	R1, #0
IT	EQ
BEQ	L_LCD_CURSOR_OR_DISPLAY_SHIFT52
; to_the_right end address is: 4 (R1)
;LCD.c,67 :: 		opcode |= 0x4;
ORR	R0, R0, #4
UXTB	R0, R0
; opcode end address is: 0 (R0)
IT	AL
BAL	L_LCD_CURSOR_OR_DISPLAY_SHIFT14
L_LCD_CURSOR_OR_DISPLAY_SHIFT52:
;LCD.c,66 :: 		if (to_the_right)
;LCD.c,67 :: 		opcode |= 0x4;
L_LCD_CURSOR_OR_DISPLAY_SHIFT14:
;LCD.c,68 :: 		return opcode;
; opcode start address is: 0 (R0)
; opcode end address is: 0 (R0)
;LCD.c,69 :: 		}
L_end_CURSOR_OR_DISPLAY_SHIFT:
ADD	SP, SP, #4
BX	LR
; end of LCD_CURSOR_OR_DISPLAY_SHIFT
LCD_FUNCTION_SET:
;LCD.c,70 :: 		static Lcd_Instruction FUNCTION_SET() {
SUB	SP, SP, #4
;LCD.c,77 :: 		return 0x28;
MOVS	R0, #40
;LCD.c,78 :: 		}
L_end_FUNCTION_SET:
ADD	SP, SP, #4
BX	LR
; end of LCD_FUNCTION_SET
LCD_SET_DDRAM_ADDRESS:
;LCD.c,80 :: 		static Lcd_Instruction SET_DDRAM_ADDRESS(unsigned char address) {
; address start address is: 0 (R0)
SUB	SP, SP, #4
; address end address is: 0 (R0)
; address start address is: 0 (R0)
;LCD.c,81 :: 		Lcd_Instruction opcode = 0x80;
; opcode start address is: 8 (R2)
MOVS	R2, #128
;LCD.c,82 :: 		opcode |= address;
ORR	R1, R2, R0, LSL #0
; address end address is: 0 (R0)
; opcode end address is: 8 (R2)
;LCD.c,83 :: 		return opcode;
UXTB	R0, R1
;LCD.c,84 :: 		}
L_end_SET_DDRAM_ADDRESS:
ADD	SP, SP, #4
BX	LR
; end of LCD_SET_DDRAM_ADDRESS
LCD_pulse:
;LCD.c,86 :: 		static void pulse() {
SUB	SP, SP, #4
;LCD.c,87 :: 		LCD_EN = 1;
MOVS	R1, #1
SXTB	R1, R1
MOVW	R0, #lo_addr(GPIOE_ODR+0)
MOVT	R0, #hi_addr(GPIOE_ODR+0)
STR	R1, [R0, #0]
;LCD.c,88 :: 		Delay_ms(1);
MOVW	R7, #46665
MOVT	R7, #0
NOP
NOP
L_LCD_pulse15:
SUBS	R7, R7, #1
BNE	L_LCD_pulse15
NOP
NOP
;LCD.c,89 :: 		LCD_EN = 0;
MOVS	R1, #0
SXTB	R1, R1
MOVW	R0, #lo_addr(GPIOE_ODR+0)
MOVT	R0, #hi_addr(GPIOE_ODR+0)
STR	R1, [R0, #0]
;LCD.c,90 :: 		}
L_end_pulse:
ADD	SP, SP, #4
BX	LR
; end of LCD_pulse
LCD_send_nibble:
;LCD.c,92 :: 		static void send_nibble(nibble n) {
; n start address is: 0 (R0)
SUB	SP, SP, #4
STR	LR, [SP, #0]
; n end address is: 0 (R0)
; n start address is: 0 (R0)
;LCD.c,93 :: 		Delay_ms(1);
MOVW	R7, #46665
MOVT	R7, #0
NOP
NOP
L_LCD_send_nibble17:
SUBS	R7, R7, #1
BNE	L_LCD_send_nibble17
NOP
NOP
;LCD.c,94 :: 		nibble_to_data_pins(n);
; n end address is: 0 (R0)
BL	LCD_nibble_to_data_pins+0
;LCD.c,95 :: 		pulse();
BL	LCD_pulse+0
;LCD.c,96 :: 		}
L_end_send_nibble:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of LCD_send_nibble
LCD_send_instruction:
;LCD.c,98 :: 		static void send_instruction(Lcd_Instruction ins) {
; ins start address is: 0 (R0)
SUB	SP, SP, #4
STR	LR, [SP, #0]
UXTB	R3, R0
; ins end address is: 0 (R0)
; ins start address is: 12 (R3)
;LCD.c,99 :: 		nibble first_nibble = upper_nibble(ins),
UXTB	R0, R3
BL	LCD_upper_nibble+0
; first_nibble start address is: 8 (R2)
UXTB	R2, R0
;LCD.c,100 :: 		second_nibble = lower_nibble(ins);
UXTB	R0, R3
; ins end address is: 12 (R3)
BL	LCD_lower_nibble+0
; second_nibble start address is: 12 (R3)
UXTB	R3, R0
;LCD.c,101 :: 		send_nibble(first_nibble);
UXTB	R0, R2
; first_nibble end address is: 8 (R2)
BL	LCD_send_nibble+0
;LCD.c,102 :: 		send_nibble(second_nibble);
UXTB	R0, R3
; second_nibble end address is: 12 (R3)
BL	LCD_send_nibble+0
;LCD.c,103 :: 		}
L_end_send_instruction:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of LCD_send_instruction
_LCD_initialize:
;LCD.c,105 :: 		void LCD_initialize() {
SUB	SP, SP, #4
STR	LR, [SP, #0]
;LCD.c,107 :: 		GPIO_Clk_Enable(LCD_RS_PORT);
MOVW	R0, #lo_addr(_LCD_RS_PORT+0)
MOVT	R0, #hi_addr(_LCD_RS_PORT+0)
LDR	R0, [R0, #0]
BL	_GPIO_Clk_Enable+0
;LCD.c,108 :: 		GPIO_Clk_Enable(LCD_EN_PORT);
MOVW	R0, #lo_addr(_LCD_EN_PORT+0)
MOVT	R0, #hi_addr(_LCD_EN_PORT+0)
LDR	R0, [R0, #0]
BL	_GPIO_Clk_Enable+0
;LCD.c,109 :: 		GPIO_Clk_Enable(LCD_D7_PORT);
MOVW	R0, #lo_addr(_LCD_D7_PORT+0)
MOVT	R0, #hi_addr(_LCD_D7_PORT+0)
LDR	R0, [R0, #0]
BL	_GPIO_Clk_Enable+0
;LCD.c,110 :: 		GPIO_Clk_Enable(LCD_D6_PORT);
MOVW	R0, #lo_addr(_LCD_D6_PORT+0)
MOVT	R0, #hi_addr(_LCD_D6_PORT+0)
LDR	R0, [R0, #0]
BL	_GPIO_Clk_Enable+0
;LCD.c,111 :: 		GPIO_Clk_Enable(LCD_D5_PORT);
MOVW	R0, #lo_addr(_LCD_D5_PORT+0)
MOVT	R0, #hi_addr(_LCD_D5_PORT+0)
LDR	R0, [R0, #0]
BL	_GPIO_Clk_Enable+0
;LCD.c,112 :: 		GPIO_Clk_Enable(LCD_D4_PORT);
MOVW	R0, #lo_addr(_LCD_D4_PORT+0)
MOVT	R0, #hi_addr(_LCD_D4_PORT+0)
LDR	R0, [R0, #0]
BL	_GPIO_Clk_Enable+0
;LCD.c,114 :: 		GPIO_Digital_Output(LCD_RS_PORT, LCD_RS_PINMASK);
MOVW	R0, #lo_addr(_LCD_RS_PINMASK+0)
MOVT	R0, #hi_addr(_LCD_RS_PINMASK+0)
LDRSH	R1, [R0, #0]
MOVW	R0, #lo_addr(_LCD_RS_PORT+0)
MOVT	R0, #hi_addr(_LCD_RS_PORT+0)
LDR	R0, [R0, #0]
BL	_GPIO_Digital_Output+0
;LCD.c,115 :: 		GPIO_Digital_Output(LCD_EN_PORT, LCD_EN_PINMASK);
MOVW	R0, #lo_addr(_LCD_EN_PINMASK+0)
MOVT	R0, #hi_addr(_LCD_EN_PINMASK+0)
LDRSH	R1, [R0, #0]
MOVW	R0, #lo_addr(_LCD_EN_PORT+0)
MOVT	R0, #hi_addr(_LCD_EN_PORT+0)
LDR	R0, [R0, #0]
BL	_GPIO_Digital_Output+0
;LCD.c,116 :: 		GPIO_Digital_Output(LCD_D7_PORT, LCD_D7_PINMASK);
MOVW	R0, #lo_addr(_LCD_D7_PINMASK+0)
MOVT	R0, #hi_addr(_LCD_D7_PINMASK+0)
LDRSH	R1, [R0, #0]
MOVW	R0, #lo_addr(_LCD_D7_PORT+0)
MOVT	R0, #hi_addr(_LCD_D7_PORT+0)
LDR	R0, [R0, #0]
BL	_GPIO_Digital_Output+0
;LCD.c,117 :: 		GPIO_Digital_Output(LCD_D6_PORT, LCD_D6_PINMASK);
MOVW	R0, #lo_addr(_LCD_D6_PINMASK+0)
MOVT	R0, #hi_addr(_LCD_D6_PINMASK+0)
LDRSH	R1, [R0, #0]
MOVW	R0, #lo_addr(_LCD_D6_PORT+0)
MOVT	R0, #hi_addr(_LCD_D6_PORT+0)
LDR	R0, [R0, #0]
BL	_GPIO_Digital_Output+0
;LCD.c,118 :: 		GPIO_Digital_Output(LCD_D5_PORT, LCD_D5_PINMASK);
MOVW	R0, #lo_addr(_LCD_D5_PINMASK+0)
MOVT	R0, #hi_addr(_LCD_D5_PINMASK+0)
LDRSH	R1, [R0, #0]
MOVW	R0, #lo_addr(_LCD_D5_PORT+0)
MOVT	R0, #hi_addr(_LCD_D5_PORT+0)
LDR	R0, [R0, #0]
BL	_GPIO_Digital_Output+0
;LCD.c,119 :: 		GPIO_Digital_Output(LCD_D4_PORT, LCD_D4_PINMASK);
MOVW	R0, #lo_addr(_LCD_D4_PINMASK+0)
MOVT	R0, #hi_addr(_LCD_D4_PINMASK+0)
LDRSH	R1, [R0, #0]
MOVW	R0, #lo_addr(_LCD_D4_PORT+0)
MOVT	R0, #hi_addr(_LCD_D4_PORT+0)
LDR	R0, [R0, #0]
BL	_GPIO_Digital_Output+0
;LCD.c,121 :: 		LCD_RS = LCD_EN = LCD_D7 = LCD_D6 = LCD_D5 = LCD_D4 = 0;
MOVS	R1, #0
SXTB	R1, R1
MOVW	R0, #lo_addr(GPIOE_ODR+0)
MOVT	R0, #hi_addr(GPIOE_ODR+0)
STR	R1, [R0, #0]
LDR	R1, [R0, #0]
MOVW	R0, #lo_addr(GPIOE_ODR+0)
MOVT	R0, #hi_addr(GPIOE_ODR+0)
STR	R1, [R0, #0]
LDR	R1, [R0, #0]
MOVW	R0, #lo_addr(GPIOE_ODR+0)
MOVT	R0, #hi_addr(GPIOE_ODR+0)
STR	R1, [R0, #0]
LDR	R1, [R0, #0]
MOVW	R0, #lo_addr(GPIOB_ODR+0)
MOVT	R0, #hi_addr(GPIOB_ODR+0)
STR	R1, [R0, #0]
LDR	R1, [R0, #0]
MOVW	R0, #lo_addr(GPIOE_ODR+0)
MOVT	R0, #hi_addr(GPIOE_ODR+0)
STR	R1, [R0, #0]
LDR	R1, [R0, #0]
MOVW	R0, #lo_addr(GPIOE_ODR+0)
MOVT	R0, #hi_addr(GPIOE_ODR+0)
STR	R1, [R0, #0]
;LCD.c,123 :: 		Delay_ms(50);
MOVW	R7, #39571
MOVT	R7, #35
NOP
NOP
L_LCD_initialize19:
SUBS	R7, R7, #1
BNE	L_LCD_initialize19
NOP
NOP
NOP
NOP
;LCD.c,124 :: 		send_nibble(0x3);
MOVS	R0, #3
BL	LCD_send_nibble+0
;LCD.c,125 :: 		Delay_ms(5);
MOVW	R7, #36723
MOVT	R7, #3
NOP
NOP
L_LCD_initialize21:
SUBS	R7, R7, #1
BNE	L_LCD_initialize21
NOP
NOP
NOP
NOP
;LCD.c,126 :: 		send_nibble(0x3);
MOVS	R0, #3
BL	LCD_send_nibble+0
;LCD.c,127 :: 		Delay_ms(100);
MOVW	R7, #13609
MOVT	R7, #71
NOP
NOP
L_LCD_initialize23:
SUBS	R7, R7, #1
BNE	L_LCD_initialize23
NOP
NOP
;LCD.c,128 :: 		send_nibble(0x3);
MOVS	R0, #3
BL	LCD_send_nibble+0
;LCD.c,129 :: 		send_nibble(0x2);
MOVS	R0, #2
BL	LCD_send_nibble+0
;LCD.c,131 :: 		send_instruction(FUNCTION_SET());
BL	LCD_FUNCTION_SET+0
BL	LCD_send_instruction+0
;LCD.c,132 :: 		send_instruction(DISPLAY_CONTROL(false, false, false));
MOVS	R2, #0
MOVS	R1, #0
MOVS	R0, #0
BL	LCD_DISPLAY_CONTROL+0
BL	LCD_send_instruction+0
;LCD.c,133 :: 		send_instruction(CLEAR_DISPLAY());
BL	LCD_CLEAR_DISPLAY+0
BL	LCD_send_instruction+0
;LCD.c,134 :: 		send_instruction(ENTRY_MODE_SET(true, false));
MOVS	R1, #0
MOVS	R0, #1
BL	LCD_ENTRY_MODE_SET+0
BL	LCD_send_instruction+0
;LCD.c,135 :: 		send_instruction(DISPLAY_CONTROL(true, true, true));
MOVS	R2, #1
MOVS	R1, #1
MOVS	R0, #1
BL	LCD_DISPLAY_CONTROL+0
BL	LCD_send_instruction+0
;LCD.c,137 :: 		send_instruction(CLEAR_DISPLAY());
BL	LCD_CLEAR_DISPLAY+0
BL	LCD_send_instruction+0
;LCD.c,139 :: 		screen = blink = underline = true;
MOVS	R1, #1
MOVW	R0, #lo_addr(LCD_underline+0)
MOVT	R0, #hi_addr(LCD_underline+0)
STRB	R1, [R0, #0]
MOVS	R1, #1
MOVW	R0, #lo_addr(LCD_blink+0)
MOVT	R0, #hi_addr(LCD_blink+0)
STRB	R1, [R0, #0]
MOVS	R1, #1
MOVW	R0, #lo_addr(LCD_screen+0)
MOVT	R0, #hi_addr(LCD_screen+0)
STRB	R1, [R0, #0]
;LCD.c,140 :: 		cursor_position = 0x00;
MOVS	R1, #0
MOVW	R0, #lo_addr(LCD_cursor_position+0)
MOVT	R0, #hi_addr(LCD_cursor_position+0)
STRB	R1, [R0, #0]
;LCD.c,141 :: 		}
L_end_LCD_initialize:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _LCD_initialize
_LCD_clear_screen:
;LCD.c,143 :: 		void LCD_clear_screen() {
SUB	SP, SP, #4
STR	LR, [SP, #0]
;LCD.c,144 :: 		send_instruction(CLEAR_DISPLAY());
BL	LCD_CLEAR_DISPLAY+0
BL	LCD_send_instruction+0
;LCD.c,145 :: 		}
L_end_LCD_clear_screen:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _LCD_clear_screen
_LCD_clear_screen_top:
;LCD.c,147 :: 		void LCD_clear_screen_top() {
SUB	SP, SP, #4
STR	LR, [SP, #0]
;LCD.c,148 :: 		LCD_write_line_position(1, 1, empty_string);
MOVW	R0, #lo_addr(LCD_empty_string+0)
MOVT	R0, #hi_addr(LCD_empty_string+0)
LDR	R0, [R0, #0]
MOV	R2, R0
MOVS	R1, #1
MOVS	R0, #1
BL	_LCD_write_line_position+0
;LCD.c,149 :: 		LCD_set_cursor(2, 1);
MOVS	R1, #1
MOVS	R0, #2
BL	_LCD_set_cursor+0
;LCD.c,150 :: 		}
L_end_LCD_clear_screen_top:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _LCD_clear_screen_top
_LCD_clear_screen_bottom:
;LCD.c,152 :: 		void LCD_clear_screen_bottom() {
SUB	SP, SP, #4
STR	LR, [SP, #0]
;LCD.c,153 :: 		LCD_write_line_position(2, 1, empty_string);
MOVW	R0, #lo_addr(LCD_empty_string+0)
MOVT	R0, #hi_addr(LCD_empty_string+0)
LDR	R0, [R0, #0]
MOV	R2, R0
MOVS	R1, #1
MOVS	R0, #2
BL	_LCD_write_line_position+0
;LCD.c,154 :: 		LCD_set_cursor(2, 1);
MOVS	R1, #1
MOVS	R0, #2
BL	_LCD_set_cursor+0
;LCD.c,155 :: 		}
L_end_LCD_clear_screen_bottom:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _LCD_clear_screen_bottom
_LCD_turn_screen_off:
;LCD.c,157 :: 		void LCD_turn_screen_off() {
SUB	SP, SP, #4
STR	LR, [SP, #0]
;LCD.c,158 :: 		if (!screen)
MOVW	R0, #lo_addr(LCD_screen+0)
MOVT	R0, #hi_addr(LCD_screen+0)
LDRB	R0, [R0, #0]
CMP	R0, #0
IT	NE
BNE	L_LCD_turn_screen_off25
;LCD.c,159 :: 		return;
IT	AL
BAL	L_end_LCD_turn_screen_off
L_LCD_turn_screen_off25:
;LCD.c,160 :: 		send_instruction(DISPLAY_CONTROL(false, underline, blink));
MOVW	R0, #lo_addr(LCD_blink+0)
MOVT	R0, #hi_addr(LCD_blink+0)
LDRB	R1, [R0, #0]
MOVW	R0, #lo_addr(LCD_underline+0)
MOVT	R0, #hi_addr(LCD_underline+0)
LDRB	R0, [R0, #0]
UXTB	R2, R1
UXTB	R1, R0
MOVS	R0, #0
BL	LCD_DISPLAY_CONTROL+0
BL	LCD_send_instruction+0
;LCD.c,161 :: 		screen = false;
MOVS	R1, #0
MOVW	R0, #lo_addr(LCD_screen+0)
MOVT	R0, #hi_addr(LCD_screen+0)
STRB	R1, [R0, #0]
;LCD.c,162 :: 		}
L_end_LCD_turn_screen_off:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _LCD_turn_screen_off
_LCD_turn_screen_on:
;LCD.c,164 :: 		void LCD_turn_screen_on() {
SUB	SP, SP, #4
STR	LR, [SP, #0]
;LCD.c,165 :: 		if (screen)
MOVW	R0, #lo_addr(LCD_screen+0)
MOVT	R0, #hi_addr(LCD_screen+0)
LDRB	R0, [R0, #0]
CMP	R0, #0
IT	EQ
BEQ	L_LCD_turn_screen_on26
;LCD.c,166 :: 		return;
IT	AL
BAL	L_end_LCD_turn_screen_on
L_LCD_turn_screen_on26:
;LCD.c,167 :: 		send_instruction(DISPLAY_CONTROL(true, underline, blink));
MOVW	R0, #lo_addr(LCD_blink+0)
MOVT	R0, #hi_addr(LCD_blink+0)
LDRB	R1, [R0, #0]
MOVW	R0, #lo_addr(LCD_underline+0)
MOVT	R0, #hi_addr(LCD_underline+0)
LDRB	R0, [R0, #0]
UXTB	R2, R1
UXTB	R1, R0
MOVS	R0, #1
BL	LCD_DISPLAY_CONTROL+0
BL	LCD_send_instruction+0
;LCD.c,168 :: 		screen = true;
MOVS	R1, #1
MOVW	R0, #lo_addr(LCD_screen+0)
MOVT	R0, #hi_addr(LCD_screen+0)
STRB	R1, [R0, #0]
;LCD.c,169 :: 		}
L_end_LCD_turn_screen_on:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _LCD_turn_screen_on
_LCD_turn_underline_off:
;LCD.c,171 :: 		void LCD_turn_underline_off() {
SUB	SP, SP, #4
STR	LR, [SP, #0]
;LCD.c,172 :: 		if (!underline)
MOVW	R0, #lo_addr(LCD_underline+0)
MOVT	R0, #hi_addr(LCD_underline+0)
LDRB	R0, [R0, #0]
CMP	R0, #0
IT	NE
BNE	L_LCD_turn_underline_off27
;LCD.c,173 :: 		return;
IT	AL
BAL	L_end_LCD_turn_underline_off
L_LCD_turn_underline_off27:
;LCD.c,174 :: 		send_instruction(DISPLAY_CONTROL(screen, false, blink));
MOVW	R0, #lo_addr(LCD_blink+0)
MOVT	R0, #hi_addr(LCD_blink+0)
LDRB	R1, [R0, #0]
MOVW	R0, #lo_addr(LCD_screen+0)
MOVT	R0, #hi_addr(LCD_screen+0)
LDRB	R0, [R0, #0]
UXTB	R2, R1
MOVS	R1, #0
BL	LCD_DISPLAY_CONTROL+0
BL	LCD_send_instruction+0
;LCD.c,175 :: 		underline = false;
MOVS	R1, #0
MOVW	R0, #lo_addr(LCD_underline+0)
MOVT	R0, #hi_addr(LCD_underline+0)
STRB	R1, [R0, #0]
;LCD.c,176 :: 		}
L_end_LCD_turn_underline_off:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _LCD_turn_underline_off
_LCD_turn_underline_on:
;LCD.c,178 :: 		void LCD_turn_underline_on() {
SUB	SP, SP, #4
STR	LR, [SP, #0]
;LCD.c,179 :: 		if (underline)
MOVW	R0, #lo_addr(LCD_underline+0)
MOVT	R0, #hi_addr(LCD_underline+0)
LDRB	R0, [R0, #0]
CMP	R0, #0
IT	EQ
BEQ	L_LCD_turn_underline_on28
;LCD.c,180 :: 		return;
IT	AL
BAL	L_end_LCD_turn_underline_on
L_LCD_turn_underline_on28:
;LCD.c,181 :: 		send_instruction(DISPLAY_CONTROL(screen, true, blink));
MOVW	R0, #lo_addr(LCD_blink+0)
MOVT	R0, #hi_addr(LCD_blink+0)
LDRB	R1, [R0, #0]
MOVW	R0, #lo_addr(LCD_screen+0)
MOVT	R0, #hi_addr(LCD_screen+0)
LDRB	R0, [R0, #0]
UXTB	R2, R1
MOVS	R1, #1
BL	LCD_DISPLAY_CONTROL+0
BL	LCD_send_instruction+0
;LCD.c,182 :: 		underline = true;
MOVS	R1, #1
MOVW	R0, #lo_addr(LCD_underline+0)
MOVT	R0, #hi_addr(LCD_underline+0)
STRB	R1, [R0, #0]
;LCD.c,183 :: 		}
L_end_LCD_turn_underline_on:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _LCD_turn_underline_on
_LCD_turn_blinking_off:
;LCD.c,185 :: 		void LCD_turn_blinking_off() {
SUB	SP, SP, #4
STR	LR, [SP, #0]
;LCD.c,186 :: 		if (!blink)
MOVW	R0, #lo_addr(LCD_blink+0)
MOVT	R0, #hi_addr(LCD_blink+0)
LDRB	R0, [R0, #0]
CMP	R0, #0
IT	NE
BNE	L_LCD_turn_blinking_off29
;LCD.c,187 :: 		return;
IT	AL
BAL	L_end_LCD_turn_blinking_off
L_LCD_turn_blinking_off29:
;LCD.c,188 :: 		send_instruction(DISPLAY_CONTROL(screen, underline, false));
MOVW	R0, #lo_addr(LCD_underline+0)
MOVT	R0, #hi_addr(LCD_underline+0)
LDRB	R1, [R0, #0]
MOVW	R0, #lo_addr(LCD_screen+0)
MOVT	R0, #hi_addr(LCD_screen+0)
LDRB	R0, [R0, #0]
MOVS	R2, #0
BL	LCD_DISPLAY_CONTROL+0
BL	LCD_send_instruction+0
;LCD.c,189 :: 		blink = false;
MOVS	R1, #0
MOVW	R0, #lo_addr(LCD_blink+0)
MOVT	R0, #hi_addr(LCD_blink+0)
STRB	R1, [R0, #0]
;LCD.c,190 :: 		}
L_end_LCD_turn_blinking_off:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _LCD_turn_blinking_off
_LCD_turn_blinking_on:
;LCD.c,192 :: 		void LCD_turn_blinking_on() {
SUB	SP, SP, #4
STR	LR, [SP, #0]
;LCD.c,193 :: 		if (blink)
MOVW	R0, #lo_addr(LCD_blink+0)
MOVT	R0, #hi_addr(LCD_blink+0)
LDRB	R0, [R0, #0]
CMP	R0, #0
IT	EQ
BEQ	L_LCD_turn_blinking_on30
;LCD.c,194 :: 		return;
IT	AL
BAL	L_end_LCD_turn_blinking_on
L_LCD_turn_blinking_on30:
;LCD.c,195 :: 		send_instruction(DISPLAY_CONTROL(screen, underline, true));
MOVW	R0, #lo_addr(LCD_underline+0)
MOVT	R0, #hi_addr(LCD_underline+0)
LDRB	R1, [R0, #0]
MOVW	R0, #lo_addr(LCD_screen+0)
MOVT	R0, #hi_addr(LCD_screen+0)
LDRB	R0, [R0, #0]
MOVS	R2, #1
BL	LCD_DISPLAY_CONTROL+0
BL	LCD_send_instruction+0
;LCD.c,196 :: 		blink = true;
MOVS	R1, #1
MOVW	R0, #lo_addr(LCD_blink+0)
MOVT	R0, #hi_addr(LCD_blink+0)
STRB	R1, [R0, #0]
;LCD.c,197 :: 		}
L_end_LCD_turn_blinking_on:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _LCD_turn_blinking_on
_LCD_command_device:
;LCD.c,199 :: 		void LCD_command_device(bool screen_on, bool underline_on, bool blinking_on) {
; blinking_on start address is: 8 (R2)
; underline_on start address is: 4 (R1)
; screen_on start address is: 0 (R0)
SUB	SP, SP, #4
STR	LR, [SP, #0]
; blinking_on end address is: 8 (R2)
; underline_on end address is: 4 (R1)
; screen_on end address is: 0 (R0)
; screen_on start address is: 0 (R0)
; underline_on start address is: 4 (R1)
; blinking_on start address is: 8 (R2)
;LCD.c,200 :: 		screen = screen_on;
MOVW	R3, #lo_addr(LCD_screen+0)
MOVT	R3, #hi_addr(LCD_screen+0)
STRB	R0, [R3, #0]
;LCD.c,201 :: 		underline = underline_on;
MOVW	R3, #lo_addr(LCD_underline+0)
MOVT	R3, #hi_addr(LCD_underline+0)
STRB	R1, [R3, #0]
;LCD.c,202 :: 		blink = blinking_on;
MOVW	R3, #lo_addr(LCD_blink+0)
MOVT	R3, #hi_addr(LCD_blink+0)
STRB	R2, [R3, #0]
;LCD.c,203 :: 		send_instruction(DISPLAY_CONTROL(screen, underline, blink));
UXTB	R5, R2
; blinking_on end address is: 8 (R2)
UXTB	R4, R1
; underline_on end address is: 4 (R1)
UXTB	R3, R0
; screen_on end address is: 0 (R0)
UXTB	R2, R5
UXTB	R1, R4
UXTB	R0, R3
BL	LCD_DISPLAY_CONTROL+0
BL	LCD_send_instruction+0
;LCD.c,204 :: 		}
L_end_LCD_command_device:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _LCD_command_device
_LCD_shift_screen_right:
;LCD.c,206 :: 		void LCD_shift_screen_right() {
SUB	SP, SP, #4
STR	LR, [SP, #0]
;LCD.c,207 :: 		send_instruction(CURSOR_OR_DISPLAY_SHIFT(true, true));
MOVS	R1, #1
MOVS	R0, #1
BL	LCD_CURSOR_OR_DISPLAY_SHIFT+0
BL	LCD_send_instruction+0
;LCD.c,208 :: 		}
L_end_LCD_shift_screen_right:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _LCD_shift_screen_right
_LCD_shift_screen_left:
;LCD.c,210 :: 		void LCD_shift_screen_left() {
SUB	SP, SP, #4
STR	LR, [SP, #0]
;LCD.c,211 :: 		send_instruction(CURSOR_OR_DISPLAY_SHIFT(true, false));
MOVS	R1, #0
MOVS	R0, #1
BL	LCD_CURSOR_OR_DISPLAY_SHIFT+0
BL	LCD_send_instruction+0
;LCD.c,212 :: 		}
L_end_LCD_shift_screen_left:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _LCD_shift_screen_left
_LCD_shift_screen:
;LCD.c,214 :: 		void LCD_shift_screen(bool right) {
; right start address is: 0 (R0)
SUB	SP, SP, #4
STR	LR, [SP, #0]
; right end address is: 0 (R0)
; right start address is: 0 (R0)
;LCD.c,215 :: 		send_instruction(CURSOR_OR_DISPLAY_SHIFT(false, !right));
CMP	R0, #0
MOVW	R1, #0
BNE	L__LCD_shift_screen90
MOVS	R1, #1
L__LCD_shift_screen90:
; right end address is: 0 (R0)
MOVS	R0, #0
BL	LCD_CURSOR_OR_DISPLAY_SHIFT+0
BL	LCD_send_instruction+0
;LCD.c,216 :: 		}
L_end_LCD_shift_screen:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _LCD_shift_screen
_LCD_set_cursor:
;LCD.c,218 :: 		void LCD_set_cursor(unsigned char row, unsigned char column) {
; column start address is: 4 (R1)
; row start address is: 0 (R0)
SUB	SP, SP, #4
STR	LR, [SP, #0]
; column end address is: 4 (R1)
; row end address is: 0 (R0)
; row start address is: 0 (R0)
; column start address is: 4 (R1)
;LCD.c,219 :: 		unsigned char address = column - 1;
SUBS	R2, R1, #1
; address start address is: 12 (R3)
UXTB	R3, R2
;LCD.c,220 :: 		if (row != 1 && row != 2)
CMP	R0, #1
IT	EQ
BEQ	L__LCD_set_cursor56
CMP	R0, #2
IT	EQ
BEQ	L__LCD_set_cursor55
; row end address is: 0 (R0)
; column end address is: 4 (R1)
; address end address is: 12 (R3)
L__LCD_set_cursor54:
;LCD.c,221 :: 		return;
IT	AL
BAL	L_end_LCD_set_cursor
;LCD.c,220 :: 		if (row != 1 && row != 2)
L__LCD_set_cursor56:
; address start address is: 12 (R3)
; column start address is: 4 (R1)
; row start address is: 0 (R0)
L__LCD_set_cursor55:
;LCD.c,222 :: 		if (!column || column > 16)
CMP	R1, #0
IT	EQ
BEQ	L__LCD_set_cursor58
CMP	R1, #16
IT	HI
BHI	L__LCD_set_cursor57
; column end address is: 4 (R1)
IT	AL
BAL	L_LCD_set_cursor36
; row end address is: 0 (R0)
; address end address is: 12 (R3)
L__LCD_set_cursor58:
L__LCD_set_cursor57:
;LCD.c,223 :: 		return;
IT	AL
BAL	L_end_LCD_set_cursor
L_LCD_set_cursor36:
;LCD.c,224 :: 		if (row == 2)
; address start address is: 12 (R3)
; row start address is: 0 (R0)
CMP	R0, #2
IT	NE
BNE	L__LCD_set_cursor59
; row end address is: 0 (R0)
;LCD.c,225 :: 		address |= 0x40;
ORR	R4, R3, #64
UXTB	R4, R4
; address end address is: 12 (R3)
; address start address is: 16 (R4)
; address end address is: 16 (R4)
IT	AL
BAL	L_LCD_set_cursor37
L__LCD_set_cursor59:
;LCD.c,224 :: 		if (row == 2)
UXTB	R4, R3
;LCD.c,225 :: 		address |= 0x40;
L_LCD_set_cursor37:
;LCD.c,226 :: 		send_instruction(SET_DDRAM_ADDRESS(address));
; address start address is: 16 (R4)
UXTB	R0, R4
BL	LCD_SET_DDRAM_ADDRESS+0
BL	LCD_send_instruction+0
;LCD.c,227 :: 		cursor_position = address;
MOVW	R2, #lo_addr(LCD_cursor_position+0)
MOVT	R2, #hi_addr(LCD_cursor_position+0)
STRB	R4, [R2, #0]
; address end address is: 16 (R4)
;LCD.c,228 :: 		}
L_end_LCD_set_cursor:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _LCD_set_cursor
_LCD_set_cursor_to_row:
;LCD.c,230 :: 		void LCD_set_cursor_to_row(unsigned char line) {
; line start address is: 0 (R0)
SUB	SP, SP, #4
STR	LR, [SP, #0]
; line end address is: 0 (R0)
; line start address is: 0 (R0)
;LCD.c,231 :: 		if ((line != 1) || (line != 2))
CMP	R0, #1
IT	NE
BNE	L__LCD_set_cursor_to_row62
CMP	R0, #2
IT	NE
BNE	L__LCD_set_cursor_to_row61
IT	AL
BAL	L_LCD_set_cursor_to_row40
; line end address is: 0 (R0)
L__LCD_set_cursor_to_row62:
L__LCD_set_cursor_to_row61:
;LCD.c,232 :: 		return;
IT	AL
BAL	L_end_LCD_set_cursor_to_row
L_LCD_set_cursor_to_row40:
;LCD.c,233 :: 		LCD_set_cursor(line, 0);
; line start address is: 0 (R0)
MOVS	R1, #0
; line end address is: 0 (R0)
BL	_LCD_set_cursor+0
;LCD.c,234 :: 		}
L_end_LCD_set_cursor_to_row:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _LCD_set_cursor_to_row
_LCD_write_character_cursor:
;LCD.c,236 :: 		void LCD_write_character_cursor(char character) {
; character start address is: 0 (R0)
SUB	SP, SP, #8
STR	LR, [SP, #0]
UXTB	R5, R0
; character end address is: 0 (R0)
; character start address is: 20 (R5)
;LCD.c,237 :: 		if (cursor_position == 0x10) {
MOVW	R1, #lo_addr(LCD_cursor_position+0)
MOVT	R1, #hi_addr(LCD_cursor_position+0)
LDRB	R1, [R1, #0]
CMP	R1, #16
IT	NE
BNE	L_LCD_write_character_cursor41
;LCD.c,238 :: 		LCD_set_cursor(2, 1);
MOVS	R1, #1
MOVS	R0, #2
BL	_LCD_set_cursor+0
;LCD.c,239 :: 		cursor_position = 0x40;
MOVS	R2, #64
MOVW	R1, #lo_addr(LCD_cursor_position+0)
MOVT	R1, #hi_addr(LCD_cursor_position+0)
STRB	R2, [R1, #0]
;LCD.c,240 :: 		}
IT	AL
BAL	L_LCD_write_character_cursor42
L_LCD_write_character_cursor41:
;LCD.c,241 :: 		else if (cursor_position == 0x50) {
MOVW	R1, #lo_addr(LCD_cursor_position+0)
MOVT	R1, #hi_addr(LCD_cursor_position+0)
LDRB	R1, [R1, #0]
CMP	R1, #80
IT	NE
BNE	L_LCD_write_character_cursor43
;LCD.c,242 :: 		LCD_set_cursor(1, 1);
MOVS	R1, #1
MOVS	R0, #1
BL	_LCD_set_cursor+0
;LCD.c,243 :: 		cursor_position = 0x00;
MOVS	R2, #0
MOVW	R1, #lo_addr(LCD_cursor_position+0)
MOVT	R1, #hi_addr(LCD_cursor_position+0)
STRB	R2, [R1, #0]
;LCD.c,244 :: 		}
L_LCD_write_character_cursor43:
L_LCD_write_character_cursor42:
;LCD.c,245 :: 		LCD_RS = 1;
MOVS	R2, #1
SXTB	R2, R2
MOVW	R1, #lo_addr(GPIOE_ODR+0)
MOVT	R1, #hi_addr(GPIOE_ODR+0)
STR	R1, [SP, #4]
STR	R2, [R1, #0]
;LCD.c,246 :: 		send_instruction(character);
UXTB	R0, R5
; character end address is: 20 (R5)
BL	LCD_send_instruction+0
;LCD.c,247 :: 		++cursor_position;
MOVW	R2, #lo_addr(LCD_cursor_position+0)
MOVT	R2, #hi_addr(LCD_cursor_position+0)
LDRB	R1, [R2, #0]
ADDS	R1, R1, #1
STRB	R1, [R2, #0]
;LCD.c,248 :: 		LCD_RS = 0;
MOVS	R2, #0
SXTB	R2, R2
LDR	R1, [SP, #4]
STR	R2, [R1, #0]
;LCD.c,249 :: 		}
L_end_LCD_write_character_cursor:
LDR	LR, [SP, #0]
ADD	SP, SP, #8
BX	LR
; end of _LCD_write_character_cursor
_LCD_write_line_cursor:
;LCD.c,251 :: 		void LCD_write_line_cursor(const char* line) {
; line start address is: 0 (R0)
SUB	SP, SP, #4
STR	LR, [SP, #0]
; line end address is: 0 (R0)
; line start address is: 0 (R0)
MOV	R6, R0
; line end address is: 0 (R0)
;LCD.c,252 :: 		while (*line != '\0') {
L_LCD_write_line_cursor44:
; line start address is: 24 (R6)
LDRB	R1, [R6, #0]
CMP	R1, #0
IT	EQ
BEQ	L_LCD_write_line_cursor45
;LCD.c,253 :: 		LCD_write_character_cursor(*line);
LDRB	R1, [R6, #0]
UXTB	R0, R1
BL	_LCD_write_character_cursor+0
;LCD.c,254 :: 		++line;
ADDS	R6, R6, #1
;LCD.c,255 :: 		}
; line end address is: 24 (R6)
IT	AL
BAL	L_LCD_write_line_cursor44
L_LCD_write_line_cursor45:
;LCD.c,256 :: 		}
L_end_LCD_write_line_cursor:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _LCD_write_line_cursor
_LCD_write_character_position:
;LCD.c,258 :: 		void LCD_write_character_position(unsigned char row, unsigned char column, char character) {
; character start address is: 8 (R2)
; column start address is: 4 (R1)
; row start address is: 0 (R0)
SUB	SP, SP, #4
STR	LR, [SP, #0]
UXTB	R5, R2
; character end address is: 8 (R2)
; column end address is: 4 (R1)
; row end address is: 0 (R0)
; row start address is: 0 (R0)
; column start address is: 4 (R1)
; character start address is: 20 (R5)
;LCD.c,259 :: 		LCD_set_cursor(row, column);
; column end address is: 4 (R1)
; row end address is: 0 (R0)
BL	_LCD_set_cursor+0
;LCD.c,260 :: 		LCD_write_character_cursor(character);
UXTB	R0, R5
; character end address is: 20 (R5)
BL	_LCD_write_character_cursor+0
;LCD.c,261 :: 		}
L_end_LCD_write_character_position:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _LCD_write_character_position
_LCD_write_line_position:
;LCD.c,263 :: 		void LCD_write_line_position(unsigned char row, unsigned char column, const char* line) {
; line start address is: 8 (R2)
; column start address is: 4 (R1)
; row start address is: 0 (R0)
SUB	SP, SP, #4
STR	LR, [SP, #0]
MOV	R5, R2
; line end address is: 8 (R2)
; column end address is: 4 (R1)
; row end address is: 0 (R0)
; row start address is: 0 (R0)
; column start address is: 4 (R1)
; line start address is: 20 (R5)
;LCD.c,264 :: 		LCD_set_cursor(row, column);
; column end address is: 4 (R1)
; row end address is: 0 (R0)
BL	_LCD_set_cursor+0
;LCD.c,265 :: 		LCD_write_line_cursor(line);
MOV	R0, R5
; line end address is: 20 (R5)
BL	_LCD_write_line_cursor+0
;LCD.c,266 :: 		}
L_end_LCD_write_line_position:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _LCD_write_line_position
_LCD_write_unsigned_int_cursor:
;LCD.c,268 :: 		void LCD_write_unsigned_int_cursor(unsigned int num) {
; num start address is: 0 (R0)
SUB	SP, SP, #12
STR	LR, [SP, #0]
; num end address is: 0 (R0)
; num start address is: 0 (R0)
;LCD.c,271 :: 		WordToStr(num, txt);
ADD	R1, SP, #4
; num end address is: 0 (R0)
BL	_WordToStr+0
;LCD.c,272 :: 		Ltrim(txt);
ADD	R1, SP, #4
MOV	R0, R1
BL	_Ltrim+0
;LCD.c,273 :: 		Rtrim(txt);
ADD	R1, SP, #4
MOV	R0, R1
BL	_Rtrim+0
;LCD.c,274 :: 		LCD_write_line_cursor(txt);
ADD	R1, SP, #4
MOV	R0, R1
BL	_LCD_write_line_cursor+0
;LCD.c,276 :: 		}
L_end_LCD_write_unsigned_int_cursor:
LDR	LR, [SP, #0]
ADD	SP, SP, #12
BX	LR
; end of _LCD_write_unsigned_int_cursor
_LCD_write_unsigned_int_position:
;LCD.c,278 :: 		void LCD_write_unsigned_int_position(int row, int column, unsigned int num) {
; num start address is: 8 (R2)
; column start address is: 4 (R1)
; row start address is: 0 (R0)
SUB	SP, SP, #4
STR	LR, [SP, #0]
UXTH	R5, R2
; num end address is: 8 (R2)
; column end address is: 4 (R1)
; row end address is: 0 (R0)
; row start address is: 0 (R0)
; column start address is: 4 (R1)
; num start address is: 20 (R5)
;LCD.c,279 :: 		LCD_set_cursor(row, column);
UXTB	R1, R1
; column end address is: 4 (R1)
UXTB	R0, R0
; row end address is: 0 (R0)
BL	_LCD_set_cursor+0
;LCD.c,280 :: 		LCD_write_unsigned_int_cursor(num);
UXTH	R0, R5
; num end address is: 20 (R5)
BL	_LCD_write_unsigned_int_cursor+0
;LCD.c,281 :: 		}
L_end_LCD_write_unsigned_int_position:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _LCD_write_unsigned_int_position
_LCD_write_signed_int_cursor:
;LCD.c,283 :: 		void LCD_write_signed_int_cursor(int num) {
; num start address is: 0 (R0)
SUB	SP, SP, #12
STR	LR, [SP, #0]
; num end address is: 0 (R0)
; num start address is: 0 (R0)
;LCD.c,286 :: 		IntToStr(num, txt);
ADD	R1, SP, #4
; num end address is: 0 (R0)
BL	_IntToStr+0
;LCD.c,287 :: 		Ltrim(txt);
ADD	R1, SP, #4
MOV	R0, R1
BL	_Ltrim+0
;LCD.c,288 :: 		Rtrim(txt);
ADD	R1, SP, #4
MOV	R0, R1
BL	_Rtrim+0
;LCD.c,289 :: 		LCD_write_line_cursor(txt);
ADD	R1, SP, #4
MOV	R0, R1
BL	_LCD_write_line_cursor+0
;LCD.c,290 :: 		}
L_end_LCD_write_signed_int_cursor:
LDR	LR, [SP, #0]
ADD	SP, SP, #12
BX	LR
; end of _LCD_write_signed_int_cursor
_LCD_write_signed_int_position:
;LCD.c,292 :: 		void LCD_write_signed_int_position(int row, int column, int num) {
; num start address is: 8 (R2)
; column start address is: 4 (R1)
; row start address is: 0 (R0)
SUB	SP, SP, #4
STR	LR, [SP, #0]
SXTH	R5, R2
; num end address is: 8 (R2)
; column end address is: 4 (R1)
; row end address is: 0 (R0)
; row start address is: 0 (R0)
; column start address is: 4 (R1)
; num start address is: 20 (R5)
;LCD.c,293 :: 		LCD_set_cursor(row, column);
UXTB	R1, R1
; column end address is: 4 (R1)
UXTB	R0, R0
; row end address is: 0 (R0)
BL	_LCD_set_cursor+0
;LCD.c,294 :: 		LCD_write_signed_int_cursor(num);
SXTH	R0, R5
; num end address is: 20 (R5)
BL	_LCD_write_signed_int_cursor+0
;LCD.c,295 :: 		}
L_end_LCD_write_signed_int_position:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _LCD_write_signed_int_position
_LCD_write_short_cursor:
;LCD.c,297 :: 		void LCD_write_short_cursor(short num) {
; num start address is: 0 (R0)
SUB	SP, SP, #12
STR	LR, [SP, #0]
; num end address is: 0 (R0)
; num start address is: 0 (R0)
;LCD.c,300 :: 		ShortToStr(num, txt);
ADD	R1, SP, #4
; num end address is: 0 (R0)
BL	_ShortToStr+0
;LCD.c,301 :: 		Ltrim(txt);
ADD	R1, SP, #4
MOV	R0, R1
BL	_Ltrim+0
;LCD.c,302 :: 		Rtrim(txt);
ADD	R1, SP, #4
MOV	R0, R1
BL	_Rtrim+0
;LCD.c,303 :: 		LCD_write_line_cursor(txt);
ADD	R1, SP, #4
MOV	R0, R1
BL	_LCD_write_line_cursor+0
;LCD.c,304 :: 		}
L_end_LCD_write_short_cursor:
LDR	LR, [SP, #0]
ADD	SP, SP, #12
BX	LR
; end of _LCD_write_short_cursor
_LCD_write_short_position:
;LCD.c,306 :: 		void LCD_write_short_position(int row, int column, short num) {
; num start address is: 8 (R2)
; column start address is: 4 (R1)
; row start address is: 0 (R0)
SUB	SP, SP, #4
STR	LR, [SP, #0]
SXTB	R5, R2
; num end address is: 8 (R2)
; column end address is: 4 (R1)
; row end address is: 0 (R0)
; row start address is: 0 (R0)
; column start address is: 4 (R1)
; num start address is: 20 (R5)
;LCD.c,307 :: 		LCD_set_cursor(row, column);
UXTB	R1, R1
; column end address is: 4 (R1)
UXTB	R0, R0
; row end address is: 0 (R0)
BL	_LCD_set_cursor+0
;LCD.c,308 :: 		LCD_write_short_cursor(num);
SXTB	R0, R5
; num end address is: 20 (R5)
BL	_LCD_write_short_cursor+0
;LCD.c,309 :: 		}
L_end_LCD_write_short_position:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _LCD_write_short_position
_LCD_write_unsigned_long_long_cursor:
;LCD.c,311 :: 		void LCD_write_unsigned_long_long_cursor(unsigned long long num) {
; num start address is: 0 (R0)
SUB	SP, SP, #24
STR	LR, [SP, #0]
; num end address is: 0 (R0)
; num start address is: 0 (R0)
;LCD.c,314 :: 		LongLongUnsignedToHex(num, txt);
ADD	R2, SP, #4
; num end address is: 0 (R0)
BL	_LongLongUnsignedToHex+0
;LCD.c,315 :: 		Ltrim(txt);
ADD	R2, SP, #4
MOV	R0, R2
BL	_Ltrim+0
;LCD.c,316 :: 		Rtrim(txt);
ADD	R2, SP, #4
MOV	R0, R2
BL	_Rtrim+0
;LCD.c,317 :: 		LCD_write_line_cursor(txt);
ADD	R2, SP, #4
MOV	R0, R2
BL	_LCD_write_line_cursor+0
;LCD.c,318 :: 		}
L_end_LCD_write_unsigned_long_long_cursor:
LDR	LR, [SP, #0]
ADD	SP, SP, #24
BX	LR
; end of _LCD_write_unsigned_long_long_cursor
_LCD_write_unsigned_long_long_position:
;LCD.c,320 :: 		void LCD_write_unsigned_long_long_position(int row, int column, unsigned long long num) {
; num start address is: 8 (R2)
; column start address is: 4 (R1)
; row start address is: 0 (R0)
SUB	SP, SP, #4
STR	LR, [SP, #0]
MOV	R5, R2
MOV	R6, R3
; num end address is: 8 (R2)
; column end address is: 4 (R1)
; row end address is: 0 (R0)
; row start address is: 0 (R0)
; column start address is: 4 (R1)
; num start address is: 20 (R5)
;LCD.c,321 :: 		LCD_set_cursor(row, column);
UXTB	R1, R1
; column end address is: 4 (R1)
UXTB	R0, R0
; row end address is: 0 (R0)
BL	_LCD_set_cursor+0
;LCD.c,322 :: 		LCD_write_unsigned_long_long_cursor(num);
MOV	R0, R5
MOV	R1, R6
; num end address is: 20 (R5)
BL	_LCD_write_unsigned_long_long_cursor+0
;LCD.c,323 :: 		}
L_end_LCD_write_unsigned_long_long_position:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _LCD_write_unsigned_long_long_position
_LCD_write_float_cursor:
;LCD.c,325 :: 		void LCD_write_float_cursor(float f) {
SUB	SP, SP, #20
STR	LR, [SP, #0]
; f start address is: 0 (R0)
; f end address is: 0 (R0)
; f start address is: 0 (R0)
;LCD.c,328 :: 		FloatToStr(f, txt);
ADD	R1, SP, #4
MOV	R0, R1
; f end address is: 0 (R0)
BL	_FloatToStr+0
;LCD.c,329 :: 		Ltrim(txt);
ADD	R1, SP, #4
MOV	R0, R1
BL	_Ltrim+0
;LCD.c,330 :: 		Rtrim(txt);
ADD	R1, SP, #4
MOV	R0, R1
BL	_Rtrim+0
;LCD.c,331 :: 		LCD_write_line_cursor(txt);
ADD	R1, SP, #4
MOV	R0, R1
BL	_LCD_write_line_cursor+0
;LCD.c,332 :: 		}
L_end_LCD_write_float_cursor:
LDR	LR, [SP, #0]
ADD	SP, SP, #20
BX	LR
; end of _LCD_write_float_cursor
_LCD_write_float_position:
;LCD.c,334 :: 		void LCD_write_float_position(int row, int column, float f) {
; f start address is: 0 (R0)
; column start address is: 4 (R1)
SUB	SP, SP, #4
STR	LR, [SP, #0]
; row start address is: 0 (R0)
; f end address is: 0 (R0)
; column end address is: 4 (R1)
; row end address is: 0 (R0)
; row start address is: 0 (R0)
; column start address is: 4 (R1)
; f start address is: 0 (R0)
;LCD.c,335 :: 		LCD_set_cursor(row, column);
UXTB	R1, R1
; column end address is: 4 (R1)
UXTB	R0, R0
; row end address is: 0 (R0)
BL	_LCD_set_cursor+0
;LCD.c,336 :: 		LCD_write_float_cursor(f);
; f end address is: 0 (R0)
BL	_LCD_write_float_cursor+0
;LCD.c,337 :: 		}
L_end_LCD_write_float_position:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _LCD_write_float_position
LCD____?ag:
SUB	SP, SP, #4
L_end_LCD___?ag:
ADD	SP, SP, #4
BX	LR
; end of LCD____?ag
