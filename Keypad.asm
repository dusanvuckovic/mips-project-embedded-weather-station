_keypad_initialize:
;Keypad.c,19 :: 		void keypad_initialize(bool interrupt_mode) {
; interrupt_mode start address is: 0 (R0)
SUB	SP, SP, #4
STR	LR, [SP, #0]
UXTB	R11, R0
; interrupt_mode end address is: 0 (R0)
; interrupt_mode start address is: 44 (R11)
;Keypad.c,22 :: 		GPIO_Clk_Enable(KEYPAD_R1_PORT);
MOVW	R1, #lo_addr(_KEYPAD_R1_PORT+0)
MOVT	R1, #hi_addr(_KEYPAD_R1_PORT+0)
LDR	R1, [R1, #0]
MOV	R0, R1
BL	_GPIO_Clk_Enable+0
;Keypad.c,23 :: 		GPIO_Clk_Enable(KEYPAD_R2_PORT);
MOVW	R1, #lo_addr(_KEYPAD_R2_PORT+0)
MOVT	R1, #hi_addr(_KEYPAD_R2_PORT+0)
LDR	R1, [R1, #0]
MOV	R0, R1
BL	_GPIO_Clk_Enable+0
;Keypad.c,24 :: 		GPIO_Clk_Enable(KEYPAD_R3_PORT);
MOVW	R1, #lo_addr(_KEYPAD_R3_PORT+0)
MOVT	R1, #hi_addr(_KEYPAD_R3_PORT+0)
LDR	R1, [R1, #0]
MOV	R0, R1
BL	_GPIO_Clk_Enable+0
;Keypad.c,25 :: 		GPIO_Clk_Enable(KEYPAD_R4_PORT);
MOVW	R1, #lo_addr(_KEYPAD_R4_PORT+0)
MOVT	R1, #hi_addr(_KEYPAD_R4_PORT+0)
LDR	R1, [R1, #0]
MOV	R0, R1
BL	_GPIO_Clk_Enable+0
;Keypad.c,26 :: 		GPIO_Clk_Enable(KEYPAD_C1_PORT);
MOVW	R1, #lo_addr(_KEYPAD_C1_PORT+0)
MOVT	R1, #hi_addr(_KEYPAD_C1_PORT+0)
LDR	R1, [R1, #0]
MOV	R0, R1
BL	_GPIO_Clk_Enable+0
;Keypad.c,27 :: 		GPIO_Clk_Enable(KEYPAD_C2_PORT);
MOVW	R1, #lo_addr(_KEYPAD_C2_PORT+0)
MOVT	R1, #hi_addr(_KEYPAD_C2_PORT+0)
LDR	R1, [R1, #0]
MOV	R0, R1
BL	_GPIO_Clk_Enable+0
;Keypad.c,28 :: 		GPIO_Clk_Enable(KEYPAD_C3_PORT);
MOVW	R1, #lo_addr(_KEYPAD_C3_PORT+0)
MOVT	R1, #hi_addr(_KEYPAD_C3_PORT+0)
LDR	R1, [R1, #0]
MOV	R0, R1
BL	_GPIO_Clk_Enable+0
;Keypad.c,29 :: 		GPIO_Clk_Enable(KEYPAD_C4_PORT);
MOVW	R1, #lo_addr(_KEYPAD_C4_PORT+0)
MOVT	R1, #hi_addr(_KEYPAD_C4_PORT+0)
LDR	R1, [R1, #0]
MOV	R0, R1
BL	_GPIO_Clk_Enable+0
;Keypad.c,31 :: 		GPIO_Digital_Input(KEYPAD_R1_PORT, KEYPAD_R1_PINMASK);
MOVW	R1, #lo_addr(_KEYPAD_R1_PINMASK+0)
MOVT	R1, #hi_addr(_KEYPAD_R1_PINMASK+0)
LDRSH	R2, [R1, #0]
MOVW	R1, #lo_addr(_KEYPAD_R1_PORT+0)
MOVT	R1, #hi_addr(_KEYPAD_R1_PORT+0)
LDR	R1, [R1, #0]
MOV	R0, R1
MOV	R1, R2
BL	_GPIO_Digital_Input+0
;Keypad.c,32 :: 		GPIO_Digital_Input(KEYPAD_R2_PORT, KEYPAD_R2_PINMASK);
MOVW	R1, #lo_addr(_KEYPAD_R2_PINMASK+0)
MOVT	R1, #hi_addr(_KEYPAD_R2_PINMASK+0)
LDRSH	R2, [R1, #0]
MOVW	R1, #lo_addr(_KEYPAD_R2_PORT+0)
MOVT	R1, #hi_addr(_KEYPAD_R2_PORT+0)
LDR	R1, [R1, #0]
MOV	R0, R1
MOV	R1, R2
BL	_GPIO_Digital_Input+0
;Keypad.c,33 :: 		GPIO_Digital_Input(KEYPAD_R3_PORT, KEYPAD_R3_PINMASK);
MOVW	R1, #lo_addr(_KEYPAD_R3_PINMASK+0)
MOVT	R1, #hi_addr(_KEYPAD_R3_PINMASK+0)
LDRSH	R2, [R1, #0]
MOVW	R1, #lo_addr(_KEYPAD_R3_PORT+0)
MOVT	R1, #hi_addr(_KEYPAD_R3_PORT+0)
LDR	R1, [R1, #0]
MOV	R0, R1
MOV	R1, R2
BL	_GPIO_Digital_Input+0
;Keypad.c,34 :: 		GPIO_Digital_Input(KEYPAD_R4_PORT, KEYPAD_R4_PINMASK);
MOVW	R1, #lo_addr(_KEYPAD_R4_PINMASK+0)
MOVT	R1, #hi_addr(_KEYPAD_R4_PINMASK+0)
LDRSH	R2, [R1, #0]
MOVW	R1, #lo_addr(_KEYPAD_R4_PORT+0)
MOVT	R1, #hi_addr(_KEYPAD_R4_PORT+0)
LDR	R1, [R1, #0]
MOV	R0, R1
MOV	R1, R2
BL	_GPIO_Digital_Input+0
;Keypad.c,37 :: 		GPIO_Config(KEYPAD_R1_PORT, KEYPAD_R1_PINMASK, _GPIO_CFG_PULL_DOWN);
MOVW	R1, #lo_addr(_KEYPAD_R1_PINMASK+0)
MOVT	R1, #hi_addr(_KEYPAD_R1_PINMASK+0)
LDRSH	R2, [R1, #0]
MOVW	R1, #lo_addr(_KEYPAD_R1_PORT+0)
MOVT	R1, #hi_addr(_KEYPAD_R1_PORT+0)
LDR	R1, [R1, #0]
MOV	R0, R1
UXTH	R1, R2
MOV	R2, #256
BL	_GPIO_Config+0
;Keypad.c,38 :: 		GPIO_Config(KEYPAD_R2_PORT, KEYPAD_R2_PINMASK, _GPIO_CFG_PULL_DOWN);
MOVW	R1, #lo_addr(_KEYPAD_R2_PINMASK+0)
MOVT	R1, #hi_addr(_KEYPAD_R2_PINMASK+0)
LDRSH	R2, [R1, #0]
MOVW	R1, #lo_addr(_KEYPAD_R2_PORT+0)
MOVT	R1, #hi_addr(_KEYPAD_R2_PORT+0)
LDR	R1, [R1, #0]
MOV	R0, R1
UXTH	R1, R2
MOV	R2, #256
BL	_GPIO_Config+0
;Keypad.c,39 :: 		GPIO_Config(KEYPAD_R3_PORT, KEYPAD_R3_PINMASK, _GPIO_CFG_PULL_DOWN);
MOVW	R1, #lo_addr(_KEYPAD_R3_PINMASK+0)
MOVT	R1, #hi_addr(_KEYPAD_R3_PINMASK+0)
LDRSH	R2, [R1, #0]
MOVW	R1, #lo_addr(_KEYPAD_R3_PORT+0)
MOVT	R1, #hi_addr(_KEYPAD_R3_PORT+0)
LDR	R1, [R1, #0]
MOV	R0, R1
UXTH	R1, R2
MOV	R2, #256
BL	_GPIO_Config+0
;Keypad.c,40 :: 		GPIO_Config(KEYPAD_R4_PORT, KEYPAD_R4_PINMASK, _GPIO_CFG_PULL_DOWN);
MOVW	R1, #lo_addr(_KEYPAD_R4_PINMASK+0)
MOVT	R1, #hi_addr(_KEYPAD_R4_PINMASK+0)
LDRSH	R2, [R1, #0]
MOVW	R1, #lo_addr(_KEYPAD_R4_PORT+0)
MOVT	R1, #hi_addr(_KEYPAD_R4_PORT+0)
LDR	R1, [R1, #0]
MOV	R0, R1
UXTH	R1, R2
MOV	R2, #256
BL	_GPIO_Config+0
;Keypad.c,43 :: 		GPIO_Digital_Output(KEYPAD_C1_PORT, KEYPAD_C1_PINMASK);
MOVW	R1, #lo_addr(_KEYPAD_C1_PINMASK+0)
MOVT	R1, #hi_addr(_KEYPAD_C1_PINMASK+0)
LDRSH	R2, [R1, #0]
MOVW	R1, #lo_addr(_KEYPAD_C1_PORT+0)
MOVT	R1, #hi_addr(_KEYPAD_C1_PORT+0)
LDR	R1, [R1, #0]
MOV	R0, R1
MOV	R1, R2
BL	_GPIO_Digital_Output+0
;Keypad.c,44 :: 		GPIO_Digital_Output(KEYPAD_C2_PORT, KEYPAD_C2_PINMASK);
MOVW	R1, #lo_addr(_KEYPAD_C2_PINMASK+0)
MOVT	R1, #hi_addr(_KEYPAD_C2_PINMASK+0)
LDRSH	R2, [R1, #0]
MOVW	R1, #lo_addr(_KEYPAD_C2_PORT+0)
MOVT	R1, #hi_addr(_KEYPAD_C2_PORT+0)
LDR	R1, [R1, #0]
MOV	R0, R1
MOV	R1, R2
BL	_GPIO_Digital_Output+0
;Keypad.c,45 :: 		GPIO_Digital_Output(KEYPAD_C3_PORT, KEYPAD_C3_PINMASK);
MOVW	R1, #lo_addr(_KEYPAD_C3_PINMASK+0)
MOVT	R1, #hi_addr(_KEYPAD_C3_PINMASK+0)
LDRSH	R2, [R1, #0]
MOVW	R1, #lo_addr(_KEYPAD_C3_PORT+0)
MOVT	R1, #hi_addr(_KEYPAD_C3_PORT+0)
LDR	R1, [R1, #0]
MOV	R0, R1
MOV	R1, R2
BL	_GPIO_Digital_Output+0
;Keypad.c,46 :: 		GPIO_Digital_Output(KEYPAD_C4_PORT, KEYPAD_C4_PINMASK);
MOVW	R1, #lo_addr(_KEYPAD_C4_PINMASK+0)
MOVT	R1, #hi_addr(_KEYPAD_C4_PINMASK+0)
LDRSH	R2, [R1, #0]
MOVW	R1, #lo_addr(_KEYPAD_C4_PORT+0)
MOVT	R1, #hi_addr(_KEYPAD_C4_PORT+0)
LDR	R1, [R1, #0]
MOV	R0, R1
MOV	R1, R2
BL	_GPIO_Digital_Output+0
;Keypad.c,48 :: 		if (interrupt_mode)
CMP	R11, #0
IT	EQ
BEQ	L_keypad_initialize0
; interrupt_mode end address is: 44 (R11)
;Keypad.c,49 :: 		raise_columns();
BL	Keypad_raise_columns+0
IT	AL
BAL	L_keypad_initialize1
L_keypad_initialize0:
;Keypad.c,51 :: 		lower_columns();
BL	Keypad_lower_columns+0
L_keypad_initialize1:
;Keypad.c,52 :: 		}
L_end_keypad_initialize:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _keypad_initialize
Keypad_reset_debounce:
;Keypad.c,54 :: 		static inline void reset_debounce() {
SUB	SP, SP, #4
;Keypad.c,55 :: 		debounce_check = DEBOUNCE_INIT[current_column];
MOVW	R0, #lo_addr(Keypad_current_column+0)
MOVT	R0, #hi_addr(Keypad_current_column+0)
LDRB	R0, [R0, #0]
LSLS	R1, R0, #1
MOVW	R0, #lo_addr(Keypad_DEBOUNCE_INIT+0)
MOVT	R0, #hi_addr(Keypad_DEBOUNCE_INIT+0)
ADDS	R0, R0, R1
LDRH	R1, [R0, #0]
MOVW	R0, #lo_addr(Keypad_debounce_check+0)
MOVT	R0, #hi_addr(Keypad_debounce_check+0)
STRH	R1, [R0, #0]
;Keypad.c,56 :: 		}
L_end_reset_debounce:
ADD	SP, SP, #4
BX	LR
; end of Keypad_reset_debounce
Keypad_set_column:
;Keypad.c,58 :: 		static void set_column(unsigned int column_no) {
; column_no start address is: 0 (R0)
SUB	SP, SP, #4
; column_no end address is: 0 (R0)
; column_no start address is: 0 (R0)
;Keypad.c,59 :: 		if (!column_no || column_no > 4)
CMP	R0, #0
IT	EQ
BEQ	L_Keypad_set_column43
CMP	R0, #4
IT	HI
BHI	L_Keypad_set_column42
IT	AL
BAL	L_Keypad_set_column4
; column_no end address is: 0 (R0)
L_Keypad_set_column43:
L_Keypad_set_column42:
;Keypad.c,60 :: 		return;
IT	AL
BAL	L_end_set_column
L_Keypad_set_column4:
;Keypad.c,61 :: 		switch (column_no) {
; column_no start address is: 0 (R0)
IT	AL
BAL	L_Keypad_set_column5
;Keypad.c,62 :: 		case 1: KEYPAD_C1 = 1; break;
L_Keypad_set_column7:
MOVS	R2, #1
SXTB	R2, R2
MOVW	R1, #lo_addr(GPIOB_ODR+0)
MOVT	R1, #hi_addr(GPIOB_ODR+0)
STR	R2, [R1, #0]
IT	AL
BAL	L_Keypad_set_column6
;Keypad.c,63 :: 		case 2: KEYPAD_C2 = 1; break;
L_Keypad_set_column8:
MOVS	R2, #1
SXTB	R2, R2
MOVW	R1, #lo_addr(GPIOA_ODR+0)
MOVT	R1, #hi_addr(GPIOA_ODR+0)
STR	R2, [R1, #0]
IT	AL
BAL	L_Keypad_set_column6
;Keypad.c,64 :: 		case 3: KEYPAD_C3 = 1; break;
L_Keypad_set_column9:
MOVS	R2, #1
SXTB	R2, R2
MOVW	R1, #lo_addr(GPIOC_ODR+0)
MOVT	R1, #hi_addr(GPIOC_ODR+0)
STR	R2, [R1, #0]
IT	AL
BAL	L_Keypad_set_column6
;Keypad.c,65 :: 		case 4: KEYPAD_C4 = 1; break;
L_Keypad_set_column10:
MOVS	R2, #1
SXTB	R2, R2
MOVW	R1, #lo_addr(GPIOD_ODR+0)
MOVT	R1, #hi_addr(GPIOD_ODR+0)
STR	R2, [R1, #0]
IT	AL
BAL	L_Keypad_set_column6
;Keypad.c,66 :: 		default: break;
L_Keypad_set_column11:
IT	AL
BAL	L_Keypad_set_column6
;Keypad.c,67 :: 		}
L_Keypad_set_column5:
CMP	R0, #1
IT	EQ
BEQ	L_Keypad_set_column7
CMP	R0, #2
IT	EQ
BEQ	L_Keypad_set_column8
CMP	R0, #3
IT	EQ
BEQ	L_Keypad_set_column9
CMP	R0, #4
IT	EQ
BEQ	L_Keypad_set_column10
IT	AL
BAL	L_Keypad_set_column11
L_Keypad_set_column6:
;Keypad.c,68 :: 		current_column = column_no - 1;
SUBS	R2, R0, #1
; column_no end address is: 0 (R0)
MOVW	R1, #lo_addr(Keypad_current_column+0)
MOVT	R1, #hi_addr(Keypad_current_column+0)
STRB	R2, [R1, #0]
;Keypad.c,69 :: 		}
L_end_set_column:
ADD	SP, SP, #4
BX	LR
; end of Keypad_set_column
Keypad_clear_column:
;Keypad.c,70 :: 		static void clear_column(unsigned int column_no) {
; column_no start address is: 0 (R0)
SUB	SP, SP, #4
; column_no end address is: 0 (R0)
; column_no start address is: 0 (R0)
;Keypad.c,71 :: 		if (!column_no || column_no > 4)
CMP	R0, #0
IT	EQ
BEQ	L_Keypad_clear_column46
CMP	R0, #4
IT	HI
BHI	L_Keypad_clear_column45
IT	AL
BAL	L_Keypad_clear_column14
; column_no end address is: 0 (R0)
L_Keypad_clear_column46:
L_Keypad_clear_column45:
;Keypad.c,72 :: 		return;
IT	AL
BAL	L_end_clear_column
L_Keypad_clear_column14:
;Keypad.c,73 :: 		switch (column_no) {
; column_no start address is: 0 (R0)
IT	AL
BAL	L_Keypad_clear_column15
; column_no end address is: 0 (R0)
;Keypad.c,74 :: 		case 1: KEYPAD_C1 = 0; break;
L_Keypad_clear_column17:
MOVS	R2, #0
SXTB	R2, R2
MOVW	R1, #lo_addr(GPIOB_ODR+0)
MOVT	R1, #hi_addr(GPIOB_ODR+0)
STR	R2, [R1, #0]
IT	AL
BAL	L_Keypad_clear_column16
;Keypad.c,75 :: 		case 2: KEYPAD_C2 = 0; break;
L_Keypad_clear_column18:
MOVS	R2, #0
SXTB	R2, R2
MOVW	R1, #lo_addr(GPIOA_ODR+0)
MOVT	R1, #hi_addr(GPIOA_ODR+0)
STR	R2, [R1, #0]
IT	AL
BAL	L_Keypad_clear_column16
;Keypad.c,76 :: 		case 3: KEYPAD_C3 = 0; break;
L_Keypad_clear_column19:
MOVS	R2, #0
SXTB	R2, R2
MOVW	R1, #lo_addr(GPIOC_ODR+0)
MOVT	R1, #hi_addr(GPIOC_ODR+0)
STR	R2, [R1, #0]
IT	AL
BAL	L_Keypad_clear_column16
;Keypad.c,77 :: 		case 4: KEYPAD_C4 = 0; break;
L_Keypad_clear_column20:
MOVS	R2, #0
SXTB	R2, R2
MOVW	R1, #lo_addr(GPIOD_ODR+0)
MOVT	R1, #hi_addr(GPIOD_ODR+0)
STR	R2, [R1, #0]
IT	AL
BAL	L_Keypad_clear_column16
;Keypad.c,78 :: 		default: break;
L_Keypad_clear_column21:
IT	AL
BAL	L_Keypad_clear_column16
;Keypad.c,79 :: 		}
L_Keypad_clear_column15:
; column_no start address is: 0 (R0)
CMP	R0, #1
IT	EQ
BEQ	L_Keypad_clear_column17
CMP	R0, #2
IT	EQ
BEQ	L_Keypad_clear_column18
CMP	R0, #3
IT	EQ
BEQ	L_Keypad_clear_column19
CMP	R0, #4
IT	EQ
BEQ	L_Keypad_clear_column20
; column_no end address is: 0 (R0)
IT	AL
BAL	L_Keypad_clear_column21
L_Keypad_clear_column16:
;Keypad.c,81 :: 		}
L_end_clear_column:
ADD	SP, SP, #4
BX	LR
; end of Keypad_clear_column
Keypad_check_rows:
;Keypad.c,83 :: 		static char check_rows() {
SUB	SP, SP, #4
;Keypad.c,84 :: 		if (KEYPAD_R1 == 1)
MOVW	R1, #lo_addr(GPIOC_IDR+0)
MOVT	R1, #hi_addr(GPIOC_IDR+0)
LDR	R0, [R1, #0]
CMP	R0, #0
IT	EQ
BEQ	L_Keypad_check_rows22
;Keypad.c,85 :: 		return KEYPAD[0][current_column];
MOVW	R0, #lo_addr(Keypad_current_column+0)
MOVT	R0, #hi_addr(Keypad_current_column+0)
LDRB	R1, [R0, #0]
MOVW	R0, #lo_addr(_KEYPAD+0)
MOVT	R0, #hi_addr(_KEYPAD+0)
ADDS	R0, R0, R1
LDRB	R0, [R0, #0]
IT	AL
BAL	L_end_check_rows
L_Keypad_check_rows22:
;Keypad.c,86 :: 		if (KEYPAD_R2 == 1)
MOVW	R1, #lo_addr(GPIOC_IDR+0)
MOVT	R1, #hi_addr(GPIOC_IDR+0)
LDR	R0, [R1, #0]
CMP	R0, #0
IT	EQ
BEQ	L_Keypad_check_rows23
;Keypad.c,87 :: 		return KEYPAD[1][current_column];
MOVW	R0, #lo_addr(Keypad_current_column+0)
MOVT	R0, #hi_addr(Keypad_current_column+0)
LDRB	R1, [R0, #0]
MOVW	R0, #lo_addr(_KEYPAD+4)
MOVT	R0, #hi_addr(_KEYPAD+4)
ADDS	R0, R0, R1
LDRB	R0, [R0, #0]
IT	AL
BAL	L_end_check_rows
L_Keypad_check_rows23:
;Keypad.c,88 :: 		if (KEYPAD_R3 == 1)
MOVW	R1, #lo_addr(GPIOC_IDR+0)
MOVT	R1, #hi_addr(GPIOC_IDR+0)
LDR	R0, [R1, #0]
CMP	R0, #0
IT	EQ
BEQ	L_Keypad_check_rows24
;Keypad.c,89 :: 		return KEYPAD[2][current_column];
MOVW	R0, #lo_addr(Keypad_current_column+0)
MOVT	R0, #hi_addr(Keypad_current_column+0)
LDRB	R1, [R0, #0]
MOVW	R0, #lo_addr(_KEYPAD+8)
MOVT	R0, #hi_addr(_KEYPAD+8)
ADDS	R0, R0, R1
LDRB	R0, [R0, #0]
IT	AL
BAL	L_end_check_rows
L_Keypad_check_rows24:
;Keypad.c,90 :: 		if (KEYPAD_R4 == 1)
MOVW	R1, #lo_addr(GPIOC_IDR+0)
MOVT	R1, #hi_addr(GPIOC_IDR+0)
LDR	R0, [R1, #0]
CMP	R0, #0
IT	EQ
BEQ	L_Keypad_check_rows25
;Keypad.c,91 :: 		return KEYPAD[3][current_column];
MOVW	R0, #lo_addr(Keypad_current_column+0)
MOVT	R0, #hi_addr(Keypad_current_column+0)
LDRB	R1, [R0, #0]
MOVW	R0, #lo_addr(_KEYPAD+12)
MOVT	R0, #hi_addr(_KEYPAD+12)
ADDS	R0, R0, R1
LDRB	R0, [R0, #0]
IT	AL
BAL	L_end_check_rows
L_Keypad_check_rows25:
;Keypad.c,92 :: 		return NO_DETECTION;
MOVS	R0, #36
;Keypad.c,93 :: 		}
L_end_check_rows:
ADD	SP, SP, #4
BX	LR
; end of Keypad_check_rows
Keypad_set_column_and_check:
;Keypad.c,95 :: 		static char set_column_and_check(unsigned int column_no) {
; column_no start address is: 0 (R0)
SUB	SP, SP, #4
STR	LR, [SP, #0]
UXTH	R3, R0
; column_no end address is: 0 (R0)
; column_no start address is: 12 (R3)
;Keypad.c,96 :: 		char pressed = NO_DETECTION;
;Keypad.c,98 :: 		set_column(column_no);
UXTH	R0, R3
BL	Keypad_set_column+0
;Keypad.c,99 :: 		pressed = check_rows();
BL	Keypad_check_rows+0
; pressed start address is: 16 (R4)
UXTB	R4, R0
;Keypad.c,100 :: 		clear_column(column_no);
UXTH	R0, R3
; column_no end address is: 12 (R3)
BL	Keypad_clear_column+0
;Keypad.c,102 :: 		return pressed;
UXTB	R0, R4
; pressed end address is: 16 (R4)
;Keypad.c,103 :: 		}
L_end_set_column_and_check:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of Keypad_set_column_and_check
Keypad_get_pressed_button_singular:
;Keypad.c,105 :: 		static char get_pressed_button_singular() {
SUB	SP, SP, #4
STR	LR, [SP, #0]
;Keypad.c,106 :: 		char pressed = NO_DETECTION;
;Keypad.c,108 :: 		pressed = set_column_and_check(1);
MOVS	R0, #1
BL	Keypad_set_column_and_check+0
; pressed start address is: 4 (R1)
UXTB	R1, R0
;Keypad.c,110 :: 		if (pressed != NO_DETECTION)
CMP	R0, #36
IT	EQ
BEQ	L_Keypad_get_pressed_button_singular26
;Keypad.c,111 :: 		return pressed;
UXTB	R0, R1
; pressed end address is: 4 (R1)
IT	AL
BAL	L_end_get_pressed_button_singular
L_Keypad_get_pressed_button_singular26:
;Keypad.c,113 :: 		pressed = set_column_and_check(2);
MOVS	R0, #2
BL	Keypad_set_column_and_check+0
; pressed start address is: 4 (R1)
UXTB	R1, R0
;Keypad.c,115 :: 		if (pressed != NO_DETECTION)
CMP	R0, #36
IT	EQ
BEQ	L_Keypad_get_pressed_button_singular27
;Keypad.c,116 :: 		return pressed;
UXTB	R0, R1
; pressed end address is: 4 (R1)
IT	AL
BAL	L_end_get_pressed_button_singular
L_Keypad_get_pressed_button_singular27:
;Keypad.c,118 :: 		pressed = set_column_and_check(3);
MOVS	R0, #3
BL	Keypad_set_column_and_check+0
; pressed start address is: 4 (R1)
UXTB	R1, R0
;Keypad.c,120 :: 		if (pressed != NO_DETECTION)
CMP	R0, #36
IT	EQ
BEQ	L_Keypad_get_pressed_button_singular28
;Keypad.c,121 :: 		return pressed;
UXTB	R0, R1
; pressed end address is: 4 (R1)
IT	AL
BAL	L_end_get_pressed_button_singular
L_Keypad_get_pressed_button_singular28:
;Keypad.c,123 :: 		pressed = set_column_and_check(4);
MOVS	R0, #4
BL	Keypad_set_column_and_check+0
;Keypad.c,125 :: 		return pressed;
;Keypad.c,126 :: 		}
L_end_get_pressed_button_singular:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of Keypad_get_pressed_button_singular
Keypad_lower_columns:
;Keypad.c,128 :: 		static void lower_columns() {
SUB	SP, SP, #4
;Keypad.c,129 :: 		KEYPAD_C1 = KEYPAD_C2 = KEYPAD_C3 = KEYPAD_C4 = 0;
MOVS	R1, #0
SXTB	R1, R1
MOVW	R0, #lo_addr(GPIOD_ODR+0)
MOVT	R0, #hi_addr(GPIOD_ODR+0)
STR	R1, [R0, #0]
LDR	R1, [R0, #0]
MOVW	R0, #lo_addr(GPIOC_ODR+0)
MOVT	R0, #hi_addr(GPIOC_ODR+0)
STR	R1, [R0, #0]
LDR	R1, [R0, #0]
MOVW	R0, #lo_addr(GPIOA_ODR+0)
MOVT	R0, #hi_addr(GPIOA_ODR+0)
STR	R1, [R0, #0]
LDR	R1, [R0, #0]
MOVW	R0, #lo_addr(GPIOB_ODR+0)
MOVT	R0, #hi_addr(GPIOB_ODR+0)
STR	R1, [R0, #0]
;Keypad.c,130 :: 		}
L_end_lower_columns:
ADD	SP, SP, #4
BX	LR
; end of Keypad_lower_columns
Keypad_raise_columns:
;Keypad.c,131 :: 		static void raise_columns() {
SUB	SP, SP, #4
;Keypad.c,132 :: 		KEYPAD_C1 = KEYPAD_C2 = KEYPAD_C3 = KEYPAD_C4 = 1;
MOVS	R1, #1
SXTB	R1, R1
MOVW	R0, #lo_addr(GPIOD_ODR+0)
MOVT	R0, #hi_addr(GPIOD_ODR+0)
STR	R1, [R0, #0]
LDR	R1, [R0, #0]
MOVW	R0, #lo_addr(GPIOC_ODR+0)
MOVT	R0, #hi_addr(GPIOC_ODR+0)
STR	R1, [R0, #0]
LDR	R1, [R0, #0]
MOVW	R0, #lo_addr(GPIOA_ODR+0)
MOVT	R0, #hi_addr(GPIOA_ODR+0)
STR	R1, [R0, #0]
LDR	R1, [R0, #0]
MOVW	R0, #lo_addr(GPIOB_ODR+0)
MOVT	R0, #hi_addr(GPIOB_ODR+0)
STR	R1, [R0, #0]
;Keypad.c,133 :: 		}
L_end_raise_columns:
ADD	SP, SP, #4
BX	LR
; end of Keypad_raise_columns
_keypad_get_pressed_button:
;Keypad.c,135 :: 		char keypad_get_pressed_button() {
SUB	SP, SP, #4
STR	LR, [SP, #0]
;Keypad.c,136 :: 		char new_pressed = get_pressed_button_singular();
BL	Keypad_get_pressed_button_singular+0
; new_pressed start address is: 8 (R2)
UXTB	R2, R0
;Keypad.c,137 :: 		if (new_pressed != NO_DETECTION) {
CMP	R0, #36
IT	EQ
BEQ	L_keypad_get_pressed_button29
;Keypad.c,138 :: 		if (new_pressed == last_pressed) {
MOVW	R0, #lo_addr(Keypad_last_pressed+0)
MOVT	R0, #hi_addr(Keypad_last_pressed+0)
LDRB	R0, [R0, #0]
CMP	R2, R0
IT	NE
BNE	L_keypad_get_pressed_button30
;Keypad.c,139 :: 		--debounce_check;
MOVW	R1, #lo_addr(Keypad_debounce_check+0)
MOVT	R1, #hi_addr(Keypad_debounce_check+0)
LDRH	R0, [R1, #0]
SUBS	R0, R0, #1
UXTH	R0, R0
STRH	R0, [R1, #0]
;Keypad.c,140 :: 		if (!debounce_check) {
CMP	R0, #0
IT	NE
BNE	L_keypad_get_pressed_button31
;Keypad.c,141 :: 		reset_debounce();
BL	Keypad_reset_debounce+0
;Keypad.c,142 :: 		return new_pressed;
UXTB	R0, R2
; new_pressed end address is: 8 (R2)
IT	AL
BAL	L_end_keypad_get_pressed_button
;Keypad.c,143 :: 		}
L_keypad_get_pressed_button31:
;Keypad.c,145 :: 		return NO_DETECTION;
MOVS	R0, #36
IT	AL
BAL	L_end_keypad_get_pressed_button
;Keypad.c,146 :: 		}
L_keypad_get_pressed_button30:
;Keypad.c,148 :: 		reset_debounce();
; new_pressed start address is: 8 (R2)
BL	Keypad_reset_debounce+0
;Keypad.c,149 :: 		last_pressed = new_pressed;
MOVW	R0, #lo_addr(Keypad_last_pressed+0)
MOVT	R0, #hi_addr(Keypad_last_pressed+0)
STRB	R2, [R0, #0]
; new_pressed end address is: 8 (R2)
;Keypad.c,150 :: 		return NO_DETECTION;
MOVS	R0, #36
IT	AL
BAL	L_end_keypad_get_pressed_button
;Keypad.c,152 :: 		}
L_keypad_get_pressed_button29:
;Keypad.c,154 :: 		reset_debounce();
BL	Keypad_reset_debounce+0
;Keypad.c,155 :: 		return NO_DETECTION;
MOVS	R0, #36
;Keypad.c,159 :: 		}
L_end_keypad_get_pressed_button:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _keypad_get_pressed_button
_keypad_get_pressed_button_interrupt:
;Keypad.c,161 :: 		char keypad_get_pressed_button_interrupt() {
SUB	SP, SP, #8
STR	LR, [SP, #0]
;Keypad.c,162 :: 		char pressed = '$';
MOVS	R0, #36
STRB	R0, [SP, #4]
;Keypad.c,164 :: 		lower_columns();
BL	Keypad_lower_columns+0
;Keypad.c,165 :: 		while (pressed == '$')
L_keypad_get_pressed_button_interrupt35:
LDRB	R0, [SP, #4]
CMP	R0, #36
IT	NE
BNE	L_keypad_get_pressed_button_interrupt36
;Keypad.c,166 :: 		pressed = keypad_get_pressed_button();
BL	_keypad_get_pressed_button+0
STRB	R0, [SP, #4]
IT	AL
BAL	L_keypad_get_pressed_button_interrupt35
L_keypad_get_pressed_button_interrupt36:
;Keypad.c,167 :: 		raise_columns();
BL	Keypad_raise_columns+0
;Keypad.c,168 :: 		return pressed;
LDRB	R0, [SP, #4]
;Keypad.c,169 :: 		}
L_end_keypad_get_pressed_button_interrupt:
LDR	LR, [SP, #0]
ADD	SP, SP, #8
BX	LR
; end of _keypad_get_pressed_button_interrupt
_keypad_block_until_pressed_any:
;Keypad.c,171 :: 		char keypad_block_until_pressed_any() {
SUB	SP, SP, #4
STR	LR, [SP, #0]
;Keypad.c,172 :: 		char pressed = NO_DETECTION;
; pressed start address is: 0 (R0)
MOVS	R0, #36
; pressed end address is: 0 (R0)
;Keypad.c,173 :: 		while (pressed == NO_DETECTION)
L_keypad_block_until_pressed_any37:
; pressed start address is: 0 (R0)
CMP	R0, #36
IT	NE
BNE	L_keypad_block_until_pressed_any38
; pressed end address is: 0 (R0)
;Keypad.c,174 :: 		pressed = keypad_get_pressed_button();
BL	_keypad_get_pressed_button+0
; pressed start address is: 0 (R0)
IT	AL
BAL	L_keypad_block_until_pressed_any37
L_keypad_block_until_pressed_any38:
;Keypad.c,175 :: 		return pressed;
; pressed end address is: 0 (R0)
;Keypad.c,176 :: 		}
L_end_keypad_block_until_pressed_any:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _keypad_block_until_pressed_any
_keypad_block_until_pressed_character:
;Keypad.c,178 :: 		void keypad_block_until_pressed_character(char to_be_pressed) {
; to_be_pressed start address is: 0 (R0)
SUB	SP, SP, #4
STR	LR, [SP, #0]
UXTB	R1, R0
; to_be_pressed end address is: 0 (R0)
; to_be_pressed start address is: 4 (R1)
;Keypad.c,179 :: 		char pressed = NO_DETECTION;
; pressed start address is: 0 (R0)
MOVS	R0, #36
; to_be_pressed end address is: 4 (R1)
; pressed end address is: 0 (R0)
UXTB	R5, R1
;Keypad.c,180 :: 		while (pressed != to_be_pressed)
L_keypad_block_until_pressed_character39:
; pressed start address is: 0 (R0)
; to_be_pressed start address is: 20 (R5)
CMP	R0, R5
IT	EQ
BEQ	L_keypad_block_until_pressed_character40
; pressed end address is: 0 (R0)
;Keypad.c,181 :: 		pressed = keypad_get_pressed_button();
BL	_keypad_get_pressed_button+0
; pressed start address is: 0 (R0)
; to_be_pressed end address is: 20 (R5)
; pressed end address is: 0 (R0)
IT	AL
BAL	L_keypad_block_until_pressed_character39
L_keypad_block_until_pressed_character40:
;Keypad.c,182 :: 		}
L_end_keypad_block_until_pressed_character:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _keypad_block_until_pressed_character
Keypad____?ag:
SUB	SP, SP, #4
L_end_Keypad___?ag:
ADD	SP, SP, #4
BX	LR
; end of Keypad____?ag
