#ifndef THERMOMETER_H_
#define THERMOMETER_H_

#include <stdbool.h>
#include "Thermometer.h"

#define READ_ROM (0x33)
#define CONVERT_T (0x44)
#define READ_SCRATCHPAD (0xBE)
#define SKIP_ROM (0xCC)

extern sbit THERMO_OUT;
extern sbit THERMO_IN;

extern unsigned long *thermo_port;
extern signed int thermo_pinmask;

//static functions (exclusive to compilation unit)
static void set_as_input();
static void set_as_output();
static unsigned char thermo_read_bit();

static void thermo_write_zero_bit();
static void thermo_write_one_bit();
static void thermo_write_bit(unsigned char c);

static void thermo_write(unsigned char write_data);
static unsigned char thermo_read();
static unsigned short thermo_reset();

static float calculate_result(unsigned char lower, unsigned char higher);
static void thermo_measure_temperature();
static unsigned char calc_crc(unsigned char buffer[]);
//static functions (exclusive to compilation unit)

//public functions (external interface)
unsigned short thermo_get_family_code();
unsigned long long thermo_get_serial_number();
float thermo_read_temperature();
float thermo_read_precise_temperature();
bool thermo_check_crc();
//public functions (external interface)

#endif