#line 1 "C:/Users/Dusan/Desktop/clicker 2 for STM32 Project/MIPS.c"
#line 1 "c:/users/dusan/desktop/clicker 2 for stm32 project/mips.h"
#line 1 "c:/mikroelektronika/mikroc pro for arm/include/stdbool.h"



 typedef char _Bool;
#line 6 "c:/users/dusan/desktop/clicker 2 for stm32 project/mips.h"
extern void keypad_initialize( _Bool );
extern void LCD_initialize();
extern void states_initialize();
extern void change_state(int);
extern void interrupt_handler();

void interrupt_initialize();
void cpu_sleep();
#line 4 "C:/Users/Dusan/Desktop/clicker 2 for STM32 Project/MIPS.c"
void interrupt_initialize() {
 SYSCFGEN_bit = 1;



 SYSCFG_EXTICR1 |= 0x2222;
 EXTI_RTSR |= 0xf;
 EXTI_IMR |= 0xf;
 NVIC_IntEnable(IVT_INT_EXTI0);
 NVIC_IntEnable(IVT_INT_EXTI1);
 NVIC_IntEnable(IVT_INT_EXTI2);
 NVIC_IntEnable(IVT_INT_EXTI3);

}

void cpu_sleep() {
 asm {
 wfi;
 }
}

void interrupt_EXTI0() iv IVT_INT_EXTI0 ics ICS_AUTO {

 interrupt_handler();
 EXTI_PR.B0 = 1;
 return;
}

void interrupt_EXTI1() iv IVT_INT_EXTI1 ics ICS_AUTO {

 interrupt_handler();
 EXTI_PR.B1 = 1;
 return;
}

void interrupt_EXTI2() iv IVT_INT_EXTI2 ics ICS_AUTO {

 interrupt_handler();
 EXTI_PR.B2 = 1;
 return;
}

void interrupt_EXTI3() iv IVT_INT_EXTI3 ics ICS_AUTO {

 interrupt_handler();
 EXTI_PR.B3 = 1;
 return;
}





void main() {

 LCD_initialize();
 keypad_initialize( 1 );
 interrupt_initialize();
 states_initialize();

 while ( 1 ) {
 cpu_sleep();
 }

}
