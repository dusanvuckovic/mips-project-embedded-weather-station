#ifndef MIPS_H_
#define MIPS_H_

#include <stdbool.h>

extern void keypad_initialize(bool);
extern void LCD_initialize();
extern void states_initialize();
extern void change_state(int);
extern void interrupt_handler();

void interrupt_initialize();
void cpu_sleep();

#endif