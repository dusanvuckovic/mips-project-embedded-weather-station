#ifndef LCD_H_
#define LCD_H_

#include <stdbool.h>

typedef unsigned long* port;
typedef signed int pinmask;

//LCD ports
extern port   LCD_RS_PORT;
extern port   LCD_EN_PORT;
extern port   LCD_D4_PORT;
extern port   LCD_D5_PORT;
extern port   LCD_D6_PORT;
extern port   LCD_D7_PORT;

//LCD ports

//LCD pinmasks
extern pinmask LCD_RS_PINMASK;
extern pinmask LCD_EN_PINMASK;
extern pinmask LCD_D4_PINMASK;
extern pinmask LCD_D5_PINMASK;
extern pinmask LCD_D6_PINMASK;
extern pinmask LCD_D7_PINMASK;
//LCD pinmasks

// LCD bit positions
extern sbit LCD_RS;
extern sbit LCD_EN;
extern sbit LCD_D4;
extern sbit LCD_D5;
extern sbit LCD_D6;
extern sbit LCD_D7;
//LCD bit positions

typedef char Lcd_Instruction;
typedef char nibble;
static const Lcd_Instruction ERROR_INSTRUCTION = 0xff;

//static functions (exclusive to compilation unit)
static char upper_nibble(Lcd_Instruction ins);
static char lower_nibble(Lcd_Instruction ins);
static void nibble_to_data_pins(char nibble);

static Lcd_Instruction CLEAR_DISPLAY();
static Lcd_Instruction RETURN_HOME();
static Lcd_Instruction ENTRY_MODE_SET(bool increment, bool shift);
static Lcd_Instruction DISPLAY_CONTROL(bool display_on, bool cursor_on, bool cursor_blink);
static Lcd_Instruction CURSOR_OR_DISPLAY_SHIFT(bool shift_instead_of_move, bool to_the_right);
static Lcd_Instruction FUNCTION_SET();
static Lcd_Instruction SET_DDRAM_ADDRESS(unsigned char address);

static void pulse();
static void send_nibble(nibble n);
static void send_instruction(Lcd_Instruction ins);
//static functions (exclusive to compilation unit)

//public functions (external interface)
void LCD_initialize();
void LCD_clear_screen();
void LCD_clear_screen_top();
void LCD_clear_screen_bottom();
void LCD_turn_screen_off();
void LCD_turn_screen_on();
void LCD_turn_underline_off();
void LCD_turn_underline_on();
void LCD_turn_blinking_off();
void LCD_turn_blinking_on();
void LCD_command_device(bool screen_on, bool underline_on, bool blinking_on);

void LCD_shift_screen_right();
void LCD_shift_screen_left();
void LCD_shift_screen(bool right);
void LCD_set_cursor(unsigned char row, unsigned char column);
void LCD_set_cursor_to_row(unsigned char line);

void LCD_write_character_cursor(char character);
void LCD_write_line_cursor(const char* line);
void LCD_write_character_position(unsigned char row, unsigned char column, char character);
void LCD_write_line_position(unsigned char row, unsigned char column, const char* line);
void LCD_write_unsigned_int_cursor(unsigned int num);
void LCD_write_unsigned_int_position(int row, int column, unsigned int num);
void LCD_write_signed_int_cursor(int num);
void LCD_write_signed_int_position(int row, int column, int num);
void LCD_write_float_cursor(float f);
void LCD_write_float_position(int row, int column, float f);
//public functions (external interface)

#endif