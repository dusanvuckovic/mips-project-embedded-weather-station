KeypadBuffer_add_to_string:
;KeypadBuffer.c,20 :: 		static void add_to_string(unsigned char ch) {
; ch start address is: 0 (R0)
SUB	SP, SP, #4
; ch end address is: 0 (R0)
; ch start address is: 0 (R0)
;KeypadBuffer.c,21 :: 		if (position == 15)
MOVW	R1, #lo_addr(KeypadBuffer_position+0)
MOVT	R1, #hi_addr(KeypadBuffer_position+0)
LDRH	R1, [R1, #0]
CMP	R1, #15
IT	NE
BNE	L_KeypadBuffer_add_to_string0
; ch end address is: 0 (R0)
;KeypadBuffer.c,22 :: 		return;
IT	AL
BAL	L_end_add_to_string
L_KeypadBuffer_add_to_string0:
;KeypadBuffer.c,23 :: 		if (ch == PERIOD) {
; ch start address is: 0 (R0)
MOVW	R1, #lo_addr(KeypadBuffer_PERIOD+0)
MOVT	R1, #hi_addr(KeypadBuffer_PERIOD+0)
LDRB	R1, [R1, #0]
CMP	R0, R1
IT	NE
BNE	L_KeypadBuffer_add_to_string1
;KeypadBuffer.c,24 :: 		if (decimal_in)
MOVW	R1, #lo_addr(KeypadBuffer_decimal_in+0)
MOVT	R1, #hi_addr(KeypadBuffer_decimal_in+0)
LDRB	R1, [R1, #0]
CMP	R1, #0
IT	EQ
BEQ	L_KeypadBuffer_add_to_string2
; ch end address is: 0 (R0)
;KeypadBuffer.c,25 :: 		return;
IT	AL
BAL	L_end_add_to_string
L_KeypadBuffer_add_to_string2:
;KeypadBuffer.c,27 :: 		decimal_in = true;
; ch start address is: 0 (R0)
MOVS	R2, #1
MOVW	R1, #lo_addr(KeypadBuffer_decimal_in+0)
MOVT	R1, #hi_addr(KeypadBuffer_decimal_in+0)
STRB	R2, [R1, #0]
;KeypadBuffer.c,28 :: 		}
IT	AL
BAL	L_KeypadBuffer_add_to_string4
L_KeypadBuffer_add_to_string1:
;KeypadBuffer.c,29 :: 		else if (!decimal_in && to_the_left == 3)
MOVW	R1, #lo_addr(KeypadBuffer_decimal_in+0)
MOVT	R1, #hi_addr(KeypadBuffer_decimal_in+0)
LDRB	R1, [R1, #0]
CMP	R1, #0
IT	NE
BNE	L_KeypadBuffer_add_to_string58
MOVW	R1, #lo_addr(KeypadBuffer_to_the_left+0)
MOVT	R1, #hi_addr(KeypadBuffer_to_the_left+0)
LDRH	R1, [R1, #0]
CMP	R1, #3
IT	NE
BNE	L_KeypadBuffer_add_to_string57
; ch end address is: 0 (R0)
L_KeypadBuffer_add_to_string56:
;KeypadBuffer.c,30 :: 		return;
IT	AL
BAL	L_end_add_to_string
;KeypadBuffer.c,29 :: 		else if (!decimal_in && to_the_left == 3)
L_KeypadBuffer_add_to_string58:
; ch start address is: 0 (R0)
L_KeypadBuffer_add_to_string57:
;KeypadBuffer.c,31 :: 		else if (decimal_in && to_the_right == 4)
MOVW	R1, #lo_addr(KeypadBuffer_decimal_in+0)
MOVT	R1, #hi_addr(KeypadBuffer_decimal_in+0)
LDRB	R1, [R1, #0]
CMP	R1, #0
IT	EQ
BEQ	L_KeypadBuffer_add_to_string60
MOVW	R1, #lo_addr(KeypadBuffer_to_the_right+0)
MOVT	R1, #hi_addr(KeypadBuffer_to_the_right+0)
LDRH	R1, [R1, #0]
CMP	R1, #4
IT	NE
BNE	L_KeypadBuffer_add_to_string59
; ch end address is: 0 (R0)
L_KeypadBuffer_add_to_string55:
;KeypadBuffer.c,32 :: 		return;
IT	AL
BAL	L_end_add_to_string
;KeypadBuffer.c,31 :: 		else if (decimal_in && to_the_right == 4)
L_KeypadBuffer_add_to_string60:
; ch start address is: 0 (R0)
L_KeypadBuffer_add_to_string59:
;KeypadBuffer.c,33 :: 		else if (decimal_in)
MOVW	R1, #lo_addr(KeypadBuffer_decimal_in+0)
MOVT	R1, #hi_addr(KeypadBuffer_decimal_in+0)
LDRB	R1, [R1, #0]
CMP	R1, #0
IT	EQ
BEQ	L_KeypadBuffer_add_to_string13
;KeypadBuffer.c,34 :: 		++to_the_right;
MOVW	R2, #lo_addr(KeypadBuffer_to_the_right+0)
MOVT	R2, #hi_addr(KeypadBuffer_to_the_right+0)
LDRH	R1, [R2, #0]
ADDS	R1, R1, #1
STRH	R1, [R2, #0]
IT	AL
BAL	L_KeypadBuffer_add_to_string14
L_KeypadBuffer_add_to_string13:
;KeypadBuffer.c,36 :: 		++to_the_left;
MOVW	R2, #lo_addr(KeypadBuffer_to_the_left+0)
MOVT	R2, #hi_addr(KeypadBuffer_to_the_left+0)
LDRH	R1, [R2, #0]
ADDS	R1, R1, #1
STRH	R1, [R2, #0]
L_KeypadBuffer_add_to_string14:
L_KeypadBuffer_add_to_string4:
;KeypadBuffer.c,38 :: 		input_string[position++] = ch;
MOVW	R3, #lo_addr(KeypadBuffer_position+0)
MOVT	R3, #hi_addr(KeypadBuffer_position+0)
LDRH	R2, [R3, #0]
MOVW	R1, #lo_addr(KeypadBuffer_input_string+0)
MOVT	R1, #hi_addr(KeypadBuffer_input_string+0)
ADDS	R1, R1, R2
STRB	R0, [R1, #0]
; ch end address is: 0 (R0)
MOV	R1, R3
LDRH	R1, [R1, #0]
ADDS	R2, R1, #1
UXTH	R2, R2
STRH	R2, [R3, #0]
;KeypadBuffer.c,39 :: 		input_string[position] = '\0';
MOVW	R1, #lo_addr(KeypadBuffer_input_string+0)
MOVT	R1, #hi_addr(KeypadBuffer_input_string+0)
ADDS	R2, R1, R2
MOVS	R1, #0
STRB	R1, [R2, #0]
;KeypadBuffer.c,40 :: 		}
L_end_add_to_string:
ADD	SP, SP, #4
BX	LR
; end of KeypadBuffer_add_to_string
KeypadBuffer_remove_from_string:
;KeypadBuffer.c,42 :: 		static void remove_from_string() {
SUB	SP, SP, #4
;KeypadBuffer.c,43 :: 		if (!position)
MOVW	R0, #lo_addr(KeypadBuffer_position+0)
MOVT	R0, #hi_addr(KeypadBuffer_position+0)
LDRH	R0, [R0, #0]
CMP	R0, #0
IT	NE
BNE	L_KeypadBuffer_remove_from_string15
;KeypadBuffer.c,44 :: 		return;
IT	AL
BAL	L_end_remove_from_string
L_KeypadBuffer_remove_from_string15:
;KeypadBuffer.c,45 :: 		if (input_string[--position] == PERIOD)
MOVW	R2, #lo_addr(KeypadBuffer_position+0)
MOVT	R2, #hi_addr(KeypadBuffer_position+0)
LDRH	R0, [R2, #0]
SUBS	R1, R0, #1
UXTH	R1, R1
STRH	R1, [R2, #0]
MOVW	R0, #lo_addr(KeypadBuffer_input_string+0)
MOVT	R0, #hi_addr(KeypadBuffer_input_string+0)
ADDS	R0, R0, R1
LDRB	R1, [R0, #0]
MOVW	R0, #lo_addr(KeypadBuffer_PERIOD+0)
MOVT	R0, #hi_addr(KeypadBuffer_PERIOD+0)
LDRB	R0, [R0, #0]
CMP	R1, R0
IT	NE
BNE	L_KeypadBuffer_remove_from_string16
;KeypadBuffer.c,46 :: 		decimal_in = false;
MOVS	R1, #0
MOVW	R0, #lo_addr(KeypadBuffer_decimal_in+0)
MOVT	R0, #hi_addr(KeypadBuffer_decimal_in+0)
STRB	R1, [R0, #0]
IT	AL
BAL	L_KeypadBuffer_remove_from_string17
L_KeypadBuffer_remove_from_string16:
;KeypadBuffer.c,47 :: 		else if (decimal_in)
MOVW	R0, #lo_addr(KeypadBuffer_decimal_in+0)
MOVT	R0, #hi_addr(KeypadBuffer_decimal_in+0)
LDRB	R0, [R0, #0]
CMP	R0, #0
IT	EQ
BEQ	L_KeypadBuffer_remove_from_string18
;KeypadBuffer.c,48 :: 		--to_the_right;
MOVW	R1, #lo_addr(KeypadBuffer_to_the_right+0)
MOVT	R1, #hi_addr(KeypadBuffer_to_the_right+0)
LDRH	R0, [R1, #0]
SUBS	R0, R0, #1
STRH	R0, [R1, #0]
IT	AL
BAL	L_KeypadBuffer_remove_from_string19
L_KeypadBuffer_remove_from_string18:
;KeypadBuffer.c,50 :: 		--to_the_left;
MOVW	R1, #lo_addr(KeypadBuffer_to_the_left+0)
MOVT	R1, #hi_addr(KeypadBuffer_to_the_left+0)
LDRH	R0, [R1, #0]
SUBS	R0, R0, #1
STRH	R0, [R1, #0]
L_KeypadBuffer_remove_from_string19:
L_KeypadBuffer_remove_from_string17:
;KeypadBuffer.c,52 :: 		input_string[position] = '\0';
MOVW	R0, #lo_addr(KeypadBuffer_position+0)
MOVT	R0, #hi_addr(KeypadBuffer_position+0)
LDRH	R1, [R0, #0]
MOVW	R0, #lo_addr(KeypadBuffer_input_string+0)
MOVT	R0, #hi_addr(KeypadBuffer_input_string+0)
ADDS	R1, R0, R1
MOVS	R0, #0
STRB	R0, [R1, #0]
;KeypadBuffer.c,53 :: 		}
L_end_remove_from_string:
ADD	SP, SP, #4
BX	LR
; end of KeypadBuffer_remove_from_string
KeypadBuffer_m_pow:
;KeypadBuffer.c,55 :: 		static float m_pow(int exponent) {
; exponent start address is: 0 (R0)
SUB	SP, SP, #4
STR	LR, [SP, #0]
; exponent end address is: 0 (R0)
; exponent start address is: 0 (R0)
;KeypadBuffer.c,56 :: 		if (!exponent)
CMP	R0, #0
IT	NE
BNE	L_KeypadBuffer_m_pow20
; exponent end address is: 0 (R0)
;KeypadBuffer.c,57 :: 		return 1;
VMOV.F32	S0, #1
IT	AL
BAL	L_end_m_pow
L_KeypadBuffer_m_pow20:
;KeypadBuffer.c,58 :: 		return pow(10, exponent);
; exponent start address is: 0 (R0)
VMOV	S0, R0
VCVT.F32	#1, S0, S0
; exponent end address is: 0 (R0)
VMOV.F32	S1, S0
VMOV.F32	S0, #10
BL	_pow+0
;KeypadBuffer.c,59 :: 		}
L_end_m_pow:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of KeypadBuffer_m_pow
KeypadBuffer_clear_state:
;KeypadBuffer.c,61 :: 		static void clear_state() {
SUB	SP, SP, #4
;KeypadBuffer.c,62 :: 		input_string[0] = '\0';
MOVS	R1, #0
MOVW	R0, #lo_addr(KeypadBuffer_input_string+0)
MOVT	R0, #hi_addr(KeypadBuffer_input_string+0)
STRB	R1, [R0, #0]
;KeypadBuffer.c,63 :: 		position = to_the_left = to_the_right = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(KeypadBuffer_to_the_right+0)
MOVT	R0, #hi_addr(KeypadBuffer_to_the_right+0)
STRH	R1, [R0, #0]
MOVW	R1, #0
MOVW	R0, #lo_addr(KeypadBuffer_to_the_left+0)
MOVT	R0, #hi_addr(KeypadBuffer_to_the_left+0)
STRH	R1, [R0, #0]
MOVW	R1, #0
MOVW	R0, #lo_addr(KeypadBuffer_position+0)
MOVT	R0, #hi_addr(KeypadBuffer_position+0)
STRH	R1, [R0, #0]
;KeypadBuffer.c,64 :: 		decimal_in = is_negative = false;
MOVS	R1, #0
MOVW	R0, #lo_addr(KeypadBuffer_is_negative+0)
MOVT	R0, #hi_addr(KeypadBuffer_is_negative+0)
STRB	R1, [R0, #0]
MOVS	R1, #0
MOVW	R0, #lo_addr(KeypadBuffer_decimal_in+0)
MOVT	R0, #hi_addr(KeypadBuffer_decimal_in+0)
STRB	R1, [R0, #0]
;KeypadBuffer.c,65 :: 		}
L_end_clear_state:
ADD	SP, SP, #4
BX	LR
; end of KeypadBuffer_clear_state
_calculate_float:
;KeypadBuffer.c,67 :: 		float calculate_float() {
SUB	SP, SP, #12
STR	LR, [SP, #0]
;KeypadBuffer.c,68 :: 		double result = 0;
; result start address is: 24 (R6)
MOV	R0, #0
VMOV	S6, R0
;KeypadBuffer.c,69 :: 		int i = 0, j = 0;
;KeypadBuffer.c,71 :: 		for (i = 0; i < to_the_left; i++) {
; i start address is: 28 (R7)
MOVS	R7, #0
SXTH	R7, R7
; i end address is: 28 (R7)
; result end address is: 24 (R6)
L_calculate_float21:
; i start address is: 28 (R7)
; result start address is: 24 (R6)
MOVW	R0, #lo_addr(KeypadBuffer_to_the_left+0)
MOVT	R0, #hi_addr(KeypadBuffer_to_the_left+0)
LDRH	R0, [R0, #0]
CMP	R7, R0
IT	CS
BCS	L_calculate_float22
;KeypadBuffer.c,72 :: 		result += (input_string[i] - '0') * m_pow(to_the_left - 1 - i);
MOVW	R0, #lo_addr(KeypadBuffer_input_string+0)
MOVT	R0, #hi_addr(KeypadBuffer_input_string+0)
ADDS	R0, R0, R7
LDRB	R0, [R0, #0]
SUBS	R0, #48
STRH	R0, [SP, #8]
MOVW	R0, #lo_addr(KeypadBuffer_to_the_left+0)
MOVT	R0, #hi_addr(KeypadBuffer_to_the_left+0)
LDRH	R0, [R0, #0]
SUBS	R0, R0, #1
UXTH	R0, R0
SUB	R0, R0, R7
SXTH	R0, R0
BL	KeypadBuffer_m_pow+0
LDRSH	R0, [SP, #8]
VMOV	S1, R0
VCVT.F32	#1, S1, S1
VMUL.F32	S0, S1, S0
VADD.F32	S6, S6, S0
;KeypadBuffer.c,71 :: 		for (i = 0; i < to_the_left; i++) {
ADDS	R7, R7, #1
SXTH	R7, R7
;KeypadBuffer.c,73 :: 		}
IT	AL
BAL	L_calculate_float21
L_calculate_float22:
;KeypadBuffer.c,74 :: 		if (decimal_in)
MOVW	R0, #lo_addr(KeypadBuffer_decimal_in+0)
MOVT	R0, #hi_addr(KeypadBuffer_decimal_in+0)
LDRB	R0, [R0, #0]
CMP	R0, #0
IT	EQ
BEQ	L__calculate_float61
;KeypadBuffer.c,75 :: 		for (j = i+1; j < i+1+to_the_right; j++)
ADDS	R6, R7, #1
SXTH	R6, R6
; j start address is: 24 (R6)
; i end address is: 28 (R7)
; result end address is: 24 (R6)
; j end address is: 24 (R6)
STRH	R6, [SP, #4]
LDRSH	R6, [SP, #4]
L_calculate_float25:
; j start address is: 24 (R6)
; result start address is: 24 (R6)
; i start address is: 28 (R7)
ADDS	R1, R7, #1
SXTH	R1, R1
MOVW	R0, #lo_addr(KeypadBuffer_to_the_right+0)
MOVT	R0, #hi_addr(KeypadBuffer_to_the_right+0)
LDRH	R0, [R0, #0]
ADDS	R0, R1, R0
UXTH	R0, R0
CMP	R6, R0
IT	CS
BCS	L_calculate_float26
;KeypadBuffer.c,76 :: 		result += (input_string[j] - '0') * m_pow(-(j-to_the_left));
MOVW	R0, #lo_addr(KeypadBuffer_input_string+0)
MOVT	R0, #hi_addr(KeypadBuffer_input_string+0)
ADDS	R0, R0, R6
LDRB	R0, [R0, #0]
SUBS	R0, #48
STRH	R0, [SP, #8]
MOVW	R0, #lo_addr(KeypadBuffer_to_the_left+0)
MOVT	R0, #hi_addr(KeypadBuffer_to_the_left+0)
LDRH	R0, [R0, #0]
SUB	R0, R6, R0
UXTH	R0, R0
RSBS	R0, R0, #0
SXTH	R0, R0
BL	KeypadBuffer_m_pow+0
LDRSH	R0, [SP, #8]
VMOV	S1, R0
VCVT.F32	#1, S1, S1
VMUL.F32	S0, S1, S0
VADD.F32	S6, S6, S0
;KeypadBuffer.c,75 :: 		for (j = i+1; j < i+1+to_the_right; j++)
ADDS	R6, R6, #1
SXTH	R6, R6
;KeypadBuffer.c,76 :: 		result += (input_string[j] - '0') * m_pow(-(j-to_the_left));
STRH	R6, [SP, #4]
; i end address is: 28 (R7)
; j end address is: 24 (R6)
LDRSH	R6, [SP, #4]
IT	AL
BAL	L_calculate_float25
L_calculate_float26:
VMOV.F32	S1, S6
IT	AL
BAL	L_calculate_float24
; result end address is: 24 (R6)
L__calculate_float61:
;KeypadBuffer.c,74 :: 		if (decimal_in)
VMOV.F32	S1, S6
;KeypadBuffer.c,76 :: 		result += (input_string[j] - '0') * m_pow(-(j-to_the_left));
L_calculate_float24:
;KeypadBuffer.c,77 :: 		if (is_negative)
; result start address is: 4 (R1)
MOVW	R0, #lo_addr(KeypadBuffer_is_negative+0)
MOVT	R0, #hi_addr(KeypadBuffer_is_negative+0)
LDRB	R0, [R0, #0]
CMP	R0, #0
IT	EQ
BEQ	L__calculate_float62
;KeypadBuffer.c,78 :: 		result = -result;
VNEG.F32	S0, S1
VMOV.F32	S1, S0
; result end address is: 4 (R1)
VMOV.F32	S0, S1
IT	AL
BAL	L_calculate_float28
L__calculate_float62:
;KeypadBuffer.c,77 :: 		if (is_negative)
VMOV.F32	S0, S1
;KeypadBuffer.c,78 :: 		result = -result;
L_calculate_float28:
;KeypadBuffer.c,79 :: 		clear_state();
; result start address is: 0 (R0)
BL	KeypadBuffer_clear_state+0
;KeypadBuffer.c,80 :: 		return result;
; result end address is: 0 (R0)
;KeypadBuffer.c,81 :: 		}
L_end_calculate_float:
LDR	LR, [SP, #0]
ADD	SP, SP, #12
BX	LR
; end of _calculate_float
_input_float_blocking:
;KeypadBuffer.c,83 :: 		float input_float_blocking() {
SUB	SP, SP, #8
STR	LR, [SP, #0]
;KeypadBuffer.c,86 :: 		bool input_done = false, change_made = false;
MOVS	R0, #0
STRB	R0, [SP, #4]
MOVS	R0, #0
STRB	R0, [SP, #5]
;KeypadBuffer.c,87 :: 		LCD_set_cursor_to_row(2);
MOVS	R0, #2
BL	_LCD_set_cursor_to_row+0
;KeypadBuffer.c,89 :: 		while (!input_done) {
L_input_float_blocking29:
LDRB	R0, [SP, #4]
CMP	R0, #0
IT	NE
BNE	L_input_float_blocking30
;KeypadBuffer.c,90 :: 		c = keypad_block_until_pressed_any();
BL	_keypad_block_until_pressed_any+0
; c start address is: 0 (R0)
;KeypadBuffer.c,91 :: 		switch (c) {
IT	AL
BAL	L_input_float_blocking31
; c end address is: 0 (R0)
;KeypadBuffer.c,92 :: 		case '$': case 'A': case 'D':
L_input_float_blocking33:
L_input_float_blocking34:
L_input_float_blocking35:
;KeypadBuffer.c,93 :: 		break;
IT	AL
BAL	L_input_float_blocking32
;KeypadBuffer.c,94 :: 		case '#':
L_input_float_blocking36:
;KeypadBuffer.c,95 :: 		input_done = true; break;
MOVS	R0, #1
STRB	R0, [SP, #4]
IT	AL
BAL	L_input_float_blocking32
;KeypadBuffer.c,96 :: 		case '*':
L_input_float_blocking37:
;KeypadBuffer.c,97 :: 		add_to_string(PERIOD); change_made = true; break;
MOVW	R0, #lo_addr(KeypadBuffer_PERIOD+0)
MOVT	R0, #hi_addr(KeypadBuffer_PERIOD+0)
LDRB	R0, [R0, #0]
BL	KeypadBuffer_add_to_string+0
MOVS	R0, #1
STRB	R0, [SP, #5]
IT	AL
BAL	L_input_float_blocking32
;KeypadBuffer.c,98 :: 		case 'B':
L_input_float_blocking38:
;KeypadBuffer.c,99 :: 		remove_from_string(); change_made = true; break;
BL	KeypadBuffer_remove_from_string+0
MOVS	R0, #1
STRB	R0, [SP, #5]
IT	AL
BAL	L_input_float_blocking32
;KeypadBuffer.c,100 :: 		case 'C':
L_input_float_blocking39:
;KeypadBuffer.c,101 :: 		is_negative = !is_negative; change_made = true; break;
MOVW	R1, #lo_addr(KeypadBuffer_is_negative+0)
MOVT	R1, #hi_addr(KeypadBuffer_is_negative+0)
LDRB	R0, [R1, #0]
CMP	R0, #0
MOVW	R0, #0
BNE	L__input_float_blocking69
MOVS	R0, #1
L__input_float_blocking69:
STRB	R0, [R1, #0]
MOVS	R0, #1
STRB	R0, [SP, #5]
IT	AL
BAL	L_input_float_blocking32
;KeypadBuffer.c,102 :: 		default:
L_input_float_blocking40:
;KeypadBuffer.c,103 :: 		add_to_string(c); change_made = true; break;
; c start address is: 0 (R0)
; c end address is: 0 (R0)
BL	KeypadBuffer_add_to_string+0
MOVS	R0, #1
STRB	R0, [SP, #5]
IT	AL
BAL	L_input_float_blocking32
;KeypadBuffer.c,104 :: 		}
L_input_float_blocking31:
; c start address is: 0 (R0)
CMP	R0, #36
IT	EQ
BEQ	L_input_float_blocking33
CMP	R0, #65
IT	EQ
BEQ	L_input_float_blocking34
CMP	R0, #68
IT	EQ
BEQ	L_input_float_blocking35
CMP	R0, #35
IT	EQ
BEQ	L_input_float_blocking36
CMP	R0, #42
IT	EQ
BEQ	L_input_float_blocking37
CMP	R0, #66
IT	EQ
BEQ	L_input_float_blocking38
CMP	R0, #67
IT	EQ
BEQ	L_input_float_blocking39
IT	AL
BAL	L_input_float_blocking40
; c end address is: 0 (R0)
L_input_float_blocking32:
;KeypadBuffer.c,105 :: 		if (change_made) {
LDRB	R0, [SP, #5]
CMP	R0, #0
IT	EQ
BEQ	L_input_float_blocking41
;KeypadBuffer.c,106 :: 		LCD_clear_screen_bottom();
BL	_LCD_clear_screen_bottom+0
;KeypadBuffer.c,107 :: 		if (is_negative)
MOVW	R0, #lo_addr(KeypadBuffer_is_negative+0)
MOVT	R0, #hi_addr(KeypadBuffer_is_negative+0)
LDRB	R0, [R0, #0]
CMP	R0, #0
IT	EQ
BEQ	L_input_float_blocking42
;KeypadBuffer.c,108 :: 		LCD_write_character_position(2, 1, '-');
MOVS	R2, #45
MOVS	R1, #1
MOVS	R0, #2
BL	_LCD_write_character_position+0
IT	AL
BAL	L_input_float_blocking43
L_input_float_blocking42:
;KeypadBuffer.c,110 :: 		LCD_write_character_position(2, 1, '+');
MOVS	R2, #43
MOVS	R1, #1
MOVS	R0, #2
BL	_LCD_write_character_position+0
L_input_float_blocking43:
;KeypadBuffer.c,111 :: 		LCD_write_line_cursor(input_string);
MOVW	R0, #lo_addr(KeypadBuffer_input_string+0)
MOVT	R0, #hi_addr(KeypadBuffer_input_string+0)
BL	_LCD_write_line_cursor+0
;KeypadBuffer.c,112 :: 		change_made = false;
MOVS	R0, #0
STRB	R0, [SP, #5]
;KeypadBuffer.c,113 :: 		}
L_input_float_blocking41:
;KeypadBuffer.c,114 :: 		}
IT	AL
BAL	L_input_float_blocking29
L_input_float_blocking30:
;KeypadBuffer.c,115 :: 		ret_val = calculate_float();
BL	_calculate_float+0
;KeypadBuffer.c,116 :: 		return ret_val;
;KeypadBuffer.c,117 :: 		}
L_end_input_float_blocking:
LDR	LR, [SP, #0]
ADD	SP, SP, #8
BX	LR
; end of _input_float_blocking
_input_float:
;KeypadBuffer.c,119 :: 		void input_float(char c) {
; c start address is: 0 (R0)
SUB	SP, SP, #4
STR	LR, [SP, #0]
; c end address is: 0 (R0)
; c start address is: 0 (R0)
;KeypadBuffer.c,120 :: 		switch (c) {
IT	AL
BAL	L_input_float44
; c end address is: 0 (R0)
;KeypadBuffer.c,121 :: 		case '*':
L_input_float46:
;KeypadBuffer.c,122 :: 		add_to_string(PERIOD); break;
MOVW	R1, #lo_addr(KeypadBuffer_PERIOD+0)
MOVT	R1, #hi_addr(KeypadBuffer_PERIOD+0)
LDRB	R1, [R1, #0]
UXTB	R0, R1
BL	KeypadBuffer_add_to_string+0
IT	AL
BAL	L_input_float45
;KeypadBuffer.c,123 :: 		case 'B':
L_input_float47:
;KeypadBuffer.c,124 :: 		remove_from_string(); break;
BL	KeypadBuffer_remove_from_string+0
IT	AL
BAL	L_input_float45
;KeypadBuffer.c,125 :: 		case 'C':
L_input_float48:
;KeypadBuffer.c,126 :: 		is_negative = !is_negative; break;
MOVW	R2, #lo_addr(KeypadBuffer_is_negative+0)
MOVT	R2, #hi_addr(KeypadBuffer_is_negative+0)
LDRB	R1, [R2, #0]
CMP	R1, #0
MOVW	R1, #0
BNE	L__input_float71
MOVS	R1, #1
L__input_float71:
STRB	R1, [R2, #0]
IT	AL
BAL	L_input_float45
;KeypadBuffer.c,127 :: 		case '#': case 'A': case 'D': return;
L_input_float49:
L_input_float50:
L_input_float51:
IT	AL
BAL	L_end_input_float
;KeypadBuffer.c,128 :: 		default:
L_input_float52:
;KeypadBuffer.c,129 :: 		add_to_string(c); break;
; c start address is: 0 (R0)
; c end address is: 0 (R0)
BL	KeypadBuffer_add_to_string+0
IT	AL
BAL	L_input_float45
;KeypadBuffer.c,130 :: 		}
L_input_float44:
; c start address is: 0 (R0)
CMP	R0, #42
IT	EQ
BEQ	L_input_float46
CMP	R0, #66
IT	EQ
BEQ	L_input_float47
CMP	R0, #67
IT	EQ
BEQ	L_input_float48
CMP	R0, #35
IT	EQ
BEQ	L_input_float49
CMP	R0, #65
IT	EQ
BEQ	L_input_float50
CMP	R0, #68
IT	EQ
BEQ	L_input_float51
IT	AL
BAL	L_input_float52
; c end address is: 0 (R0)
L_input_float45:
;KeypadBuffer.c,131 :: 		LCD_clear_screen_bottom();
BL	_LCD_clear_screen_bottom+0
;KeypadBuffer.c,132 :: 		if (is_negative)
MOVW	R1, #lo_addr(KeypadBuffer_is_negative+0)
MOVT	R1, #hi_addr(KeypadBuffer_is_negative+0)
LDRB	R1, [R1, #0]
CMP	R1, #0
IT	EQ
BEQ	L_input_float53
;KeypadBuffer.c,133 :: 		LCD_write_character_position(2, 1, '-');
MOVS	R2, #45
MOVS	R1, #1
MOVS	R0, #2
BL	_LCD_write_character_position+0
IT	AL
BAL	L_input_float54
L_input_float53:
;KeypadBuffer.c,135 :: 		LCD_write_character_position(2, 1, '+');
MOVS	R2, #43
MOVS	R1, #1
MOVS	R0, #2
BL	_LCD_write_character_position+0
L_input_float54:
;KeypadBuffer.c,136 :: 		LCD_write_line_cursor(input_string);
MOVW	R0, #lo_addr(KeypadBuffer_input_string+0)
MOVT	R0, #hi_addr(KeypadBuffer_input_string+0)
BL	_LCD_write_line_cursor+0
;KeypadBuffer.c,137 :: 		}
L_end_input_float:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _input_float
