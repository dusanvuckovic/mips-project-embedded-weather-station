#line 1 "C:/Users/Dusan/Desktop/clicker 2 for STM32 Project/LCD.c"
#line 1 "c:/users/dusan/desktop/clicker 2 for stm32 project/lcd.h"
#line 1 "c:/mikroelektronika/mikroc pro for arm/include/stdbool.h"



 typedef char _Bool;
#line 6 "c:/users/dusan/desktop/clicker 2 for stm32 project/lcd.h"
typedef unsigned long* port;
typedef signed int pinmask;


extern port LCD_RS_PORT;
extern port LCD_EN_PORT;
extern port LCD_D4_PORT;
extern port LCD_D5_PORT;
extern port LCD_D6_PORT;
extern port LCD_D7_PORT;




extern pinmask LCD_RS_PINMASK;
extern pinmask LCD_EN_PINMASK;
extern pinmask LCD_D4_PINMASK;
extern pinmask LCD_D5_PINMASK;
extern pinmask LCD_D6_PINMASK;
extern pinmask LCD_D7_PINMASK;



extern sbit LCD_RS;
extern sbit LCD_EN;
extern sbit LCD_D4;
extern sbit LCD_D5;
extern sbit LCD_D6;
extern sbit LCD_D7;


typedef char Lcd_Instruction;
typedef char nibble;
static const Lcd_Instruction ERROR_INSTRUCTION = 0xff;


static char upper_nibble(Lcd_Instruction ins);
static char lower_nibble(Lcd_Instruction ins);
static void nibble_to_data_pins(char nibble);

static Lcd_Instruction CLEAR_DISPLAY();
static Lcd_Instruction RETURN_HOME();
static Lcd_Instruction ENTRY_MODE_SET( _Bool  increment,  _Bool  shift);
static Lcd_Instruction DISPLAY_CONTROL( _Bool  display_on,  _Bool  cursor_on,  _Bool  cursor_blink);
static Lcd_Instruction CURSOR_OR_DISPLAY_SHIFT( _Bool  shift_instead_of_move,  _Bool  to_the_right);
static Lcd_Instruction FUNCTION_SET();
static Lcd_Instruction SET_DDRAM_ADDRESS(unsigned char address);

static void pulse();
static void send_nibble(nibble n);
static void send_instruction(Lcd_Instruction ins);



void LCD_initialize();
void LCD_clear_screen();
void LCD_clear_screen_top();
void LCD_clear_screen_bottom();
void LCD_turn_screen_off();
void LCD_turn_screen_on();
void LCD_turn_underline_off();
void LCD_turn_underline_on();
void LCD_turn_blinking_off();
void LCD_turn_blinking_on();
void LCD_command_device( _Bool  screen_on,  _Bool  underline_on,  _Bool  blinking_on);

void LCD_shift_screen_right();
void LCD_shift_screen_left();
void LCD_shift_screen( _Bool  right);
void LCD_set_cursor(unsigned char row, unsigned char column);
void LCD_set_cursor_to_row(unsigned char line);

void LCD_write_character_cursor(char character);
void LCD_write_line_cursor(const char* line);
void LCD_write_character_position(unsigned char row, unsigned char column, char character);
void LCD_write_line_position(unsigned char row, unsigned char column, const char* line);
void LCD_write_unsigned_int_cursor(unsigned int num);
void LCD_write_unsigned_int_position(int row, int column, unsigned int num);
void LCD_write_signed_int_cursor(int num);
void LCD_write_signed_int_position(int row, int column, int num);
void LCD_write_float_cursor(float f);
void LCD_write_float_position(int row, int column, float f);
#line 1 "c:/users/dusan/desktop/clicker 2 for stm32 project/lcd_constants.h"




unsigned long *LCD_RS_PORT = &GPIOE_BASE,
 *LCD_EN_PORT = &GPIOE_BASE,
 *LCD_D4_PORT = &GPIOE_BASE,
 *LCD_D5_PORT = &GPIOE_BASE,
 *LCD_D6_PORT = &GPIOE_BASE,
 *LCD_D7_PORT = &GPIOB_BASE;



signed int LCD_RS_PINMASK = _GPIO_PINMASK_1,
 LCD_EN_PINMASK = _GPIO_PINMASK_2,
 LCD_D4_PINMASK = _GPIO_PINMASK_3,
 LCD_D5_PINMASK = _GPIO_PINMASK_4,
 LCD_D6_PINMASK = _GPIO_PINMASK_6,
 LCD_D7_PINMASK = _GPIO_PINMASK_6;



sbit LCD_RS at GPIOE_ODR.B1;
sbit LCD_EN at GPIOE_ODR.B2;
sbit LCD_D4 at GPIOE_ODR.B3;
sbit LCD_D5 at GPIOE_ODR.B4;
sbit LCD_D6 at GPIOE_ODR.B6;
sbit LCD_D7 at GPIOB_ODR.B6;
#line 4 "C:/Users/Dusan/Desktop/clicker 2 for STM32 Project/LCD.c"
static  _Bool  screen, blink, underline;
static char cursor_position;
static char* empty_string = "                ";

static char upper_nibble(Lcd_Instruction ins) {
 return (ins >> 4) & 0xf;
}
static char lower_nibble(Lcd_Instruction ins) {
 return (ins & 0xf);
}

static void nibble_to_data_pins(char nibble) {
 if (nibble & 0x8)
 LCD_D7 = 1;
 else
 LCD_D7 = 0;
 if (nibble & 0x4)
 LCD_D6 = 1;
 else
 LCD_D6 = 0;
 if (nibble & 0x2)
 LCD_D5 = 1;
 else
 LCD_D5 = 0;
 if (nibble & 0x1)
 LCD_D4 = 1;
 else
 LCD_D4 = 0;
}

static Lcd_Instruction CLEAR_DISPLAY() {
 cursor_position = 0x00;
 return 0x1;
}
static Lcd_Instruction RETURN_HOME() {
 cursor_position = 0x00;
 return 0x2;
}
static Lcd_Instruction ENTRY_MODE_SET( _Bool  increment,  _Bool  shift) {
 Lcd_Instruction opcode = 0x4;
 if (increment)
 opcode |= 0x2;
 if (shift)
 opcode |= 0x1;
 return opcode;
}
static Lcd_Instruction DISPLAY_CONTROL( _Bool  display_on,  _Bool  cursor_on,  _Bool  cursor_blink) {
 Lcd_Instruction opcode = 0x8;
 LCD_RS = 0;
 if (display_on)
 opcode |= 0x4;
 if (cursor_on)
 opcode |= 0x2;
 if (cursor_blink)
 opcode |= 0x1;
 return opcode;
}
static Lcd_Instruction CURSOR_OR_DISPLAY_SHIFT( _Bool  shift_instead_of_move,  _Bool  to_the_right) {
 Lcd_Instruction opcode = 0x10;
 LCD_RS = 0;
 if (shift_instead_of_move)
 opcode |= 0x8;
 if (to_the_right)
 opcode |= 0x4;
 return opcode;
}
static Lcd_Instruction FUNCTION_SET() {






 return 0x28;
}

static Lcd_Instruction SET_DDRAM_ADDRESS(unsigned char address) {
 Lcd_Instruction opcode = 0x80;
 opcode |= address;
 return opcode;
}

static void pulse() {
 LCD_EN = 1;
 Delay_ms(1);
 LCD_EN = 0;
}

static void send_nibble(nibble n) {
 Delay_ms(1);
 nibble_to_data_pins(n);
 pulse();
}

static void send_instruction(Lcd_Instruction ins) {
 nibble first_nibble = upper_nibble(ins),
 second_nibble = lower_nibble(ins);
 send_nibble(first_nibble);
 send_nibble(second_nibble);
}

void LCD_initialize() {

 GPIO_Clk_Enable(LCD_RS_PORT);
 GPIO_Clk_Enable(LCD_EN_PORT);
 GPIO_Clk_Enable(LCD_D7_PORT);
 GPIO_Clk_Enable(LCD_D6_PORT);
 GPIO_Clk_Enable(LCD_D5_PORT);
 GPIO_Clk_Enable(LCD_D4_PORT);

 GPIO_Digital_Output(LCD_RS_PORT, LCD_RS_PINMASK);
 GPIO_Digital_Output(LCD_EN_PORT, LCD_EN_PINMASK);
 GPIO_Digital_Output(LCD_D7_PORT, LCD_D7_PINMASK);
 GPIO_Digital_Output(LCD_D6_PORT, LCD_D6_PINMASK);
 GPIO_Digital_Output(LCD_D5_PORT, LCD_D5_PINMASK);
 GPIO_Digital_Output(LCD_D4_PORT, LCD_D4_PINMASK);

 LCD_RS = LCD_EN = LCD_D7 = LCD_D6 = LCD_D5 = LCD_D4 = 0;

 Delay_ms(50);
 send_nibble(0x3);
 Delay_ms(5);
 send_nibble(0x3);
 Delay_ms(100);
 send_nibble(0x3);
 send_nibble(0x2);

 send_instruction(FUNCTION_SET());
 send_instruction(DISPLAY_CONTROL( 0 ,  0 ,  0 ));
 send_instruction(CLEAR_DISPLAY());
 send_instruction(ENTRY_MODE_SET( 1 ,  0 ));
 send_instruction(DISPLAY_CONTROL( 1 ,  1 ,  1 ));

 send_instruction(CLEAR_DISPLAY());

 screen = blink = underline =  1 ;
 cursor_position = 0x00;
}

void LCD_clear_screen() {
 send_instruction(CLEAR_DISPLAY());
}

void LCD_clear_screen_top() {
 LCD_write_line_position(1, 1, empty_string);
 LCD_set_cursor(2, 1);
}

void LCD_clear_screen_bottom() {
 LCD_write_line_position(2, 1, empty_string);
 LCD_set_cursor(2, 1);
}

void LCD_turn_screen_off() {
 if (!screen)
 return;
 send_instruction(DISPLAY_CONTROL( 0 , underline, blink));
 screen =  0 ;
}

void LCD_turn_screen_on() {
 if (screen)
 return;
 send_instruction(DISPLAY_CONTROL( 1 , underline, blink));
 screen =  1 ;
}

void LCD_turn_underline_off() {
 if (!underline)
 return;
 send_instruction(DISPLAY_CONTROL(screen,  0 , blink));
 underline =  0 ;
}

void LCD_turn_underline_on() {
 if (underline)
 return;
 send_instruction(DISPLAY_CONTROL(screen,  1 , blink));
 underline =  1 ;
}

void LCD_turn_blinking_off() {
 if (!blink)
 return;
 send_instruction(DISPLAY_CONTROL(screen, underline,  0 ));
 blink =  0 ;
}

void LCD_turn_blinking_on() {
 if (blink)
 return;
 send_instruction(DISPLAY_CONTROL(screen, underline,  1 ));
 blink =  1 ;
}

void LCD_command_device( _Bool  screen_on,  _Bool  underline_on,  _Bool  blinking_on) {
 screen = screen_on;
 underline = underline_on;
 blink = blinking_on;
 send_instruction(DISPLAY_CONTROL(screen, underline, blink));
}

void LCD_shift_screen_right() {
 send_instruction(CURSOR_OR_DISPLAY_SHIFT( 1 ,  1 ));
}

void LCD_shift_screen_left() {
 send_instruction(CURSOR_OR_DISPLAY_SHIFT( 1 ,  0 ));
}

void LCD_shift_screen( _Bool  right) {
 send_instruction(CURSOR_OR_DISPLAY_SHIFT( 0 , !right));
}

void LCD_set_cursor(unsigned char row, unsigned char column) {
 unsigned char address = column - 1;
 if (row != 1 && row != 2)
 return;
 if (!column || column > 16)
 return;
 if (row == 2)
 address |= 0x40;
 send_instruction(SET_DDRAM_ADDRESS(address));
 cursor_position = address;
}

void LCD_set_cursor_to_row(unsigned char line) {
 if ((line != 1) || (line != 2))
 return;
 LCD_set_cursor(line, 0);
}

void LCD_write_character_cursor(char character) {
 if (cursor_position == 0x10) {
 LCD_set_cursor(2, 1);
 cursor_position = 0x40;
 }
 else if (cursor_position == 0x50) {
 LCD_set_cursor(1, 1);
 cursor_position = 0x00;
 }
 LCD_RS = 1;
 send_instruction(character);
 ++cursor_position;
 LCD_RS = 0;
}

void LCD_write_line_cursor(const char* line) {
 while (*line != '\0') {
 LCD_write_character_cursor(*line);
 ++line;
 }
}

void LCD_write_character_position(unsigned char row, unsigned char column, char character) {
 LCD_set_cursor(row, column);
 LCD_write_character_cursor(character);
}

void LCD_write_line_position(unsigned char row, unsigned char column, const char* line) {
 LCD_set_cursor(row, column);
 LCD_write_line_cursor(line);
}

void LCD_write_unsigned_int_cursor(unsigned int num) {
 char txt[6];

 WordToStr(num, txt);
 Ltrim(txt);
 Rtrim(txt);
 LCD_write_line_cursor(txt);

}

void LCD_write_unsigned_int_position(int row, int column, unsigned int num) {
 LCD_set_cursor(row, column);
 LCD_write_unsigned_int_cursor(num);
}

void LCD_write_signed_int_cursor(int num) {
 char txt[7];

 IntToStr(num, txt);
 Ltrim(txt);
 Rtrim(txt);
 LCD_write_line_cursor(txt);
}

void LCD_write_signed_int_position(int row, int column, int num) {
 LCD_set_cursor(row, column);
 LCD_write_signed_int_cursor(num);
}

void LCD_write_short_cursor(short num) {
 char txt[5];

 ShortToStr(num, txt);
 Ltrim(txt);
 Rtrim(txt);
 LCD_write_line_cursor(txt);
}

void LCD_write_short_position(int row, int column, short num) {
 LCD_set_cursor(row, column);
 LCD_write_short_cursor(num);
}

void LCD_write_unsigned_long_long_cursor(unsigned long long num) {
 char txt[17];

 LongLongUnsignedToHex(num, txt);
 Ltrim(txt);
 Rtrim(txt);
 LCD_write_line_cursor(txt);
}

void LCD_write_unsigned_long_long_position(int row, int column, unsigned long long num) {
 LCD_set_cursor(row, column);
 LCD_write_unsigned_long_long_cursor(num);
}

void LCD_write_float_cursor(float f) {
 char txt[15];

 FloatToStr(f, txt);
 Ltrim(txt);
 Rtrim(txt);
 LCD_write_line_cursor(txt);
}

void LCD_write_float_position(int row, int column, float f) {
 LCD_set_cursor(row, column);
 LCD_write_float_cursor(f);
}
