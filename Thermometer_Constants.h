#ifndef _THERMOMETER_CONSTANTS_
#define _THERMOMETER_CONSTANTS_

unsigned long *thermo_port = &GPIOB_BASE;
signed int thermo_pinmask = _GPIO_PINMASK_5;

sbit THERMO_OUT at ODR5_GPIOB_ODR_bit;
sbit THERMO_IN at IDR5_GPIOB_IDR_bit;

#endif